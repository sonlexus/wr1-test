/*!
 * WP Googel Analytics Events | v2.5.1
 * Copyright (c) 2013 Yuval Oren (@yuvalo)
 * License: GPLv2
 */

/*jslint indent: 4 */
/*global $, jQuery, document, window, _gaq*/

var scroll_events = (function ($) {
    "use strict";

    var scroll_elements  = [];
    var click_elements = [];
    var universal = 0;
    var gtm = 0;
    var gst = 0;
    var ga_element;

    var track_event = function (category, action, label, universal, bounce, evalue ){
        var event_category = !category ? '' : category;
        var event_action = !action ? '' : action;
        var event_label = !label ? '' : label;
        var event_bounce = bounce === "true" ? true : false;
        var event_value = !evalue ? false : evalue;

        if( typeof ga_element === "undefined" ){
           if( typeof ga !== 'undefined' ){
               ga_element = ga;
            } 
            else if( typeof _gaq !== 'undefined' ){
                ga_element = _gaq;
            } 
            else if( typeof __gaTracker === "function" ){
                ga_element = __gaTracker;
            } else if (!gtm && !gst){
               return;
           }
        }

        if (gtm) {
            dataLayer.push({
                'event': 'WPGAE',
                'eventCategory': category,
                'eventAction': action,
                'eventLabel': label,
                'eventValue': event_value,
                'nonInteraction': event_bounce
            });
        } else if (gst) {
                gtag('event', action, {
                    // Event parameters
                    'event_category': category,
                    'event_label': label,
                    'value': event_value,
                    'non_interaction': event_bounce
                });
        } else if( universal ){
            // ga_element('send','event', category, action, label);
            if (event_value) {
                ga_element('send','event', category, action, label, event_value,{'nonInteraction': event_bounce});
            }else{
                ga_element('send','event', category, action, label, {'nonInteraction': event_bounce});
            }
            

        }
        else {
            // ga_element.push(['_trackEvent',category, action, label]);
            ga_element.push( ['_trackEvent',category, action, label, event_value, event_bounce] );
        }

    };

    var click_event = function( event ){
        
        track_event(event.data.category, event.data.action, event.data.label, event.data.universal, event.data.bounce, event.data.evalue);
        var hasHref = event.currentTarget.href;
        var hrefTarget = event.currentTarget.target;
        if (hasHref && hasHref !== "") {
            event.preventDefault();
            if (hrefTarget.trim() === "_blank") {
                var w = window.open('', '_blank');
                w.location.href = hasHref;
            } else {
                setTimeout(function () {
                    window.location = hasHref;
                }, 100);
            }
        }
    };

    var unescapeChars = function (text) {

        var map = {
            '&amp;': '&',
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '\"': '"',
            '&#039;': "'"
        };        

        if (typeof text != 'string') {

            var cleanObj;

            // if param text is non-string (assuming JSON object), we convert it first to string
            cleanObj = JSON.stringify(text);

            // we replace other chars
            cleanObj.replace(/&lt;|&gt;|&quot;|'&#039;/gi, function (m) {
                        return map[m];
                    });

            // convert it back to JSON obj
            cleanObj = JSON.parse(cleanObj);
            return cleanObj;
        }else{
            return text.replace(/&lt;|&gt;|&quot;|'&#039;/gi, function (m) {
                        return map[m];
                    });
        }
        
        return ''; //fallback
    };


    return {
        bind_events : function (settings) {
            scroll_elements = settings.scroll_elements;
            click_elements = settings.click_elements;
            universal = settings.universal;
            gtm = settings.gtm;
            gst = settings.gst;

            var i;
            for (i = 0; i < click_elements.length; i++) {
                var clicked = click_elements[i];
                var selector = unescapeChars(clicked.select);

                clicked.universal = universal;
                //$(selector).on('click', clicked, click_event);
                $('body').on('click', selector, clicked, click_event);
            }



            $(window).scroll(function () {
                var ga_window = $(window).height();
                var ga_scroll_top = $(document).scrollTop();
                var i;
                for (i = 0; i < scroll_elements.length; i++) {
                    if (!scroll_elements[i].sent) {
                        scroll_elements[i].offset =  $( unescapeChars( scroll_elements[i].select) ).offset();
                        if (scroll_elements[i].offset && ga_scroll_top + ga_window >= scroll_elements[i].offset.top + $(scroll_elements[i].select).height()) {
                            track_event(scroll_elements[i].category, scroll_elements[i].action, scroll_elements[i].label, universal, scroll_elements[i].bounce, scroll_elements[i].evalue);
                            scroll_elements[i].sent = true;
                        }
                    }
                }
            });
        }
    };

}(jQuery));
;
/******************************************
* Instagram Feed Carousel
* Author: Ryan Mahabir
* Date: 2017-20-11
******************************************/
function instagramCarousel(element){
    
    /******************************************
	* Variables
	******************************************/
    this.username = 'username';
    this.$element = jQuery( element );
    this.$domNode = '#instagram-feed';
    this.$instagramCarousel = this.$element.find( this.$domNode );

    // 12 recent posts is the limit without Instagram access-token
    this.instagramLimit = 17;
    this.feedLimitDesktop = 6;
    this.feedLimitTablet = 4;
    this.feedLimitMobile = 1;

    this.instagramCaptionFlag = true;
    //this.instagramCaptionArray = [];

    /******************************************
	* WordPress user settings
	******************************************/
    this.init = function(name, feed) {
    	//console.log('username: ' + name + ' feed: ' + feed);
    	var flag = true;

    	//WIP - Need to solidify booleans.
    	if (feed === 'false') {
    		flag = false;
    		return;
    	}

		if (this.$instagramCarousel.length) {
			//console.log('found dom node');
			//this.getInstagram(name, flag);
			return;
		}
        
	};
	

	/******************************************
	* New - Wordpress init function
	******************************************/
	this.init_2 = function(data, instagram_params) {
		//console.log('JSON Data: ' + data);
		//console.log('Wordpress Data: ', instagram_params);
		
		//WIP - Need to solidify booleans.
		var flag = true;
    	if (instagram_params.showFeed === 'false') {
			console.log('Instagram is hidden');
			
			flag = false;
    		return;
    	}

		if (this.$instagramCarousel.length) {
			//console.log('found dom node');
			this.getWR1_Instagram(data, flag, instagram_params);
			return;
		}
        
	};
	

    /******************************************
	* WIP - Create DOM nodes
	******************************************/
	/*this.createDOM = function() {
		var container = document.createElement('div'),
			btn = document.createElement('a'),
			video = document.createElement('div'),
			img = document.createElement('img'),

			overlay = document.createElement('div'),
			overlayInfo = document.createElement('div'),
			caption = document.createElement('p'),
			comments = document.createElement('p'),
			likes = document.createElement('p');

		return;
	};*/


	/******************************************
	* Caption character limit global var
	******************************************/
	this.getCharacterFlag = function(flag) {
		this.instagramCaptionFlag = flag;
		return this.instagramCaptionFlag;
	};


	/******************************************
	* Caption rerender on window resize
	******************************************/
	/*this.reRenderInstagramCaption = function(flag) {
		var self = this;

		if (flag === true) {
			//console.log('Mobile orientation change. Truncate caption: ', flag);
		}
	};*/



	/******************************************
	* Add events
	******************************************/
	this.addEvents = function() {
		var self = this;

		jQuery(self.$instagramCarousel).on('init', function(breakpoint, slick){
			var clientWidth = breakpoint.target.clientWidth;
			//console.log('Slider was initialized. Width:', clientWidth);

			if (clientWidth < 735) {
				var flag = self.getCharacterFlag(false);
				//console.log('We are showing one slide, caption truncation: ', flag);
			}

		});

		
		/******************************************
		* Caption truncation on MOBILE ONLY orientation.
		******************************************/
		// Mobile orientation change
		/*jQuery( window ).on( "orientationchange", function( event ) {
			console.log(event.orientation);
		});

		jQuery( window ).resize(function() {
			//console.log(window.innerWidth);
		 	var windowWidth = window.innerWidth;

			if (windowWidth > 700) {
				self.reRenderInstagramCaption(true);
			}
		  
		});*/

	};


    /******************************************
	* Get JSON from Instagram API
	******************************************/
    this.getInstagram = function(username, flag) {

		var url = 'https://api.instagram.com/'+ username + '?__a=1',
			//url = 'https://api.instagram.com/v1/users/search?user_id=37980436&client_id=48f55ad9719140eea4769f8f6fae6b29',
    		self = this;

    	/******************************************
		* Get JSON feed from Instagram
		******************************************/
    	jQuery.getJSON( url, {
			format: 'json'
		})
		.fail(function(err) {
			console.log( 'Instagram error:', err.status );
		})
		.done(function( data ) {
			//console.log(data.graphql.user.id);

			/******************************************
			* Instagram data
			******************************************/
			var userID = data.graphql.user.id,
				username = data.graphql.user.username,
				userImages = data.graphql.user.edge_owner_to_timeline_media.edges,
				isPrivate = data.graphql.user.is_private;

			//console.log(userImages);
			/******************************************
			* Private Instagram account message.
			******************************************/
			if (isPrivate) {
				console.log('Private Instagram account: ', isPrivate);
				return;
			}

			/******************************************
			* Init Slick.js carousel.
			******************************************/
			self.$instagramCarousel.slick({
				slidesToShow: self.feedLimitDesktop,
				slidesToScroll: 3,
				autoplay: true,
				autoplaySpeed: 4000,

				// To use lazy loading, set a data-lazy attribute
				// on your img tags and leave off the src
				lazyLoad: 'ondemand',
				adaptiveHeight: false,
				centerMode: false,
				centerPadding: '0px',
  				variableWidth: false,
  				fade: false,
  				cssEase: 'linear',

				dots: false,
				infinite: true,
				speed: 300,

				responsive: [{
					// Desktop
					breakpoint: 1024,
					  	settings: {
					    	slidesToShow: self.feedLimitDesktop,
					    	slidesToScroll: 3,
					    	//infinite: false,
					    	//dots: false
					  	}
					},

					// Tablet - landscape
					{breakpoint: 800,
						settings: {
					    	slidesToShow: self.feedLimitTablet,
					    	slidesToScroll: 2
					  	}
					},

					// Mobile - landscape
					{breakpoint: 700,
						settings: {
					    	slidesToShow: self.feedLimitTablet,
					    	slidesToScroll: 2
					  	}
					},

					// Mobile - portrait
					{breakpoint: 636,
					  	settings: {
					    	slidesToShow: self.feedLimitMobile,
					   		slidesToScroll: 1
					  	}
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				]
			});


			/******************************************
			* Loop through each media asset.
			******************************************/
			jQuery.each( userImages, function( i, item ) {
				//console.log('Images: ' + item);

				var imageLink = userImages[i].node.thumbnail_src,
					
					//imageCaption = userImages[i].node.edge_media_to_caption.edges,
					imageCaption = '',
					postCaption = userImages[i].node.edge_media_to_caption.edges,
					
					imageComments = userImages[i].node.edge_media_to_comment.count,
					imageLikes = userImages[i].node.edge_liked_by.count,
					isVideo = userImages[i].node.is_video;

				if (postCaption.length) {
					//console.log(postCaption[0].node.text);
					imageCaption = postCaption[0].node.text;
				}
				
				/*jQuery.each( imageCaption, function( i, item ) {
					console.log('Caption: ' + item);
				});*/

				/*var videoLinkCode = userImages[i].code,
					videoLink = userImages[i].display_src,
					videoResultPre = videoLink.replace('t51', 'vp/t50'),
					videoResultFinal = videoResultPre.replace('jpg', 'mp4');*/
					//truncateCaption = self.truncate(imageCaption);	


				/******************************************
				* Caption truncation based on instagramCaptionFlag.
				******************************************/
				// Save entire caption for mobile view.
				//self.instagramCaptionArray.push(imageCaption);
				//console.log(self.instagramCaptionArray);

				//console.log('Should limit: ', self.instagramCaptionFlag);
				if (typeof imageCaption === 'undefined') {
					imageCaption = '';
				}

				if (self.instagramCaptionFlag) {
					//console.log(imageCaption);
					imageCaption = self.truncate(imageCaption);

					/*if (typeof imageCaption === 'undefined') {
						console.log('NO COMMENT BY OWNER');
						imageCaption = '';
					} else {
						//console.log('COMMENT BY OWNER');
						imageCaption = self.truncate(imageCaption);
					}*/
				}


				//console.log("userID: " + userID + " username: " + username + " imageCaption: " + imageCaption + " imageLink: " + imageLink + " comments: " + comments + " likes: " + likes);


				/******************************************
				* Render ONLY where DOM element lives.
				******************************************/
				if ( self.$instagramCarousel.length && flag ) {
					//console.log('Found the DOM element');

					if (isVideo) {

						/*jQuery(self.$instagramCarousel).slick('slickAdd',
							'<div class="instagram-post">
								<a href="https://instagram.com/' + username + '" class="user">
									<div class="video"></div>
									<img data="' + imageLink + '" src="' + imageLink + '" class="mediaImg"/>
									
									<div class="overlay">
										<div class="info">
											<p class="caption">' + imageCaption + '</p>
											<p class="comments">Comments: ' + comments + '</p>
											<p class="likes">Likes: ' + likes + '</p>
										</div>
									</div>
								</a>
							</div>');*/

						/******************************************
						* Create DOM nodes for carousel
						******************************************/
						var container = document.createElement('div'),
							btn = document.createElement('a'),
							video = document.createElement('div'),
							img = document.createElement('img'),

							overlay = document.createElement('div'),
							overlayInfo = document.createElement('div'),
							caption = document.createElement('p'),
							comments = document.createElement('p'),
							commentsIcon = document.createElement('span'),
							likes = document.createElement('p'),
							likesIcon = document.createElement('span');


						/* console.log(videoResultFinal);
						t50. = Video mp4
						image
						https://scontent-lga3-1.cdninstagram.com/t51.2885-15/e15/23668084_132422104124705_5493594476771278848_n.jpg
						
						video
						https://scontent-lga3-1.cdninstagram.com/vp/t50.2886-16/23727841_131340950908418_8435699043635560448_n.mp4

						<video class="_l6uaz" playsinline="" 
							poster="https://scontent-lga3-1.cdninstagram.com/t51.2885-15/e15/23668084_132422104124705_5493594476771278848_n.jpg" 
							preload="none" 
							src="https://scontent-lga3-1.cdninstagram.com/vp/ec5e962533d95397e16c3344f9159643/5A1864E7/t50.2886-16/23727841_131340950908418_8435699043635560448_n.mp4" 
							type="video/mp4">
						</video>
						*/


						// Instagram individual posts
						container.setAttribute('class', 'instagram-post');
						video.setAttribute('class', 'instagram-video');
						btn.setAttribute('class', 'instagram-user-link');
						btn.setAttribute('target', '_blank');
						
						// Link to open video in Instagram
						//btn.href = 'https://www.instagram.com/p/' + videoLinkCode + '/?taken-by=' + username;

						// Link to open video only
						//btn.href = videoResultFinal;

						img.setAttribute('class', 'instagram-media');
						img.src = imageLink;

						// To stop anchor, add 'img' to container instead of 'btn'
						//btn.appendChild(img);
						container.appendChild(video);
						container.appendChild(img);

						// Instagram individual overlays
						overlay.setAttribute('class', 'instagram-overlay');
						overlayInfo.setAttribute('class', 'instagram-info');
						caption.setAttribute('class', 'instagram-caption');
						comments.setAttribute('class', 'instagram-comments');
						commentsIcon.setAttribute('class', 'instagram-comments-icon');
						likes.setAttribute('class', 'instagram-likes');
						likesIcon.setAttribute('class', 'instagram-likes-icon');

						//caption.textContent = '"' + imageCaption + '"';
						if (imageCaption === '') {
							caption.textContent = '';
						} else {
							caption.textContent = '"' + imageCaption + '"';
						}
						
						comments.textContent = imageComments;
						likes.textContent = imageLikes;

						comments.appendChild(commentsIcon);
						likes.appendChild(likesIcon);

						overlayInfo.appendChild(caption);
						overlayInfo.appendChild(comments);
						overlayInfo.appendChild(likes);
						overlay.appendChild(overlayInfo);

						// Add overlay to post
						container.appendChild(overlay);

						// Add posts to carousel
						jQuery(self.$instagramCarousel).slick('slickAdd', container);

					} else {

						/*jQuery(self.$instagramCarousel).slick('slickAdd',
							'<div class="instagram-post">
								<a href="https://instagram.com/' + username + '" class="user">
									<img data="' + imageLink + '" src="' + imageLink + '" class="mediaImg"/>
									<div class="overlay">
										<div class="info">
											<p class="caption">' + imageCaption + '</p>
											<p class="comments">Comments: ' + comments + '</p>
											<p class="likes">Likes: ' + likes + '</p>
										</div>
									</div>
								</a>
							</div>');*/

						/******************************************
						* Create DOM nodes for carousel
						******************************************/
						var container = document.createElement('div'),
							btn = document.createElement('a'),
							img = document.createElement('img'),

							overlay = document.createElement('div'),
							overlayInfo = document.createElement('div'),
							caption = document.createElement('p'),
							comments = document.createElement('p'),
							commentsIcon = document.createElement('span'),
							likes = document.createElement('p'),
							likesIcon = document.createElement('span');

						// Instagram individual posts
						container.setAttribute('class', 'instagram-post');
						btn.setAttribute('class', 'instagram-user');
						btn.setAttribute('target', '_blank');
						btn.href = 'https://instagram.com/' + username;
						img.setAttribute('class', 'instagram-media');
						img.src = imageLink;

						btn.appendChild(img);

						// To stop anchor, add 'img' to container instead of 'btn'
						container.appendChild(img);

						// Instagram individual overlays
						overlay.setAttribute('class', 'instagram-overlay');
						overlayInfo.setAttribute('class', 'instagram-info');
						caption.setAttribute('class', 'instagram-caption');
						comments.setAttribute('class', 'instagram-comments');
						commentsIcon.setAttribute('class', 'instagram-comments-icon');
						likes.setAttribute('class', 'instagram-likes');
						likesIcon.setAttribute('class', 'instagram-likes-icon');

						//caption.textContent = '"' + imageCaption + '"';
						if (imageCaption === '') {
							caption.textContent = '';
						} else {
							caption.textContent = '"' + imageCaption + '"';
						}
						
						comments.textContent = imageComments;
						likes.textContent = imageLikes;

						comments.appendChild(commentsIcon);
						likes.appendChild(likesIcon);

						overlayInfo.appendChild(caption);
						overlayInfo.appendChild(comments);
						overlayInfo.appendChild(likes);
						overlay.appendChild(overlayInfo);

						// Add overlay to post
						container.appendChild(overlay);

						// Add posts to carousel
						jQuery(self.$instagramCarousel).slick('slickAdd', container);

					}
				}

				/******************************************
				* To limit the amount of images rendered.
				******************************************/
				if ( i === self.instagramLimit ) {
				  return false;
				}

			});

			/******************************************
			* Add header to Instagram feed.
			******************************************/
			jQuery(self.$instagramCarousel).before('<div class="instagram-follow"><h3><a href="https://instagram.com/' + username + '" target="_blank">@ Follow me on Instagram</a></h3></div>');
			
		});

		/******************************************
		* Add events
		******************************************/
		this.addEvents();
	};
	

	/******************************************
	* Get Instagram JSON from WR1 API
	******************************************/
	this.getWR1_Instagram = function(instagram_json, flag, wp_params) {
		//console.log(instagram_json);
		//console.log(wp_params);

		var self = this,
			data = JSON.parse(instagram_json),
			username = wp_params.username;
		//console.log(username);


		/******************************************
		* Error check
		******************************************/
		if (club_id_error === 'true') {
			//console.log('Wrong club ID or no posts.');
			$alert_msg = '<h4 class="alert_error">Instagram Carousel plug-in not working.</h4><p class="alert_error">Please contact support.</p>';
			jQuery(self.$instagramCarousel).append( $alert_msg );
			return;
		}


		/******************************************
		* Add events
		******************************************/
		this.addEvents();


		/******************************************
		* Init Slick.js carousel.
		******************************************/
		self.$instagramCarousel.slick({
			slidesToShow: self.feedLimitDesktop,
			slidesToScroll: 3,
			autoplay: true,
			autoplaySpeed: 4000,

			// To use lazy loading, set a data-lazy attribute
			// on your img tags and leave off the src
			//lazyLoad: 'ondemand',
			lazyLoad: 'progressive',
			adaptiveHeight: false,
			centerMode: false,
			centerPadding: '0px',
			variableWidth: false,
			fade: false,
			cssEase: 'linear',

			dots: false,
			infinite: true,
			speed: 300,

			responsive: [{
				// Desktop
				breakpoint: 1024,
					settings: {
						slidesToShow: self.feedLimitDesktop,
						slidesToScroll: 3,
						//infinite: false,
						//dots: false
					}
				},

				// Tablet - landscape
				{breakpoint: 800,
					settings: {
						slidesToShow: self.feedLimitTablet,
						slidesToScroll: 2
					}
				},

				// Mobile - landscape
				{breakpoint: 700,
					settings: {
						slidesToShow: self.feedLimitTablet,
						slidesToScroll: 2
					}
				},

				// Mobile - portrait
				{breakpoint: 636,
					settings: {
						slidesToShow: self.feedLimitMobile,
						slidesToScroll: 1
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			]
		});


		/******************************************
		* Check if image URL is a 403
		******************************************/
		self.$isUrlExists = function(url, cb) {
			jQuery.ajax({
				url:      url,
				dataType: 'text',
				type:     'GET',
				complete:  function(xhr){
					if(typeof cb === 'function')
					   cb.apply(this, [xhr.status]);
				}
			});
		};

		
		/******************************************
		* Loop through json
		******************************************/
		for (var key in data) {
			
			if (data.hasOwnProperty(key)) {
				//console.log(key + " -> " + data);
				
				if (key === '0') {
					//console.log(key + " -> " + data);
					
					/******************************************
					* Instagram data
					******************************************/
					var instagramPost_Property = 'customData',
						instagramData = JSON.parse(data),
						instagramPosts = instagramData.posts;
					//console.log('posts: ', instagramPosts);


					/******************************************
					* Loop through all posts
					******************************************/
					jQuery.each( instagramPosts, function( i, item ) {
						//console.log('index: ' , i, 'item: ' , item);
						var wr1_instagram_posts = instagramPosts[i].customData,
							instagram_postType = instagramPosts[i].type,
							instagram_comments = instagramPosts[i].commentCount,
							instagram_likes = instagramPosts[i].likeCount,
							instagram_image = instagramPosts[i].thumbnailUrl,
							//instagram_image_wip = self.resizeIMG(instagram_image_orig),
							instagram_caption = instagramPosts[i].text,
							instagram_videoURL = instagramPosts[i].url;

						// View all posts with customData property
						//console.log('wr1 instagram posts: ', wr1_instagram_posts);


						/******************************************
						* To limit the amount of images rendered.
						******************************************/
						if ( i === self.instagramLimit ) {
							return false;
						}


						/******************************************
						* Check if image URL is a 403
						******************************************/
						//console.log('wr1 instagram image: ', instagram_image);
						self.$isUrlExists(instagram_image, function(status){

							if (status === 200) {
							   	//console.log('file was found');

								/******************************************
								* Start rendering.
								******************************************/
								if (item.hasOwnProperty(instagramPost_Property) && wr1_instagram_posts !== undefined && wr1_instagram_posts !== '' ) {
									/*console.log('instagram post ', i, ': ', item);
									console.log('instagram post type: ', instagram_postType);
									console.log('instagram comments: ', instagram_comments);
									console.log('instagram likes: ', instagram_likes);
									console.log('instagram image: ', instagram_image);
									console.log('instagram caption: ', instagram_caption);
									console.log('instagram video url: ', instagram_videoURL);
									console.log('wr1 instagram posts: ', wr1_instagram_posts);*/
									
									if (self.instagramCaptionFlag) {
										//console.log(instagram_caption);
										instagram_caption = self.truncate(instagram_caption);
									}
		
									/******************************************
									* Render ONLY where DOM element lives.
									******************************************/
									if ( self.$instagramCarousel.length && flag ) {
										//console.log('Found the DOM element');
		
										if (instagram_postType == 'VIDEO') {
		
											/******************************************
											* Create DOM nodes for carousel
											******************************************/
											var container = document.createElement('div'),
												btn = document.createElement('a'),
												video = document.createElement('div'),
												img = document.createElement('img'),
		
												overlay = document.createElement('div'),
												overlayInfo = document.createElement('div'),
												caption = document.createElement('p'),
												comments = document.createElement('p'),
												commentsIcon = document.createElement('span'),
												likes = document.createElement('p'),
												likesIcon = document.createElement('span');
		
		
											// Instagram individual posts
											container.setAttribute('class', 'instagram-post');
											video.setAttribute('class', 'instagram-video');
											btn.setAttribute('class', 'instagram-user-link');
											btn.setAttribute('target', '_blank');
											
											// Link to open video in Instagram
											//btn.href = 'https://www.instagram.com/p/' + instagram_videoURL + '/?taken-by=' + username;
		
											// Link to open video only
											btn.href = instagram_videoURL;
		
											img.setAttribute('class', 'instagram-media');
											img.src = instagram_image;
		
											// To stop anchor, add 'img' to container instead of 'btn'
											//btn.appendChild(img);
											container.appendChild(video);
											container.appendChild(img);
		
											// Add link to video
											/*btn.appendChild(img);
											container.appendChild(btn);*/
		
											// Instagram individual overlays
											overlay.setAttribute('class', 'instagram-overlay');
											overlayInfo.setAttribute('class', 'instagram-info');
											caption.setAttribute('class', 'instagram-caption');
											comments.setAttribute('class', 'instagram-comments');
											commentsIcon.setAttribute('class', 'instagram-comments-icon');
											likes.setAttribute('class', 'instagram-likes');
											likesIcon.setAttribute('class', 'instagram-likes-icon');
		
											//caption.textContent = '"' + imageCaption + '"';
											if (instagram_caption === '') {
												caption.textContent = '';
											} else {
												caption.textContent = '"' + instagram_caption + '"';
											}
											
											comments.textContent = instagram_comments;
											likes.textContent = instagram_likes;
		
											comments.appendChild(commentsIcon);
											likes.appendChild(likesIcon);
		
											overlayInfo.appendChild(caption);
											overlayInfo.appendChild(comments);
											overlayInfo.appendChild(likes);
											overlay.appendChild(overlayInfo);
		
											// Add overlay to post
											container.appendChild(overlay);
		
											// Add posts to carousel
											jQuery(self.$instagramCarousel).slick('slickAdd', container);
		
										} else {
		
											/******************************************
											* Create DOM nodes for carousel
											******************************************/
											var container = document.createElement('div'),
												btn = document.createElement('a'),
												img = document.createElement('img'),
		
												overlay = document.createElement('div'),
												overlayInfo = document.createElement('div'),
												caption = document.createElement('p'),
												comments = document.createElement('p'),
												commentsIcon = document.createElement('span'),
												likes = document.createElement('p'),
												likesIcon = document.createElement('span');
		
											// Instagram individual posts
											container.setAttribute('class', 'instagram-post');
											btn.setAttribute('class', 'instagram-user');
											btn.setAttribute('target', '_blank');
											btn.href = 'https://instagram.com/' + username;
											img.setAttribute('class', 'instagram-media');
											img.src = instagram_image;
		
											btn.appendChild(img);
		
											// To stop anchor, add 'img' to container instead of 'btn'
											container.appendChild(img);
		
											// Instagram individual overlays
											overlay.setAttribute('class', 'instagram-overlay');
											overlayInfo.setAttribute('class', 'instagram-info');
											caption.setAttribute('class', 'instagram-caption');
											comments.setAttribute('class', 'instagram-comments');
											commentsIcon.setAttribute('class', 'instagram-comments-icon');
											likes.setAttribute('class', 'instagram-likes');
											likesIcon.setAttribute('class', 'instagram-likes-icon');
		
											//caption.textContent = '"' + imageCaption + '"';
											if (instagram_caption === '') {
												caption.textContent = '';
											} else {
												caption.textContent = '"' + instagram_caption + '"';
											}
											
											comments.textContent = instagram_comments;
											likes.textContent = instagram_likes;
		
											comments.appendChild(commentsIcon);
											likes.appendChild(likesIcon);
		
											overlayInfo.appendChild(caption);
											overlayInfo.appendChild(comments);
											overlayInfo.appendChild(likes);
											overlay.appendChild(overlayInfo);
		
											// Add overlay to post
											container.appendChild(overlay);
		
											// Add posts to carousel
											jQuery(self.$instagramCarousel).slick('slickAdd', container);
		
										}
									}
		
								}


							} else if (status === 404) {
							   //console.log('404 file not found');
							   return;
							}

						});
					
					});

				}
			}	
		}


		/******************************************
		* Add header to Instagram feed.
		******************************************/
		jQuery(self.$instagramCarousel).before('<div class="instagram-follow"><h3><a href="https://instagram.com/' + username + '" target="_blank">@ Follow on Instagram</a></h3></div>');

	};


    /******************************************
	* Truncate string with ellipsis (...)
	******************************************/
	this.truncate = function(string) {

		var self = this,
			characters = 50;
		
		//console.log(string);

		if ( typeof(string) !== 'undefined' && string.length > characters ) {
			return string.substring(0, characters) + '...';
		} else if ( typeof(string) === 'undefined' ) {
			string = ' ';
			return string;
		} else {
			return string;
		}

	};


	/******************************************
	* WIP - Resizing image
	* Very slow rendering
	******************************************/
	this.resizeIMG = function(image_url) {
		
		var newImage = new Image,
			paramOperation = "square",
			paramValue = 240, // Square size in pixels.
			imageURL = image_url;
		
		/*newImage.onload = function() {
			var imgElement = document.getElementById("img1");
			imgElement.src = this.src;
		}*/
		
		newImage.src = "http://api.rethumb.com/v1/" + paramOperation + "/" + paramValue + "/" + imageURL;

		return newImage.src;
	};


};


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
		//console.log('ready');

		/*function init(el, username, feed) {
			var newInstagramFeed = new instagramCarousel( el );
			newInstagramFeed.init(username, feed);
		}
		init('body', username, feed);*/


		//console.log('Instagram Params', instagram_params);
		//console.log('Instagram Options', instagram_options);

		function init_2(el, data) {
			var newInstagramFeed = new instagramCarousel( el );
			newInstagramFeed.init_2(instagram_options, instagram_params);
		}
		init_2('body', instagram_options);

	});
}(jQuery));



;
/******************************************
* Ticket List Display
* Author: Ryan Mahabir
* Date: 2018-01-28
******************************************/


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
        //console.log('ready');

		var ticketList = [];	  
        //console.log(shop_options);
        
		//$('#event-ticket-display').append(shop_options)
		var userLimit = $('#event-ticket-display').data( "attr" );
		//console.log('UserLimit: ' + userLimit);

		$.each( shop_options, function( key, value ) {
			//console.log( key + ": " + value );

			if (userLimit === -1) {
				//console.log('userlimit is null');
				
				ticketList.push(value);
			} else if (key > userLimit-1) {
				//console.log('hit limit');

				return;
			} else {
				ticketList.push(value);
			}
		});

        $('#event-ticket-display').append(ticketList);
        
        
		/*function init(el, username, feed) {
			var newInstagramFeed = new instagramCarousel( el );
			newInstagramFeed.init(username, feed);
        }*/
        
        //init('body', username, feed);


	});
}(jQuery));

;
window.FBL = {};
function fbl_loginCheck() {
    window.FB.getLoginStatus( function(response) {
        FBL.handleResponse(response);
    } );
}
(function( $ ) {
	'use strict';
	FBL.renderFinish = function () {
        $('.fbl-spinner').hide();
    }

    FBL.handleResponse = function( response ) {
        var button          =  $('.fbl-button'),
            $form_obj       = button.parents('form') || false,
            $redirect_to    = $form_obj.find('input[name="redirect_to"]').val() || button.data('redirect'),
            running         = false;
        /**
         * If we get a successful authorization response we handle it
         */
        if (response.status == 'connected' && ! running) {
            running = true;
            var fb_response = response;
            $('.fbl-spinner').fadeIn();
            $('.fb-login-button').hide();

            /**
             * Send the obtained token to server side for extra checks and user data retrieval
             */
            $.ajax({
                data: {
                    action: "fbl_facebook_login",
                    fb_response: fb_response,
                    security: button.data('fb_nonce')
                },
                global: false,
                type: "POST",
                url: fbl.ajaxurl,
                success: function (data) {
                    if (data && data.success) {
                        if( data.redirect && data.redirect.length ) {
                            location.href = data.redirect;
                        } else if ( $redirect_to.length ) {
                            location.href = $redirect_to;
                        } else {
                            location.href = fbl.site_url;
                        }
                    } else if (data && data.error) {
                        $('.fbl-spinner').hide();
                        $('.fb-login-button').show();

                        if ($form_obj.length) {
                            $form_obj.append('<p class="fbl_error">' + data.error + '</p>');
                        } else {
                            // we just have a button
                            $('<p class="fbl_error">' + data.error + '</p>').insertAfter(button);
                        }
                        // if we had any error remove user from app so we request again permissions
                        FB.api(
                            "/"+data.fb.id+"/permissions",
                            "DELETE",
                            function (response) {
                                if (response && !response.error) {
                                }
                            }
                        );
                    }
                },
                error: function (data) {
                    $('.fbl-spinner').hide();
                    $('.fb-login-button').show();
                    $form_obj.append('<p class="fbl_error">' + data + '</p>');
                }
            });

        } else {
            button.removeClass('fbl-loading');
            if( navigator.userAgent.match('CriOS') )
                window.open('https://www.facebook.com/dialog/oauth?client_id=' + fbl.appId + '&redirect_uri=' + document.location.href + '&scope='+fbl.scopes, '', null);

            if( "standalone" in navigator && navigator.standalone )
                window.location.assign('https://www.facebook.com/dialog/oauth?client_id=' + fbl.appId + '&redirect_uri=' + document.location.href + '&scope='+fbl.scopes);
        }
    };
})( jQuery );
;
window.jQuery( document ).ready( function ( $ ) {
	$( 'body' ).on( 'adding_to_cart', function ( event, $button, data ) {
		$button && $button.hasClass( 'vc_gitem-link' ) && $button
			.addClass( 'vc-gitem-add-to-cart-loading-btn' )
			.parents( '.vc_grid-item-mini' )
			.addClass( 'vc-woocommerce-add-to-cart-loading' )
			.append( $( '<div class="vc_wc-load-add-to-loader-wrapper"><div class="vc_wc-load-add-to-loader"></div></div>' ) );
	} ).on( 'added_to_cart', function ( event, fragments, cart_hash, $button ) {
		if ( 'undefined' === typeof($button) ) {
			$button = $( '.vc-gitem-add-to-cart-loading-btn' );
		}
		$button && $button.hasClass( 'vc_gitem-link' ) && $button
			.removeClass( 'vc-gitem-add-to-cart-loading-btn' )
			.parents( '.vc_grid-item-mini' )
			.removeClass( 'vc-woocommerce-add-to-cart-loading' )
			.find( '.vc_wc-load-add-to-loader-wrapper' ).remove();
	} );
} );
;
/*! * JavaScript Cookie v2.0.0-pre * https://github.com/js-cookie/js-cookie * * Copyright 2006, 2015 Klaus Hartl * Released under the MIT license */(function (factory) {    if (typeof define === 'function' && define.amd) {        define(factory);    } else if (typeof exports === 'object') {        module.exports = factory();    } else {        var _OldCookies = window.Cookies;        var api = window.Cookies = factory(window.jQuery);        api.noConflict = function () {            window.Cookies = _OldCookies;            return api;        };    }}(function () {    function extend () {        var i = 0;        var result = {};        for (; i < arguments.length; i++) {            var attributes = arguments[ i ];            for (var key in attributes) {                result[key] = attributes[key];            }        }        return result;    }    function init (converter) {        function api (key, value, attributes) {            var result;            // Write            if (arguments.length > 1) {                attributes = extend({                    path: '/'                }, api.defaults, attributes);                if (typeof attributes.expires === 'number') {                    var expires = new Date();                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);                    attributes.expires = expires;                }                try {                    result = JSON.stringify(value);                    if (/^[\{\[]/.test(result)) {                        value = result;                    }                } catch (e) {}                value = encodeURIComponent(String(value));                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);                key = encodeURIComponent(String(key));                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);                key = key.replace(/[\(\)]/g, escape);                return (document.cookie = [                    key, '=', value,                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE                    attributes.path    && '; path=' + attributes.path,                    attributes.domain  && '; domain=' + attributes.domain,                    attributes.secure  && '; secure'                ].join(''));            }            // Read            if (!key) {                result = {};            }            // To prevent the for loop in the first place assign an empty array            // in case there are no cookies at all. Also prevents odd result when            // calling "get()"            var cookies = document.cookie ? document.cookie.split('; ') : [];            var rdecode = /(%[0-9A-Z]{2})+/g;            var i = 0;            for (; i < cookies.length; i++) {                var parts = cookies[i].split('=');                var name = parts[0].replace(rdecode, decodeURIComponent);                var cookie = parts.slice(1).join('=');                if (cookie.charAt(0) === '"') {                    cookie = cookie.slice(1, -1);                }                cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);                if (this.json) {                    try {                        cookie = JSON.parse(cookie);                    } catch (e) {}                }                if (key === name) {                    result = cookie;                    break;                }                if (!key) {                    result[name] = cookie;                }            }            return result;        }        api.get = api.set = api;        api.getJSON = function () {            return api.apply({                json: true            }, [].slice.call(arguments));        };        api.defaults = {};        api.remove = function (key, attributes) {            api(key, '', extend(attributes, {                expires: -1            }));        };        api.withConverter = init;        return api;    }    return init();}));;
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
(function($, window, document) {
    "use strict";

    $.yit_popup = function(element, options) {

        var defaults = {
            'popup_class' : 'ypop',
            'content' : '',
            'delay' : 0,
            'position': 'center',
            'mobile' : false
        };

        var self = this;

        self.settings = {};

        var $element = $(element),
            overlay = null,
            popup = null,
            close = null;

        self.init = function() {
            self.settings = $.extend({}, defaults, options);

            _createElements();
            _initEvents();

        };

        var _initEvents = function() {
                $(document).on('touchstart click', '.ypop-overlay', function(e){
                    if( $( e.target).hasClass('close') || $( e.target ).parents( '.ypop-overlay' ).length == 0 ) {
                        _close();
                    }
                }).on('keyup', function(e) {
                        if (e.keyCode == 27) {
                            _close();
                        }
                    }).on('click', '.ypop-wrapper a.close', function () {
                        _close();
                    });

                $(window).on('resize', function(){
                    _center();
                });

                $('html').removeClass('yit-opened');

                _open();
            },

            _createElements = function() {
				if( $('body').find('.ypop-modal').length == 0 ) {
					self.bigwrapper = $('<div />', {
						'class' : 'ypop-modal'
					}).prependTo('body');
				} else {
					self.bigwrapper = $('body').find('.ypop-modal');
				}

				if( self.bigwrapper.find('.ypop-overlay').length == 0 ) {
                    self.overlay = $('<div />', {
                        'class' : 'ypop-overlay'
                    }).appendTo(self.bigwrapper);
                } else {
                    self.overlay = self.bigwrapper.find('.ypop-overlay');
                }

                if( self.bigwrapper.find('.ypop-wrapper').length == 0 ) {
                    self.popup = $('<div />', {
                        'class' : 'ypop-wrapper ' + self.settings.popup_class
                    }).appendTo( self.bigwrapper );
                } else {
                    self.popup = self.bigwrapper.find('.ypop-wrapper');
                }

                if( self.popup.find('.close').length == 0 ) {
                    self.close = $('<a />', {
                        'class' : 'close'
                    }).appendTo( self.popup );
                } else {
                    self.close = self.popup.find('.close');
                }
            },

            _center = function() {

                var w = self.popup.outerWidth(),
                    h = self.popup.outerHeight();

                if( self.settings.mobile ){
                    if ( self.popup.outerHeight() > ( jQuery(window).height()  - 120 ) ||self.popup.outerWidth() > ( jQuery(window).width()  - 60 ) ){
                         w = jQuery(window).width()  - 60;
                    }

                    self.popup.css({
                        width: w + "px",
                        height: 'auto'
                    });
                }
                if( self.settings.position == 'center'){
                    self.popup.css({
                        position: 'fixed',
                        top: Math.max(0, ((jQuery(window).height() - self.popup.outerHeight()) / 2) - 50 ) + "px",//'15%',
                        left: Math.max(0, ((jQuery(window).width() - w) / 2) ) + "px"
                    });
                }else if( self.settings.position == 'left-top' ){
                    self.popup.css({
                        position: 'fixed',
                        top:  " 0px",//'15%',
                        left: "0px"
                    });
                }else if( self.settings.position == 'left-bottom' ){
                    self.popup.css({
                        position: 'fixed',
                        bottom:  " 0px",//'15%',
                        left: "0px"
                    });
                }else if( self.settings.position == 'right-bottom' ){
                    self.popup.css({
                        position: 'fixed',
                        bottom:  " 0px",//'15%',
                        right: "0px"
                    });
                }else if( self.settings.position == 'right-top' ){
                    self.popup.css({
                        position: 'fixed',
                        top:  " 0px",//'15%',
                        right: "0px"
                    });
                }
            },

            _open = function() {
                if( self.settings.delay > 0){
                   setInterval( function(){
                       _content();
                       $('.ypop-modal').addClass('open');
                       _center();
                   }, self.settings.delay) ;
                }else{
                    _content();
                    $('.ypop-modal').addClass('open');
                    _center();
                }


                //_center();
              //  self.overlay.css({ 'display': 'block', opacity: 0 }).animate({ opacity: 1 }, 500);
              //  $('html').addClass('yit-opened');

            },

            _close = function() {
                //self.overlay.css({ 'display': 'none', opacity: 1 }).animate({ opacity: 0 }, 500);
                $element.trigger('close.ypop');
                //$('html').removeClass('yit-opened');
				$('.ypop-modal').removeClass('open');
                _destroy();
            },

            _destroy = function() {
                self.popup.remove();
                self.overlay.remove();

                //self.popup = self.overlay = null;
                //$element.removeData('yit_popup');
            },

            _content = function() {
                if( self.settings.content != '' ) {
                    self.popup.html( self.settings.content );
                } else if( $element.data('container') ) {
                    self.popup.html( $($element.data('container')).html() );
                } else if( $element.data('content') ) {
                    self.popup.html( $element.data('content') );
                } else if( $element.attr('title') ) {
                    self.popup.html( $element.attr('title') );
                } else {
                    self.popup.html('');
                }

                //update <input id="" /> and <label for="">
                self.popup.find('form, input, label, a').each(function(){
                    if( typeof $(this).attr('id') != 'undefined' ) {
                        var id = $(this).attr('id') + '_ypop';
                        $(this).attr('id', id);
                    }

                    if( typeof $(this).attr('for') != 'undefined' ) {
                        var id = $(this).attr('for') + '_ypop';
                        $(this).attr('for', id);
                    }
                });

                if( self.overlay.find('.close').length == 0 ) {
                    self.close = $('<a />', {
                        'class' : 'close'
                    }).appendTo( self.popup );
                } else {
                    self.close = self.overlay.find('.close');
                }
            };

        self.init();
    };

    $.fn.yit_popup = function(options) {

        return this.each(function() {
            if (undefined === $(this).data('yit_popup')) {
                var yit_popup = new $.yit_popup(this, options);
                $(this).data('yit_popup', yit_popup);
            }
        });

    };

})(jQuery, window, document);
;
/* ************************** */
/* Init
/* ************************** */
function init() {
	var el = jQuery( 'body' ),
		windowWidth = jQuery(window).width(),
		isMobile = false;

	// Modify event tour dates
	renderEventDates(el);

	// Change mobile icon
	changeMobileIcon(windowWidth);
}


/* ************************** */
/* Change mobile icon
/* Detects homepage, mobile icon, removes icon, replaces it with custom div
/* controlled by css
/* ************************** */
function changeMobileIcon(windowWidth) {
	if (windowWidth <= 736) {
		//console.log('mobile view');

		var	el = jQuery( 'body' ),
			$header = ('#jas-header'),
			$mobileHeader = el.find( $header ),
			$domNode = 'a.hide-md',
			$mobileIcon = el.find( $domNode );

		if (el.hasClass('home')) {
			//console.log('homepage');

			if ($mobileIcon.length) {
				//console.log('found mobile icon' + $mobileIcon);

				//$newIcon = '<img src="../wp-content/themes/claue-child-artist-template/custom_images/hamburger-white.svg" width="30" height="16" alt="Menu">'

				$newIcon = '<div id="custom-mobile-icon"></div>'
	
				$mobileIcon.empty();
				$mobileIcon.append($newIcon);
			}
		}

		
	}
}


/* ************************** */
/* Change tour/event dates
/* 1 => January
/* ************************** */
function renderEventDates(element) {

	var //$element = jQuery( element ),
    	$domNode = '.displayProduct-Container',
    	$eventTicketsTable = element.find( $domNode ),
    	monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesAbv = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];


	if ($eventTicketsTable.length) {
		console.log('found event table');

		var $row = $eventTicketsTable.find( '.dp-table-tr' );

		jQuery($row).each(function(key, item){
			//console.log(key, item);

		    jQuery(this).find('.MonthDateClass').each(function(i, item){
		    	var date = Number(item.innerHTML),
		    		newDate = monthNamesAbv[date-1];
		    	
		    	//console.log(this, newDate);
		    	jQuery(this).text(newDate);
		    });

		});

		//removeMeetAndGreetCta($row);
	}

	return;
}


/* ************************** */
/* Start the magic!
/* ************************** */
jQuery( document ).ready(function() {
	console.log( 'ready!' );
	
	// Call Init function
	init();

});;
//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){function n(n){function t(t,r,e,u,i,o){for(;i>=0&&o>i;i+=n){var a=u?u[i]:i;e=r(e,t[a],a,t)}return e}return function(r,e,u,i){e=b(e,i,4);var o=!k(r)&&m.keys(r),a=(o||r).length,c=n>0?0:a-1;return arguments.length<3&&(u=r[o?o[c]:c],c+=n),t(r,e,u,o,c,a)}}function t(n){return function(t,r,e){r=x(r,e);for(var u=O(t),i=n>0?0:u-1;i>=0&&u>i;i+=n)if(r(t[i],i,t))return i;return-1}}function r(n,t,r){return function(e,u,i){var o=0,a=O(e);if("number"==typeof i)n>0?o=i>=0?i:Math.max(i+a,o):a=i>=0?Math.min(i+1,a):i+a+1;else if(r&&i&&a)return i=r(e,u),e[i]===u?i:-1;if(u!==u)return i=t(l.call(e,o,a),m.isNaN),i>=0?i+o:-1;for(i=n>0?o:a-1;i>=0&&a>i;i+=n)if(e[i]===u)return i;return-1}}function e(n,t){var r=I.length,e=n.constructor,u=m.isFunction(e)&&e.prototype||a,i="constructor";for(m.has(n,i)&&!m.contains(t,i)&&t.push(i);r--;)i=I[r],i in n&&n[i]!==u[i]&&!m.contains(t,i)&&t.push(i)}var u=this,i=u._,o=Array.prototype,a=Object.prototype,c=Function.prototype,f=o.push,l=o.slice,s=a.toString,p=a.hasOwnProperty,h=Array.isArray,v=Object.keys,g=c.bind,y=Object.create,d=function(){},m=function(n){return n instanceof m?n:this instanceof m?void(this._wrapped=n):new m(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=m),exports._=m):u._=m,m.VERSION="1.8.3";var b=function(n,t,r){if(t===void 0)return n;switch(null==r?3:r){case 1:return function(r){return n.call(t,r)};case 2:return function(r,e){return n.call(t,r,e)};case 3:return function(r,e,u){return n.call(t,r,e,u)};case 4:return function(r,e,u,i){return n.call(t,r,e,u,i)}}return function(){return n.apply(t,arguments)}},x=function(n,t,r){return null==n?m.identity:m.isFunction(n)?b(n,t,r):m.isObject(n)?m.matcher(n):m.property(n)};m.iteratee=function(n,t){return x(n,t,1/0)};var _=function(n,t){return function(r){var e=arguments.length;if(2>e||null==r)return r;for(var u=1;e>u;u++)for(var i=arguments[u],o=n(i),a=o.length,c=0;a>c;c++){var f=o[c];t&&r[f]!==void 0||(r[f]=i[f])}return r}},j=function(n){if(!m.isObject(n))return{};if(y)return y(n);d.prototype=n;var t=new d;return d.prototype=null,t},w=function(n){return function(t){return null==t?void 0:t[n]}},A=Math.pow(2,53)-1,O=w("length"),k=function(n){var t=O(n);return"number"==typeof t&&t>=0&&A>=t};m.each=m.forEach=function(n,t,r){t=b(t,r);var e,u;if(k(n))for(e=0,u=n.length;u>e;e++)t(n[e],e,n);else{var i=m.keys(n);for(e=0,u=i.length;u>e;e++)t(n[i[e]],i[e],n)}return n},m.map=m.collect=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=Array(u),o=0;u>o;o++){var a=e?e[o]:o;i[o]=t(n[a],a,n)}return i},m.reduce=m.foldl=m.inject=n(1),m.reduceRight=m.foldr=n(-1),m.find=m.detect=function(n,t,r){var e;return e=k(n)?m.findIndex(n,t,r):m.findKey(n,t,r),e!==void 0&&e!==-1?n[e]:void 0},m.filter=m.select=function(n,t,r){var e=[];return t=x(t,r),m.each(n,function(n,r,u){t(n,r,u)&&e.push(n)}),e},m.reject=function(n,t,r){return m.filter(n,m.negate(x(t)),r)},m.every=m.all=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(!t(n[o],o,n))return!1}return!0},m.some=m.any=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(t(n[o],o,n))return!0}return!1},m.contains=m.includes=m.include=function(n,t,r,e){return k(n)||(n=m.values(n)),("number"!=typeof r||e)&&(r=0),m.indexOf(n,t,r)>=0},m.invoke=function(n,t){var r=l.call(arguments,2),e=m.isFunction(t);return m.map(n,function(n){var u=e?t:n[t];return null==u?u:u.apply(n,r)})},m.pluck=function(n,t){return m.map(n,m.property(t))},m.where=function(n,t){return m.filter(n,m.matcher(t))},m.findWhere=function(n,t){return m.find(n,m.matcher(t))},m.max=function(n,t,r){var e,u,i=-1/0,o=-1/0;if(null==t&&null!=n){n=k(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],e>i&&(i=e)}else t=x(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(u>o||u===-1/0&&i===-1/0)&&(i=n,o=u)});return i},m.min=function(n,t,r){var e,u,i=1/0,o=1/0;if(null==t&&null!=n){n=k(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],i>e&&(i=e)}else t=x(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(o>u||1/0===u&&1/0===i)&&(i=n,o=u)});return i},m.shuffle=function(n){for(var t,r=k(n)?n:m.values(n),e=r.length,u=Array(e),i=0;e>i;i++)t=m.random(0,i),t!==i&&(u[i]=u[t]),u[t]=r[i];return u},m.sample=function(n,t,r){return null==t||r?(k(n)||(n=m.values(n)),n[m.random(n.length-1)]):m.shuffle(n).slice(0,Math.max(0,t))},m.sortBy=function(n,t,r){return t=x(t,r),m.pluck(m.map(n,function(n,r,e){return{value:n,index:r,criteria:t(n,r,e)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var F=function(n){return function(t,r,e){var u={};return r=x(r,e),m.each(t,function(e,i){var o=r(e,i,t);n(u,e,o)}),u}};m.groupBy=F(function(n,t,r){m.has(n,r)?n[r].push(t):n[r]=[t]}),m.indexBy=F(function(n,t,r){n[r]=t}),m.countBy=F(function(n,t,r){m.has(n,r)?n[r]++:n[r]=1}),m.toArray=function(n){return n?m.isArray(n)?l.call(n):k(n)?m.map(n,m.identity):m.values(n):[]},m.size=function(n){return null==n?0:k(n)?n.length:m.keys(n).length},m.partition=function(n,t,r){t=x(t,r);var e=[],u=[];return m.each(n,function(n,r,i){(t(n,r,i)?e:u).push(n)}),[e,u]},m.first=m.head=m.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:m.initial(n,n.length-t)},m.initial=function(n,t,r){return l.call(n,0,Math.max(0,n.length-(null==t||r?1:t)))},m.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:m.rest(n,Math.max(0,n.length-t))},m.rest=m.tail=m.drop=function(n,t,r){return l.call(n,null==t||r?1:t)},m.compact=function(n){return m.filter(n,m.identity)};var S=function(n,t,r,e){for(var u=[],i=0,o=e||0,a=O(n);a>o;o++){var c=n[o];if(k(c)&&(m.isArray(c)||m.isArguments(c))){t||(c=S(c,t,r));var f=0,l=c.length;for(u.length+=l;l>f;)u[i++]=c[f++]}else r||(u[i++]=c)}return u};m.flatten=function(n,t){return S(n,t,!1)},m.without=function(n){return m.difference(n,l.call(arguments,1))},m.uniq=m.unique=function(n,t,r,e){m.isBoolean(t)||(e=r,r=t,t=!1),null!=r&&(r=x(r,e));for(var u=[],i=[],o=0,a=O(n);a>o;o++){var c=n[o],f=r?r(c,o,n):c;t?(o&&i===f||u.push(c),i=f):r?m.contains(i,f)||(i.push(f),u.push(c)):m.contains(u,c)||u.push(c)}return u},m.union=function(){return m.uniq(S(arguments,!0,!0))},m.intersection=function(n){for(var t=[],r=arguments.length,e=0,u=O(n);u>e;e++){var i=n[e];if(!m.contains(t,i)){for(var o=1;r>o&&m.contains(arguments[o],i);o++);o===r&&t.push(i)}}return t},m.difference=function(n){var t=S(arguments,!0,!0,1);return m.filter(n,function(n){return!m.contains(t,n)})},m.zip=function(){return m.unzip(arguments)},m.unzip=function(n){for(var t=n&&m.max(n,O).length||0,r=Array(t),e=0;t>e;e++)r[e]=m.pluck(n,e);return r},m.object=function(n,t){for(var r={},e=0,u=O(n);u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},m.findIndex=t(1),m.findLastIndex=t(-1),m.sortedIndex=function(n,t,r,e){r=x(r,e,1);for(var u=r(t),i=0,o=O(n);o>i;){var a=Math.floor((i+o)/2);r(n[a])<u?i=a+1:o=a}return i},m.indexOf=r(1,m.findIndex,m.sortedIndex),m.lastIndexOf=r(-1,m.findLastIndex),m.range=function(n,t,r){null==t&&(t=n||0,n=0),r=r||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=Array(e),i=0;e>i;i++,n+=r)u[i]=n;return u};var E=function(n,t,r,e,u){if(!(e instanceof t))return n.apply(r,u);var i=j(n.prototype),o=n.apply(i,u);return m.isObject(o)?o:i};m.bind=function(n,t){if(g&&n.bind===g)return g.apply(n,l.call(arguments,1));if(!m.isFunction(n))throw new TypeError("Bind must be called on a function");var r=l.call(arguments,2),e=function(){return E(n,e,t,this,r.concat(l.call(arguments)))};return e},m.partial=function(n){var t=l.call(arguments,1),r=function(){for(var e=0,u=t.length,i=Array(u),o=0;u>o;o++)i[o]=t[o]===m?arguments[e++]:t[o];for(;e<arguments.length;)i.push(arguments[e++]);return E(n,r,this,this,i)};return r},m.bindAll=function(n){var t,r,e=arguments.length;if(1>=e)throw new Error("bindAll must be passed function names");for(t=1;e>t;t++)r=arguments[t],n[r]=m.bind(n[r],n);return n},m.memoize=function(n,t){var r=function(e){var u=r.cache,i=""+(t?t.apply(this,arguments):e);return m.has(u,i)||(u[i]=n.apply(this,arguments)),u[i]};return r.cache={},r},m.delay=function(n,t){var r=l.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},m.defer=m.partial(m.delay,m,1),m.throttle=function(n,t,r){var e,u,i,o=null,a=0;r||(r={});var c=function(){a=r.leading===!1?0:m.now(),o=null,i=n.apply(e,u),o||(e=u=null)};return function(){var f=m.now();a||r.leading!==!1||(a=f);var l=t-(f-a);return e=this,u=arguments,0>=l||l>t?(o&&(clearTimeout(o),o=null),a=f,i=n.apply(e,u),o||(e=u=null)):o||r.trailing===!1||(o=setTimeout(c,l)),i}},m.debounce=function(n,t,r){var e,u,i,o,a,c=function(){var f=m.now()-o;t>f&&f>=0?e=setTimeout(c,t-f):(e=null,r||(a=n.apply(i,u),e||(i=u=null)))};return function(){i=this,u=arguments,o=m.now();var f=r&&!e;return e||(e=setTimeout(c,t)),f&&(a=n.apply(i,u),i=u=null),a}},m.wrap=function(n,t){return m.partial(t,n)},m.negate=function(n){return function(){return!n.apply(this,arguments)}},m.compose=function(){var n=arguments,t=n.length-1;return function(){for(var r=t,e=n[t].apply(this,arguments);r--;)e=n[r].call(this,e);return e}},m.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},m.before=function(n,t){var r;return function(){return--n>0&&(r=t.apply(this,arguments)),1>=n&&(t=null),r}},m.once=m.partial(m.before,2);var M=!{toString:null}.propertyIsEnumerable("toString"),I=["valueOf","isPrototypeOf","toString","propertyIsEnumerable","hasOwnProperty","toLocaleString"];m.keys=function(n){if(!m.isObject(n))return[];if(v)return v(n);var t=[];for(var r in n)m.has(n,r)&&t.push(r);return M&&e(n,t),t},m.allKeys=function(n){if(!m.isObject(n))return[];var t=[];for(var r in n)t.push(r);return M&&e(n,t),t},m.values=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},m.mapObject=function(n,t,r){t=x(t,r);for(var e,u=m.keys(n),i=u.length,o={},a=0;i>a;a++)e=u[a],o[e]=t(n[e],e,n);return o},m.pairs=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},m.invert=function(n){for(var t={},r=m.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},m.functions=m.methods=function(n){var t=[];for(var r in n)m.isFunction(n[r])&&t.push(r);return t.sort()},m.extend=_(m.allKeys),m.extendOwn=m.assign=_(m.keys),m.findKey=function(n,t,r){t=x(t,r);for(var e,u=m.keys(n),i=0,o=u.length;o>i;i++)if(e=u[i],t(n[e],e,n))return e},m.pick=function(n,t,r){var e,u,i={},o=n;if(null==o)return i;m.isFunction(t)?(u=m.allKeys(o),e=b(t,r)):(u=S(arguments,!1,!1,1),e=function(n,t,r){return t in r},o=Object(o));for(var a=0,c=u.length;c>a;a++){var f=u[a],l=o[f];e(l,f,o)&&(i[f]=l)}return i},m.omit=function(n,t,r){if(m.isFunction(t))t=m.negate(t);else{var e=m.map(S(arguments,!1,!1,1),String);t=function(n,t){return!m.contains(e,t)}}return m.pick(n,t,r)},m.defaults=_(m.allKeys,!0),m.create=function(n,t){var r=j(n);return t&&m.extendOwn(r,t),r},m.clone=function(n){return m.isObject(n)?m.isArray(n)?n.slice():m.extend({},n):n},m.tap=function(n,t){return t(n),n},m.isMatch=function(n,t){var r=m.keys(t),e=r.length;if(null==n)return!e;for(var u=Object(n),i=0;e>i;i++){var o=r[i];if(t[o]!==u[o]||!(o in u))return!1}return!0};var N=function(n,t,r,e){if(n===t)return 0!==n||1/n===1/t;if(null==n||null==t)return n===t;n instanceof m&&(n=n._wrapped),t instanceof m&&(t=t._wrapped);var u=s.call(n);if(u!==s.call(t))return!1;switch(u){case"[object RegExp]":case"[object String]":return""+n==""+t;case"[object Number]":return+n!==+n?+t!==+t:0===+n?1/+n===1/t:+n===+t;case"[object Date]":case"[object Boolean]":return+n===+t}var i="[object Array]"===u;if(!i){if("object"!=typeof n||"object"!=typeof t)return!1;var o=n.constructor,a=t.constructor;if(o!==a&&!(m.isFunction(o)&&o instanceof o&&m.isFunction(a)&&a instanceof a)&&"constructor"in n&&"constructor"in t)return!1}r=r||[],e=e||[];for(var c=r.length;c--;)if(r[c]===n)return e[c]===t;if(r.push(n),e.push(t),i){if(c=n.length,c!==t.length)return!1;for(;c--;)if(!N(n[c],t[c],r,e))return!1}else{var f,l=m.keys(n);if(c=l.length,m.keys(t).length!==c)return!1;for(;c--;)if(f=l[c],!m.has(t,f)||!N(n[f],t[f],r,e))return!1}return r.pop(),e.pop(),!0};m.isEqual=function(n,t){return N(n,t)},m.isEmpty=function(n){return null==n?!0:k(n)&&(m.isArray(n)||m.isString(n)||m.isArguments(n))?0===n.length:0===m.keys(n).length},m.isElement=function(n){return!(!n||1!==n.nodeType)},m.isArray=h||function(n){return"[object Array]"===s.call(n)},m.isObject=function(n){var t=typeof n;return"function"===t||"object"===t&&!!n},m.each(["Arguments","Function","String","Number","Date","RegExp","Error"],function(n){m["is"+n]=function(t){return s.call(t)==="[object "+n+"]"}}),m.isArguments(arguments)||(m.isArguments=function(n){return m.has(n,"callee")}),"function"!=typeof/./&&"object"!=typeof Int8Array&&(m.isFunction=function(n){return"function"==typeof n||!1}),m.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},m.isNaN=function(n){return m.isNumber(n)&&n!==+n},m.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"===s.call(n)},m.isNull=function(n){return null===n},m.isUndefined=function(n){return n===void 0},m.has=function(n,t){return null!=n&&p.call(n,t)},m.noConflict=function(){return u._=i,this},m.identity=function(n){return n},m.constant=function(n){return function(){return n}},m.noop=function(){},m.property=w,m.propertyOf=function(n){return null==n?function(){}:function(t){return n[t]}},m.matcher=m.matches=function(n){return n=m.extendOwn({},n),function(t){return m.isMatch(t,n)}},m.times=function(n,t,r){var e=Array(Math.max(0,n));t=b(t,r,1);for(var u=0;n>u;u++)e[u]=t(u);return e},m.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},m.now=Date.now||function(){return(new Date).getTime()};var B={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},T=m.invert(B),R=function(n){var t=function(t){return n[t]},r="(?:"+m.keys(n).join("|")+")",e=RegExp(r),u=RegExp(r,"g");return function(n){return n=null==n?"":""+n,e.test(n)?n.replace(u,t):n}};m.escape=R(B),m.unescape=R(T),m.result=function(n,t,r){var e=null==n?void 0:n[t];return e===void 0&&(e=r),m.isFunction(e)?e.call(n):e};var q=0;m.uniqueId=function(n){var t=++q+"";return n?n+t:t},m.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var K=/(.)^/,z={"'":"'","\\":"\\","\r":"r","\n":"n","\u2028":"u2028","\u2029":"u2029"},D=/\\|'|\r|\n|\u2028|\u2029/g,L=function(n){return"\\"+z[n]};m.template=function(n,t,r){!t&&r&&(t=r),t=m.defaults({},t,m.templateSettings);var e=RegExp([(t.escape||K).source,(t.interpolate||K).source,(t.evaluate||K).source].join("|")+"|$","g"),u=0,i="__p+='";n.replace(e,function(t,r,e,o,a){return i+=n.slice(u,a).replace(D,L),u=a+t.length,r?i+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'":e?i+="'+\n((__t=("+e+"))==null?'':__t)+\n'":o&&(i+="';\n"+o+"\n__p+='"),t}),i+="';\n",t.variable||(i="with(obj||{}){\n"+i+"}\n"),i="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+i+"return __p;\n";try{var o=new Function(t.variable||"obj","_",i)}catch(a){throw a.source=i,a}var c=function(n){return o.call(this,n,m)},f=t.variable||"obj";return c.source="function("+f+"){\n"+i+"}",c},m.chain=function(n){var t=m(n);return t._chain=!0,t};var P=function(n,t){return n._chain?m(t).chain():t};m.mixin=function(n){m.each(m.functions(n),function(t){var r=m[t]=n[t];m.prototype[t]=function(){var n=[this._wrapped];return f.apply(n,arguments),P(this,r.apply(m,n))}})},m.mixin(m),m.each(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=o[n];m.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!==n&&"splice"!==n||0!==r.length||delete r[0],P(this,r)}}),m.each(["concat","join","slice"],function(n){var t=o[n];m.prototype[n]=function(){return P(this,t.apply(this._wrapped,arguments))}}),m.prototype.value=function(){return this._wrapped},m.prototype.valueOf=m.prototype.toJSON=m.prototype.value,m.prototype.toString=function(){return""+this._wrapped},"function"==typeof define&&define.amd&&define("underscore",[],function(){return m})}).call(this);
;
/* global _wpUtilSettings */

/** @namespace wp */
window.wp = window.wp || {};

(function ($) {
	// Check for the utility settings.
	var settings = typeof _wpUtilSettings === 'undefined' ? {} : _wpUtilSettings;

	/**
	 * wp.template( id )
	 *
	 * Fetch a JavaScript template for an id, and return a templating function for it.
	 *
	 * @param  {string} id   A string that corresponds to a DOM element with an id prefixed with "tmpl-".
	 *                       For example, "attachment" maps to "tmpl-attachment".
	 * @return {function}    A function that lazily-compiles the template requested.
	 */
	wp.template = _.memoize(function ( id ) {
		var compiled,
			/*
			 * Underscore's default ERB-style templates are incompatible with PHP
			 * when asp_tags is enabled, so WordPress uses Mustache-inspired templating syntax.
			 *
			 * @see trac ticket #22344.
			 */
			options = {
				evaluate:    /<#([\s\S]+?)#>/g,
				interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
				escape:      /\{\{([^\}]+?)\}\}(?!\})/g,
				variable:    'data'
			};

		return function ( data ) {
			compiled = compiled || _.template( $( '#tmpl-' + id ).html(),  options );
			return compiled( data );
		};
	});

	// wp.ajax
	// ------
	//
	// Tools for sending ajax requests with JSON responses and built in error handling.
	// Mirrors and wraps jQuery's ajax APIs.
	wp.ajax = {
		settings: settings.ajax || {},

		/**
		 * wp.ajax.post( [action], [data] )
		 *
		 * Sends a POST request to WordPress.
		 *
		 * @param  {(string|object)} action  The slug of the action to fire in WordPress or options passed
		 *                                   to jQuery.ajax.
		 * @param  {object=}         data    Optional. The data to populate $_POST with.
		 * @return {$.promise}     A jQuery promise that represents the request,
		 *                         decorated with an abort() method.
		 */
		post: function( action, data ) {
			return wp.ajax.send({
				data: _.isObject( action ) ? action : _.extend( data || {}, { action: action })
			});
		},

		/**
		 * wp.ajax.send( [action], [options] )
		 *
		 * Sends a POST request to WordPress.
		 *
		 * @param  {(string|object)} action  The slug of the action to fire in WordPress or options passed
		 *                                   to jQuery.ajax.
		 * @param  {object=}         options Optional. The options passed to jQuery.ajax.
		 * @return {$.promise}      A jQuery promise that represents the request,
		 *                          decorated with an abort() method.
		 */
		send: function( action, options ) {
			var promise, deferred;
			if ( _.isObject( action ) ) {
				options = action;
			} else {
				options = options || {};
				options.data = _.extend( options.data || {}, { action: action });
			}

			options = _.defaults( options || {}, {
				type:    'POST',
				url:     wp.ajax.settings.url,
				context: this
			});

			deferred = $.Deferred( function( deferred ) {
				// Transfer success/error callbacks.
				if ( options.success )
					deferred.done( options.success );
				if ( options.error )
					deferred.fail( options.error );

				delete options.success;
				delete options.error;

				// Use with PHP's wp_send_json_success() and wp_send_json_error()
				deferred.jqXHR = $.ajax( options ).done( function( response ) {
					// Treat a response of 1 as successful for backward compatibility with existing handlers.
					if ( response === '1' || response === 1 )
						response = { success: true };

					if ( _.isObject( response ) && ! _.isUndefined( response.success ) )
						deferred[ response.success ? 'resolveWith' : 'rejectWith' ]( this, [response.data] );
					else
						deferred.rejectWith( this, [response] );
				}).fail( function() {
					deferred.rejectWith( this, arguments );
				});
			});

			promise = deferred.promise();
			promise.abort = function() {
				deferred.jqXHR.abort();
				return this;
			};

			return promise;
		}
	};

}(jQuery));
;
/*!
 * jQuery UI Core 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/category/ui-core/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){function b(b,d){var e,f,g,h=b.nodeName.toLowerCase();return"area"===h?(e=b.parentNode,f=e.name,!(!b.href||!f||"map"!==e.nodeName.toLowerCase())&&(g=a("img[usemap='#"+f+"']")[0],!!g&&c(g))):(/^(input|select|textarea|button|object)$/.test(h)?!b.disabled:"a"===h?b.href||d:d)&&c(b)}function c(b){return a.expr.filters.visible(b)&&!a(b).parents().addBack().filter(function(){return"hidden"===a.css(this,"visibility")}).length}a.ui=a.ui||{},a.extend(a.ui,{version:"1.11.4",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),a.fn.extend({scrollParent:function(b){var c=this.css("position"),d="absolute"===c,e=b?/(auto|scroll|hidden)/:/(auto|scroll)/,f=this.parents().filter(function(){var b=a(this);return(!d||"static"!==b.css("position"))&&e.test(b.css("overflow")+b.css("overflow-y")+b.css("overflow-x"))}).eq(0);return"fixed"!==c&&f.length?f:a(this[0].ownerDocument||document)},uniqueId:function(){var a=0;return function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++a)})}}(),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&a(this).removeAttr("id")})}}),a.extend(a.expr[":"],{data:a.expr.createPseudo?a.expr.createPseudo(function(b){return function(c){return!!a.data(c,b)}}):function(b,c,d){return!!a.data(b,d[3])},focusable:function(c){return b(c,!isNaN(a.attr(c,"tabindex")))},tabbable:function(c){var d=a.attr(c,"tabindex"),e=isNaN(d);return(e||d>=0)&&b(c,!e)}}),a("<a>").outerWidth(1).jquery||a.each(["Width","Height"],function(b,c){function d(b,c,d,f){return a.each(e,function(){c-=parseFloat(a.css(b,"padding"+this))||0,d&&(c-=parseFloat(a.css(b,"border"+this+"Width"))||0),f&&(c-=parseFloat(a.css(b,"margin"+this))||0)}),c}var e="Width"===c?["Left","Right"]:["Top","Bottom"],f=c.toLowerCase(),g={innerWidth:a.fn.innerWidth,innerHeight:a.fn.innerHeight,outerWidth:a.fn.outerWidth,outerHeight:a.fn.outerHeight};a.fn["inner"+c]=function(b){return void 0===b?g["inner"+c].call(this):this.each(function(){a(this).css(f,d(this,b)+"px")})},a.fn["outer"+c]=function(b,e){return"number"!=typeof b?g["outer"+c].call(this,b):this.each(function(){a(this).css(f,d(this,b,!0,e)+"px")})}}),a.fn.addBack||(a.fn.addBack=function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}),a("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(a.fn.removeData=function(b){return function(c){return arguments.length?b.call(this,a.camelCase(c)):b.call(this)}}(a.fn.removeData)),a.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),a.fn.extend({focus:function(b){return function(c,d){return"number"==typeof c?this.each(function(){var b=this;setTimeout(function(){a(b).focus(),d&&d.call(b)},c)}):b.apply(this,arguments)}}(a.fn.focus),disableSelection:function(){var a="onselectstart"in document.createElement("div")?"selectstart":"mousedown";return function(){return this.bind(a+".ui-disableSelection",function(a){a.preventDefault()})}}(),enableSelection:function(){return this.unbind(".ui-disableSelection")},zIndex:function(b){if(void 0!==b)return this.css("zIndex",b);if(this.length)for(var c,d,e=a(this[0]);e.length&&e[0]!==document;){if(c=e.css("position"),("absolute"===c||"relative"===c||"fixed"===c)&&(d=parseInt(e.css("zIndex"),10),!isNaN(d)&&0!==d))return d;e=e.parent()}return 0}}),a.ui.plugin={add:function(b,c,d){var e,f=a.ui[b].prototype;for(e in d)f.plugins[e]=f.plugins[e]||[],f.plugins[e].push([c,d[e]])},call:function(a,b,c,d){var e,f=a.plugins[b];if(f&&(d||a.element[0].parentNode&&11!==a.element[0].parentNode.nodeType))for(e=0;e<f.length;e++)a.options[f[e][0]]&&f[e][1].apply(a.element,c)}}});;
/*!
 * jQuery UI Widget 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/jQuery.widget/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){var b=0,c=Array.prototype.slice;return a.cleanData=function(b){return function(c){var d,e,f;for(f=0;null!=(e=c[f]);f++)try{d=a._data(e,"events"),d&&d.remove&&a(e).triggerHandler("remove")}catch(g){}b(c)}}(a.cleanData),a.widget=function(b,c,d){var e,f,g,h,i={},j=b.split(".")[0];return b=b.split(".")[1],e=j+"-"+b,d||(d=c,c=a.Widget),a.expr[":"][e.toLowerCase()]=function(b){return!!a.data(b,e)},a[j]=a[j]||{},f=a[j][b],g=a[j][b]=function(a,b){return this._createWidget?void(arguments.length&&this._createWidget(a,b)):new g(a,b)},a.extend(g,f,{version:d.version,_proto:a.extend({},d),_childConstructors:[]}),h=new c,h.options=a.widget.extend({},h.options),a.each(d,function(b,d){return a.isFunction(d)?void(i[b]=function(){var a=function(){return c.prototype[b].apply(this,arguments)},e=function(a){return c.prototype[b].apply(this,a)};return function(){var b,c=this._super,f=this._superApply;return this._super=a,this._superApply=e,b=d.apply(this,arguments),this._super=c,this._superApply=f,b}}()):void(i[b]=d)}),g.prototype=a.widget.extend(h,{widgetEventPrefix:f?h.widgetEventPrefix||b:b},i,{constructor:g,namespace:j,widgetName:b,widgetFullName:e}),f?(a.each(f._childConstructors,function(b,c){var d=c.prototype;a.widget(d.namespace+"."+d.widgetName,g,c._proto)}),delete f._childConstructors):c._childConstructors.push(g),a.widget.bridge(b,g),g},a.widget.extend=function(b){for(var d,e,f=c.call(arguments,1),g=0,h=f.length;g<h;g++)for(d in f[g])e=f[g][d],f[g].hasOwnProperty(d)&&void 0!==e&&(a.isPlainObject(e)?b[d]=a.isPlainObject(b[d])?a.widget.extend({},b[d],e):a.widget.extend({},e):b[d]=e);return b},a.widget.bridge=function(b,d){var e=d.prototype.widgetFullName||b;a.fn[b]=function(f){var g="string"==typeof f,h=c.call(arguments,1),i=this;return g?this.each(function(){var c,d=a.data(this,e);return"instance"===f?(i=d,!1):d?a.isFunction(d[f])&&"_"!==f.charAt(0)?(c=d[f].apply(d,h),c!==d&&void 0!==c?(i=c&&c.jquery?i.pushStack(c.get()):c,!1):void 0):a.error("no such method '"+f+"' for "+b+" widget instance"):a.error("cannot call methods on "+b+" prior to initialization; attempted to call method '"+f+"'")}):(h.length&&(f=a.widget.extend.apply(null,[f].concat(h))),this.each(function(){var b=a.data(this,e);b?(b.option(f||{}),b._init&&b._init()):a.data(this,e,new d(f,this))})),i}},a.Widget=function(){},a.Widget._childConstructors=[],a.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(c,d){d=a(d||this.defaultElement||this)[0],this.element=a(d),this.uuid=b++,this.eventNamespace="."+this.widgetName+this.uuid,this.bindings=a(),this.hoverable=a(),this.focusable=a(),d!==this&&(a.data(d,this.widgetFullName,this),this._on(!0,this.element,{remove:function(a){a.target===d&&this.destroy()}}),this.document=a(d.style?d.ownerDocument:d.document||d),this.window=a(this.document[0].defaultView||this.document[0].parentWindow)),this.options=a.widget.extend({},this.options,this._getCreateOptions(),c),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:a.noop,_getCreateEventData:a.noop,_create:a.noop,_init:a.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(a.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:a.noop,widget:function(){return this.element},option:function(b,c){var d,e,f,g=b;if(0===arguments.length)return a.widget.extend({},this.options);if("string"==typeof b)if(g={},d=b.split("."),b=d.shift(),d.length){for(e=g[b]=a.widget.extend({},this.options[b]),f=0;f<d.length-1;f++)e[d[f]]=e[d[f]]||{},e=e[d[f]];if(b=d.pop(),1===arguments.length)return void 0===e[b]?null:e[b];e[b]=c}else{if(1===arguments.length)return void 0===this.options[b]?null:this.options[b];g[b]=c}return this._setOptions(g),this},_setOptions:function(a){var b;for(b in a)this._setOption(b,a[b]);return this},_setOption:function(a,b){return this.options[a]=b,"disabled"===a&&(this.widget().toggleClass(this.widgetFullName+"-disabled",!!b),b&&(this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus"))),this},enable:function(){return this._setOptions({disabled:!1})},disable:function(){return this._setOptions({disabled:!0})},_on:function(b,c,d){var e,f=this;"boolean"!=typeof b&&(d=c,c=b,b=!1),d?(c=e=a(c),this.bindings=this.bindings.add(c)):(d=c,c=this.element,e=this.widget()),a.each(d,function(d,g){function h(){if(b||f.options.disabled!==!0&&!a(this).hasClass("ui-state-disabled"))return("string"==typeof g?f[g]:g).apply(f,arguments)}"string"!=typeof g&&(h.guid=g.guid=g.guid||h.guid||a.guid++);var i=d.match(/^([\w:-]*)\s*(.*)$/),j=i[1]+f.eventNamespace,k=i[2];k?e.delegate(k,j,h):c.bind(j,h)})},_off:function(b,c){c=(c||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,b.unbind(c).undelegate(c),this.bindings=a(this.bindings.not(b).get()),this.focusable=a(this.focusable.not(b).get()),this.hoverable=a(this.hoverable.not(b).get())},_delay:function(a,b){function c(){return("string"==typeof a?d[a]:a).apply(d,arguments)}var d=this;return setTimeout(c,b||0)},_hoverable:function(b){this.hoverable=this.hoverable.add(b),this._on(b,{mouseenter:function(b){a(b.currentTarget).addClass("ui-state-hover")},mouseleave:function(b){a(b.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(b){this.focusable=this.focusable.add(b),this._on(b,{focusin:function(b){a(b.currentTarget).addClass("ui-state-focus")},focusout:function(b){a(b.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(b,c,d){var e,f,g=this.options[b];if(d=d||{},c=a.Event(c),c.type=(b===this.widgetEventPrefix?b:this.widgetEventPrefix+b).toLowerCase(),c.target=this.element[0],f=c.originalEvent)for(e in f)e in c||(c[e]=f[e]);return this.element.trigger(c,d),!(a.isFunction(g)&&g.apply(this.element[0],[c].concat(d))===!1||c.isDefaultPrevented())}},a.each({show:"fadeIn",hide:"fadeOut"},function(b,c){a.Widget.prototype["_"+b]=function(d,e,f){"string"==typeof e&&(e={effect:e});var g,h=e?e===!0||"number"==typeof e?c:e.effect||c:b;e=e||{},"number"==typeof e&&(e={duration:e}),g=!a.isEmptyObject(e),e.complete=f,e.delay&&d.delay(e.delay),g&&a.effects&&a.effects.effect[h]?d[b](e):h!==b&&d[h]?d[h](e.duration,e.easing,f):d.queue(function(c){a(this)[b](),f&&f.call(d[0]),c()})}}),a.widget});;
/*!
 * jQuery UI Position 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/position/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){return function(){function b(a,b,c){return[parseFloat(a[0])*(n.test(a[0])?b/100:1),parseFloat(a[1])*(n.test(a[1])?c/100:1)]}function c(b,c){return parseInt(a.css(b,c),10)||0}function d(b){var c=b[0];return 9===c.nodeType?{width:b.width(),height:b.height(),offset:{top:0,left:0}}:a.isWindow(c)?{width:b.width(),height:b.height(),offset:{top:b.scrollTop(),left:b.scrollLeft()}}:c.preventDefault?{width:0,height:0,offset:{top:c.pageY,left:c.pageX}}:{width:b.outerWidth(),height:b.outerHeight(),offset:b.offset()}}a.ui=a.ui||{};var e,f,g=Math.max,h=Math.abs,i=Math.round,j=/left|center|right/,k=/top|center|bottom/,l=/[\+\-]\d+(\.[\d]+)?%?/,m=/^\w+/,n=/%$/,o=a.fn.position;a.position={scrollbarWidth:function(){if(void 0!==e)return e;var b,c,d=a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),f=d.children()[0];return a("body").append(d),b=f.offsetWidth,d.css("overflow","scroll"),c=f.offsetWidth,b===c&&(c=d[0].clientWidth),d.remove(),e=b-c},getScrollInfo:function(b){var c=b.isWindow||b.isDocument?"":b.element.css("overflow-x"),d=b.isWindow||b.isDocument?"":b.element.css("overflow-y"),e="scroll"===c||"auto"===c&&b.width<b.element[0].scrollWidth,f="scroll"===d||"auto"===d&&b.height<b.element[0].scrollHeight;return{width:f?a.position.scrollbarWidth():0,height:e?a.position.scrollbarWidth():0}},getWithinInfo:function(b){var c=a(b||window),d=a.isWindow(c[0]),e=!!c[0]&&9===c[0].nodeType;return{element:c,isWindow:d,isDocument:e,offset:c.offset()||{left:0,top:0},scrollLeft:c.scrollLeft(),scrollTop:c.scrollTop(),width:d||e?c.width():c.outerWidth(),height:d||e?c.height():c.outerHeight()}}},a.fn.position=function(e){if(!e||!e.of)return o.apply(this,arguments);e=a.extend({},e);var n,p,q,r,s,t,u=a(e.of),v=a.position.getWithinInfo(e.within),w=a.position.getScrollInfo(v),x=(e.collision||"flip").split(" "),y={};return t=d(u),u[0].preventDefault&&(e.at="left top"),p=t.width,q=t.height,r=t.offset,s=a.extend({},r),a.each(["my","at"],function(){var a,b,c=(e[this]||"").split(" ");1===c.length&&(c=j.test(c[0])?c.concat(["center"]):k.test(c[0])?["center"].concat(c):["center","center"]),c[0]=j.test(c[0])?c[0]:"center",c[1]=k.test(c[1])?c[1]:"center",a=l.exec(c[0]),b=l.exec(c[1]),y[this]=[a?a[0]:0,b?b[0]:0],e[this]=[m.exec(c[0])[0],m.exec(c[1])[0]]}),1===x.length&&(x[1]=x[0]),"right"===e.at[0]?s.left+=p:"center"===e.at[0]&&(s.left+=p/2),"bottom"===e.at[1]?s.top+=q:"center"===e.at[1]&&(s.top+=q/2),n=b(y.at,p,q),s.left+=n[0],s.top+=n[1],this.each(function(){var d,j,k=a(this),l=k.outerWidth(),m=k.outerHeight(),o=c(this,"marginLeft"),t=c(this,"marginTop"),z=l+o+c(this,"marginRight")+w.width,A=m+t+c(this,"marginBottom")+w.height,B=a.extend({},s),C=b(y.my,k.outerWidth(),k.outerHeight());"right"===e.my[0]?B.left-=l:"center"===e.my[0]&&(B.left-=l/2),"bottom"===e.my[1]?B.top-=m:"center"===e.my[1]&&(B.top-=m/2),B.left+=C[0],B.top+=C[1],f||(B.left=i(B.left),B.top=i(B.top)),d={marginLeft:o,marginTop:t},a.each(["left","top"],function(b,c){a.ui.position[x[b]]&&a.ui.position[x[b]][c](B,{targetWidth:p,targetHeight:q,elemWidth:l,elemHeight:m,collisionPosition:d,collisionWidth:z,collisionHeight:A,offset:[n[0]+C[0],n[1]+C[1]],my:e.my,at:e.at,within:v,elem:k})}),e.using&&(j=function(a){var b=r.left-B.left,c=b+p-l,d=r.top-B.top,f=d+q-m,i={target:{element:u,left:r.left,top:r.top,width:p,height:q},element:{element:k,left:B.left,top:B.top,width:l,height:m},horizontal:c<0?"left":b>0?"right":"center",vertical:f<0?"top":d>0?"bottom":"middle"};p<l&&h(b+c)<p&&(i.horizontal="center"),q<m&&h(d+f)<q&&(i.vertical="middle"),g(h(b),h(c))>g(h(d),h(f))?i.important="horizontal":i.important="vertical",e.using.call(this,a,i)}),k.offset(a.extend(B,{using:j}))})},a.ui.position={fit:{left:function(a,b){var c,d=b.within,e=d.isWindow?d.scrollLeft:d.offset.left,f=d.width,h=a.left-b.collisionPosition.marginLeft,i=e-h,j=h+b.collisionWidth-f-e;b.collisionWidth>f?i>0&&j<=0?(c=a.left+i+b.collisionWidth-f-e,a.left+=i-c):j>0&&i<=0?a.left=e:i>j?a.left=e+f-b.collisionWidth:a.left=e:i>0?a.left+=i:j>0?a.left-=j:a.left=g(a.left-h,a.left)},top:function(a,b){var c,d=b.within,e=d.isWindow?d.scrollTop:d.offset.top,f=b.within.height,h=a.top-b.collisionPosition.marginTop,i=e-h,j=h+b.collisionHeight-f-e;b.collisionHeight>f?i>0&&j<=0?(c=a.top+i+b.collisionHeight-f-e,a.top+=i-c):j>0&&i<=0?a.top=e:i>j?a.top=e+f-b.collisionHeight:a.top=e:i>0?a.top+=i:j>0?a.top-=j:a.top=g(a.top-h,a.top)}},flip:{left:function(a,b){var c,d,e=b.within,f=e.offset.left+e.scrollLeft,g=e.width,i=e.isWindow?e.scrollLeft:e.offset.left,j=a.left-b.collisionPosition.marginLeft,k=j-i,l=j+b.collisionWidth-g-i,m="left"===b.my[0]?-b.elemWidth:"right"===b.my[0]?b.elemWidth:0,n="left"===b.at[0]?b.targetWidth:"right"===b.at[0]?-b.targetWidth:0,o=-2*b.offset[0];k<0?(c=a.left+m+n+o+b.collisionWidth-g-f,(c<0||c<h(k))&&(a.left+=m+n+o)):l>0&&(d=a.left-b.collisionPosition.marginLeft+m+n+o-i,(d>0||h(d)<l)&&(a.left+=m+n+o))},top:function(a,b){var c,d,e=b.within,f=e.offset.top+e.scrollTop,g=e.height,i=e.isWindow?e.scrollTop:e.offset.top,j=a.top-b.collisionPosition.marginTop,k=j-i,l=j+b.collisionHeight-g-i,m="top"===b.my[1],n=m?-b.elemHeight:"bottom"===b.my[1]?b.elemHeight:0,o="top"===b.at[1]?b.targetHeight:"bottom"===b.at[1]?-b.targetHeight:0,p=-2*b.offset[1];k<0?(d=a.top+n+o+p+b.collisionHeight-g-f,(d<0||d<h(k))&&(a.top+=n+o+p)):l>0&&(c=a.top-b.collisionPosition.marginTop+n+o+p-i,(c>0||h(c)<l)&&(a.top+=n+o+p))}},flipfit:{left:function(){a.ui.position.flip.left.apply(this,arguments),a.ui.position.fit.left.apply(this,arguments)},top:function(){a.ui.position.flip.top.apply(this,arguments),a.ui.position.fit.top.apply(this,arguments)}}},function(){var b,c,d,e,g,h=document.getElementsByTagName("body")[0],i=document.createElement("div");b=document.createElement(h?"div":"body"),d={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},h&&a.extend(d,{position:"absolute",left:"-1000px",top:"-1000px"});for(g in d)b.style[g]=d[g];b.appendChild(i),c=h||document.documentElement,c.insertBefore(b,c.firstChild),i.style.cssText="position: absolute; left: 10.7432222px;",e=a(i).offset().left,f=e>10&&e<11,b.innerHTML="",c.removeChild(b)}()}(),a.ui.position});;
/*!
 * jQuery UI Menu 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/menu/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./core","./widget","./position"],a):a(jQuery)}(function(a){return a.widget("ui.menu",{version:"1.11.4",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},items:"> *",menus:"ul",position:{my:"left-1 top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}),this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true"),this._on({"mousedown .ui-menu-item":function(a){a.preventDefault()},"click .ui-menu-item":function(b){var c=a(b.target);!this.mouseHandled&&c.not(".ui-state-disabled").length&&(this.select(b),b.isPropagationStopped()||(this.mouseHandled=!0),c.has(".ui-menu").length?this.expand(b):!this.element.is(":focus")&&a(this.document[0].activeElement).closest(".ui-menu").length&&(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(b){if(!this.previousFilter){var c=a(b.currentTarget);c.siblings(".ui-state-active").removeClass("ui-state-active"),this.focus(b,c)}},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(a,b){var c=this.active||this.element.find(this.options.items).eq(0);b||this.focus(a,c)},blur:function(b){this._delay(function(){a.contains(this.element[0],this.document[0].activeElement)||this.collapseAll(b)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(a){this._closeOnDocumentClick(a)&&this.collapseAll(a),this.mouseHandled=!1}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(),this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var b=a(this);b.data("ui-menu-submenu-carat")&&b.remove()}),this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(b){var c,d,e,f,g=!0;switch(b.keyCode){case a.ui.keyCode.PAGE_UP:this.previousPage(b);break;case a.ui.keyCode.PAGE_DOWN:this.nextPage(b);break;case a.ui.keyCode.HOME:this._move("first","first",b);break;case a.ui.keyCode.END:this._move("last","last",b);break;case a.ui.keyCode.UP:this.previous(b);break;case a.ui.keyCode.DOWN:this.next(b);break;case a.ui.keyCode.LEFT:this.collapse(b);break;case a.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(b);break;case a.ui.keyCode.ENTER:case a.ui.keyCode.SPACE:this._activate(b);break;case a.ui.keyCode.ESCAPE:this.collapse(b);break;default:g=!1,d=this.previousFilter||"",e=String.fromCharCode(b.keyCode),f=!1,clearTimeout(this.filterTimer),e===d?f=!0:e=d+e,c=this._filterMenuItems(e),c=f&&c.index(this.active.next())!==-1?this.active.nextAll(".ui-menu-item"):c,c.length||(e=String.fromCharCode(b.keyCode),c=this._filterMenuItems(e)),c.length?(this.focus(b,c),this.previousFilter=e,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter}g&&b.preventDefault()},_activate:function(a){this.active.is(".ui-state-disabled")||(this.active.is("[aria-haspopup='true']")?this.expand(a):this.select(a))},refresh:function(){var b,c,d=this,e=this.options.icons.submenu,f=this.element.find(this.options.menus);this.element.toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length),f.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var b=a(this),c=b.parent(),d=a("<span>").addClass("ui-menu-icon ui-icon "+e).data("ui-menu-submenu-carat",!0);c.attr("aria-haspopup","true").prepend(d),b.attr("aria-labelledby",c.attr("id"))}),b=f.add(this.element),c=b.find(this.options.items),c.not(".ui-menu-item").each(function(){var b=a(this);d._isDivider(b)&&b.addClass("ui-widget-content ui-menu-divider")}),c.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({tabIndex:-1,role:this._itemRole()}),c.filter(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!a.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},_setOption:function(a,b){"icons"===a&&this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(b.submenu),"disabled"===a&&this.element.toggleClass("ui-state-disabled",!!b).attr("aria-disabled",b),this._super(a,b)},focus:function(a,b){var c,d;this.blur(a,a&&"focus"===a.type),this._scrollIntoView(b),this.active=b.first(),d=this.active.addClass("ui-state-focus").removeClass("ui-state-active"),this.options.role&&this.element.attr("aria-activedescendant",d.attr("id")),this.active.parent().closest(".ui-menu-item").addClass("ui-state-active"),a&&"keydown"===a.type?this._close():this.timer=this._delay(function(){this._close()},this.delay),c=b.children(".ui-menu"),c.length&&a&&/^mouse/.test(a.type)&&this._startOpening(c),this.activeMenu=b.parent(),this._trigger("focus",a,{item:b})},_scrollIntoView:function(b){var c,d,e,f,g,h;this._hasScroll()&&(c=parseFloat(a.css(this.activeMenu[0],"borderTopWidth"))||0,d=parseFloat(a.css(this.activeMenu[0],"paddingTop"))||0,e=b.offset().top-this.activeMenu.offset().top-c-d,f=this.activeMenu.scrollTop(),g=this.activeMenu.height(),h=b.outerHeight(),e<0?this.activeMenu.scrollTop(f+e):e+h>g&&this.activeMenu.scrollTop(f+e-g+h))},blur:function(a,b){b||clearTimeout(this.timer),this.active&&(this.active.removeClass("ui-state-focus"),this.active=null,this._trigger("blur",a,{item:this.active}))},_startOpening:function(a){clearTimeout(this.timer),"true"===a.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(a)},this.delay))},_open:function(b){var c=a.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(b.parents(".ui-menu")).hide().attr("aria-hidden","true"),b.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(c)},collapseAll:function(b,c){clearTimeout(this.timer),this.timer=this._delay(function(){var d=c?this.element:a(b&&b.target).closest(this.element.find(".ui-menu"));d.length||(d=this.element),this._close(d),this.blur(b),this.activeMenu=d},this.delay)},_close:function(a){a||(a=this.active?this.active.parent():this.element),a.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active")},_closeOnDocumentClick:function(b){return!a(b.target).closest(".ui-menu").length},_isDivider:function(a){return!/[^\-\u2014\u2013\s]/.test(a.text())},collapse:function(a){var b=this.active&&this.active.parent().closest(".ui-menu-item",this.element);b&&b.length&&(this._close(),this.focus(a,b))},expand:function(a){var b=this.active&&this.active.children(".ui-menu ").find(this.options.items).first();b&&b.length&&(this._open(b.parent()),this._delay(function(){this.focus(a,b)}))},next:function(a){this._move("next","first",a)},previous:function(a){this._move("prev","last",a)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(a,b,c){var d;this.active&&(d="first"===a||"last"===a?this.active["first"===a?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[a+"All"](".ui-menu-item").eq(0)),d&&d.length&&this.active||(d=this.activeMenu.find(this.options.items)[b]()),this.focus(c,d)},nextPage:function(b){var c,d,e;return this.active?void(this.isLastItem()||(this._hasScroll()?(d=this.active.offset().top,e=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return c=a(this),c.offset().top-d-e<0}),this.focus(b,c)):this.focus(b,this.activeMenu.find(this.options.items)[this.active?"last":"first"]()))):void this.next(b)},previousPage:function(b){var c,d,e;return this.active?void(this.isFirstItem()||(this._hasScroll()?(d=this.active.offset().top,e=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return c=a(this),c.offset().top-d+e>0}),this.focus(b,c)):this.focus(b,this.activeMenu.find(this.options.items).first()))):void this.next(b)},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(b){this.active=this.active||a(b.target).closest(".ui-menu-item");var c={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(b,!0),this._trigger("select",b,c)},_filterMenuItems:function(b){var c=b.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&"),d=new RegExp("^"+c,"i");return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function(){return d.test(a.trim(a(this).text()))})}})});;
/** @namespace wp */
window.wp = window.wp || {};

( function ( wp, $ ) {
	'use strict';

	var $containerPolite,
		$containerAssertive,
		previousMessage = '';

	/**
	 * Update the ARIA live notification area text node.
	 *
	 * @since 4.2.0
	 * @since 4.3.0 Introduced the 'ariaLive' argument.
	 *
	 * @param {String} message    The message to be announced by Assistive Technologies.
	 * @param {String} [ariaLive] The politeness level for aria-live. Possible values:
	 *                            polite or assertive. Default polite.
	 * @returns {void}
	 */
	function speak( message, ariaLive ) {
		// Clear previous messages to allow repeated strings being read out.
		clear();

		// Ensure only text is sent to screen readers.
		message = $( '<p>' ).html( message ).text();

		/*
		 * Safari 10+VoiceOver don't announce repeated, identical strings. We use
		 * a `no-break space` to force them to think identical strings are different.
		 * See ticket #36853.
		 */
		if ( previousMessage === message ) {
			message = message + '\u00A0';
		}

		previousMessage = message;

		if ( $containerAssertive && 'assertive' === ariaLive ) {
			$containerAssertive.text( message );
		} else if ( $containerPolite ) {
			$containerPolite.text( message );
		}
	}

	/**
	 * Build the live regions markup.
	 *
	 * @since 4.3.0
	 *
	 * @param {String} ariaLive Optional. Value for the 'aria-live' attribute, default 'polite'.
	 *
	 * @return {Object} $container The ARIA live region jQuery object.
	 */
	function addContainer( ariaLive ) {
		ariaLive = ariaLive || 'polite';

		var $container = $( '<div>', {
			'id': 'wp-a11y-speak-' + ariaLive,
			'aria-live': ariaLive,
			'aria-relevant': 'additions text',
			'aria-atomic': 'true',
			'class': 'screen-reader-text wp-a11y-speak-region'
		});

		$( document.body ).append( $container );
		return $container;
	}

	/**
	 * Clear the live regions.
	 *
	 * @since 4.3.0
	 */
	function clear() {
		$( '.wp-a11y-speak-region' ).text( '' );
	}

	/**
	 * Initialize wp.a11y and define ARIA live notification area.
	 *
	 * @since 4.2.0
	 * @since 4.3.0 Added the assertive live region.
	 */
	$( document ).ready( function() {
		$containerPolite = $( '#wp-a11y-speak-polite' );
		$containerAssertive = $( '#wp-a11y-speak-assertive' );

		if ( ! $containerPolite.length ) {
			$containerPolite = addContainer( 'polite' );
		}

		if ( ! $containerAssertive.length ) {
			$containerAssertive = addContainer( 'assertive' );
		}
	});

	/** @namespace wp.a11y */
	wp.a11y = wp.a11y || {};
	wp.a11y.speak = speak;

}( window.wp, window.jQuery ));
;
/*!
 * jQuery UI Autocomplete 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/autocomplete/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./core","./widget","./position","./menu"],a):a(jQuery)}(function(a){return a.widget("ui.autocomplete",{version:"1.11.4",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},requestIndex:0,pending:0,_create:function(){var b,c,d,e=this.element[0].nodeName.toLowerCase(),f="textarea"===e,g="input"===e;this.isMultiLine=!!f||!g&&this.element.prop("isContentEditable"),this.valueMethod=this.element[f||g?"val":"text"],this.isNewMenu=!0,this.element.addClass("ui-autocomplete-input").attr("autocomplete","off"),this._on(this.element,{keydown:function(e){if(this.element.prop("readOnly"))return b=!0,d=!0,void(c=!0);b=!1,d=!1,c=!1;var f=a.ui.keyCode;switch(e.keyCode){case f.PAGE_UP:b=!0,this._move("previousPage",e);break;case f.PAGE_DOWN:b=!0,this._move("nextPage",e);break;case f.UP:b=!0,this._keyEvent("previous",e);break;case f.DOWN:b=!0,this._keyEvent("next",e);break;case f.ENTER:this.menu.active&&(b=!0,e.preventDefault(),this.menu.select(e));break;case f.TAB:this.menu.active&&this.menu.select(e);break;case f.ESCAPE:this.menu.element.is(":visible")&&(this.isMultiLine||this._value(this.term),this.close(e),e.preventDefault());break;default:c=!0,this._searchTimeout(e)}},keypress:function(d){if(b)return b=!1,void(this.isMultiLine&&!this.menu.element.is(":visible")||d.preventDefault());if(!c){var e=a.ui.keyCode;switch(d.keyCode){case e.PAGE_UP:this._move("previousPage",d);break;case e.PAGE_DOWN:this._move("nextPage",d);break;case e.UP:this._keyEvent("previous",d);break;case e.DOWN:this._keyEvent("next",d)}}},input:function(a){return d?(d=!1,void a.preventDefault()):void this._searchTimeout(a)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(a){return this.cancelBlur?void delete this.cancelBlur:(clearTimeout(this.searching),this.close(a),void this._change(a))}}),this._initSource(),this.menu=a("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({role:null}).hide().menu("instance"),this._on(this.menu.element,{mousedown:function(b){b.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur});var c=this.menu.element[0];a(b.target).closest(".ui-menu-item").length||this._delay(function(){var b=this;this.document.one("mousedown",function(d){d.target===b.element[0]||d.target===c||a.contains(c,d.target)||b.close()})})},menufocus:function(b,c){var d,e;return this.isNewMenu&&(this.isNewMenu=!1,b.originalEvent&&/^mouse/.test(b.originalEvent.type))?(this.menu.blur(),void this.document.one("mousemove",function(){a(b.target).trigger(b.originalEvent)})):(e=c.item.data("ui-autocomplete-item"),!1!==this._trigger("focus",b,{item:e})&&b.originalEvent&&/^key/.test(b.originalEvent.type)&&this._value(e.value),d=c.item.attr("aria-label")||e.value,void(d&&a.trim(d).length&&(this.liveRegion.children().hide(),a("<div>").text(d).appendTo(this.liveRegion))))},menuselect:function(a,b){var c=b.item.data("ui-autocomplete-item"),d=this.previous;this.element[0]!==this.document[0].activeElement&&(this.element.focus(),this.previous=d,this._delay(function(){this.previous=d,this.selectedItem=c})),!1!==this._trigger("select",a,{item:c})&&this._value(c.value),this.term=this._value(),this.close(a),this.selectedItem=c}}),this.liveRegion=a("<span>",{role:"status","aria-live":"assertive","aria-relevant":"additions"}).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(a,b){this._super(a,b),"source"===a&&this._initSource(),"appendTo"===a&&this.menu.element.appendTo(this._appendTo()),"disabled"===a&&b&&this.xhr&&this.xhr.abort()},_appendTo:function(){var b=this.options.appendTo;return b&&(b=b.jquery||b.nodeType?a(b):this.document.find(b).eq(0)),b&&b[0]||(b=this.element.closest(".ui-front")),b.length||(b=this.document[0].body),b},_initSource:function(){var b,c,d=this;a.isArray(this.options.source)?(b=this.options.source,this.source=function(c,d){d(a.ui.autocomplete.filter(b,c.term))}):"string"==typeof this.options.source?(c=this.options.source,this.source=function(b,e){d.xhr&&d.xhr.abort(),d.xhr=a.ajax({url:c,data:b,dataType:"json",success:function(a){e(a)},error:function(){e([])}})}):this.source=this.options.source},_searchTimeout:function(a){clearTimeout(this.searching),this.searching=this._delay(function(){var b=this.term===this._value(),c=this.menu.element.is(":visible"),d=a.altKey||a.ctrlKey||a.metaKey||a.shiftKey;b&&(!b||c||d)||(this.selectedItem=null,this.search(null,a))},this.options.delay)},search:function(a,b){return a=null!=a?a:this._value(),this.term=this._value(),a.length<this.options.minLength?this.close(b):this._trigger("search",b)!==!1?this._search(a):void 0},_search:function(a){this.pending++,this.element.addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:a},this._response())},_response:function(){var b=++this.requestIndex;return a.proxy(function(a){b===this.requestIndex&&this.__response(a),this.pending--,this.pending||this.element.removeClass("ui-autocomplete-loading")},this)},__response:function(a){a&&(a=this._normalize(a)),this._trigger("response",null,{content:a}),!this.options.disabled&&a&&a.length&&!this.cancelSearch?(this._suggest(a),this._trigger("open")):this._close()},close:function(a){this.cancelSearch=!0,this._close(a)},_close:function(a){this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",a))},_change:function(a){this.previous!==this._value()&&this._trigger("change",a,{item:this.selectedItem})},_normalize:function(b){return b.length&&b[0].label&&b[0].value?b:a.map(b,function(b){return"string"==typeof b?{label:b,value:b}:a.extend({},b,{label:b.label||b.value,value:b.value||b.label})})},_suggest:function(b){var c=this.menu.element.empty();this._renderMenu(c,b),this.isNewMenu=!0,this.menu.refresh(),c.show(),this._resizeMenu(),c.position(a.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var a=this.menu.element;a.outerWidth(Math.max(a.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(b,c){var d=this;a.each(c,function(a,c){d._renderItemData(b,c)})},_renderItemData:function(a,b){return this._renderItem(a,b).data("ui-autocomplete-item",b)},_renderItem:function(b,c){return a("<li>").text(c.label).appendTo(b)},_move:function(a,b){return this.menu.element.is(":visible")?this.menu.isFirstItem()&&/^previous/.test(a)||this.menu.isLastItem()&&/^next/.test(a)?(this.isMultiLine||this._value(this.term),void this.menu.blur()):void this.menu[a](b):void this.search(null,b)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(a,b){this.isMultiLine&&!this.menu.element.is(":visible")||(this._move(a,b),b.preventDefault())}}),a.extend(a.ui.autocomplete,{escapeRegex:function(a){return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(b,c){var d=new RegExp(a.ui.autocomplete.escapeRegex(c),"i");return a.grep(b,function(a){return d.test(a.label||a.value||a)})}}),a.widget("ui.autocomplete",a.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(a){return a+(a>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(b){var c;this._superApply(arguments),this.options.disabled||this.cancelSearch||(c=b&&b.length?this.options.messages.results(b.length):this.options.messages.noResults,this.liveRegion.children().hide(),a("<div>").text(c).appendTo(this.liveRegion))}}),a.ui.autocomplete});;
(function(){"use strict";function a(){}function b(a,b){for(var c=a.length;c--;)if(a[c].listener===b)return c;return-1}function c(a){return function(){return this[a].apply(this,arguments)}}var d=a.prototype,e=this,f=e.EventEmitter;d.getListeners=function(a){var b,c,d=this._getEvents();if("object"==typeof a){b={};for(c in d)d.hasOwnProperty(c)&&a.test(c)&&(b[c]=d[c])}else b=d[a]||(d[a]=[]);return b},d.flattenListeners=function(a){var b,c=[];for(b=0;b<a.length;b+=1)c.push(a[b].listener);return c},d.getListenersAsObject=function(a){var b,c=this.getListeners(a);return c instanceof Array&&(b={},b[a]=c),b||c},d.addListener=function(a,c){var d,e=this.getListenersAsObject(a),f="object"==typeof c;for(d in e)e.hasOwnProperty(d)&&-1===b(e[d],c)&&e[d].push(f?c:{listener:c,once:!1});return this},d.on=c("addListener"),d.addOnceListener=function(a,b){return this.addListener(a,{listener:b,once:!0})},d.once=c("addOnceListener"),d.defineEvent=function(a){return this.getListeners(a),this},d.defineEvents=function(a){for(var b=0;b<a.length;b+=1)this.defineEvent(a[b]);return this},d.removeListener=function(a,c){var d,e,f=this.getListenersAsObject(a);for(e in f)f.hasOwnProperty(e)&&(d=b(f[e],c),-1!==d&&f[e].splice(d,1));return this},d.off=c("removeListener"),d.addListeners=function(a,b){return this.manipulateListeners(!1,a,b)},d.removeListeners=function(a,b){return this.manipulateListeners(!0,a,b)},d.manipulateListeners=function(a,b,c){var d,e,f=a?this.removeListener:this.addListener,g=a?this.removeListeners:this.addListeners;if("object"!=typeof b||b instanceof RegExp)for(d=c.length;d--;)f.call(this,b,c[d]);else for(d in b)b.hasOwnProperty(d)&&(e=b[d])&&("function"==typeof e?f.call(this,d,e):g.call(this,d,e));return this},d.removeEvent=function(a){var b,c=typeof a,d=this._getEvents();if("string"===c)delete d[a];else if("object"===c)for(b in d)d.hasOwnProperty(b)&&a.test(b)&&delete d[b];else delete this._events;return this},d.removeAllListeners=c("removeEvent"),d.emitEvent=function(a,b){var c,d,e,f,g=this.getListenersAsObject(a);for(e in g)if(g.hasOwnProperty(e))for(d=g[e].length;d--;)c=g[e][d],c.once===!0&&this.removeListener(a,c.listener),f=c.listener.apply(this,b||[]),f===this._getOnceReturnValue()&&this.removeListener(a,c.listener);return this},d.trigger=c("emitEvent"),d.emit=function(a){var b=Array.prototype.slice.call(arguments,1);return this.emitEvent(a,b)},d.setOnceReturnValue=function(a){return this._onceReturnValue=a,this},d._getOnceReturnValue=function(){return!this.hasOwnProperty("_onceReturnValue")||this._onceReturnValue},d._getEvents=function(){return this._events||(this._events={})},a.noConflict=function(){return e.EventEmitter=f,a},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return a}):"object"==typeof module&&module.exports?module.exports=a:this.EventEmitter=a}).call(this),function(a){function b(b){var c=a.event;return c.target=c.target||c.srcElement||b,c}var c=document.documentElement,d=function(){};c.addEventListener?d=function(a,b,c){a.addEventListener(b,c,!1)}:c.attachEvent&&(d=function(a,c,d){a[c+d]=d.handleEvent?function(){var c=b(a);d.handleEvent.call(d,c)}:function(){var c=b(a);d.call(a,c)},a.attachEvent("on"+c,a[c+d])});var e=function(){};c.removeEventListener?e=function(a,b,c){a.removeEventListener(b,c,!1)}:c.detachEvent&&(e=function(a,b,c){a.detachEvent("on"+b,a[b+c]);try{delete a[b+c]}catch(d){a[b+c]=void 0}});var f={bind:d,unbind:e};"function"==typeof define&&define.amd?define("eventie/eventie",f):a.eventie=f}(this),function(a,b){"use strict";"function"==typeof define&&define.amd?define(["eventEmitter/EventEmitter","eventie/eventie"],function(c,d){return b(a,c,d)}):"object"==typeof module&&module.exports?module.exports=b(a,require("wolfy87-eventemitter"),require("eventie")):a.imagesLoaded=b(a,a.EventEmitter,a.eventie)}(window,function(a,b,c){function d(a,b){for(var c in b)a[c]=b[c];return a}function e(a){return"[object Array]"==l.call(a)}function f(a){var b=[];if(e(a))b=a;else if("number"==typeof a.length)for(var c=0;c<a.length;c++)b.push(a[c]);else b.push(a);return b}function g(a,b,c){if(!(this instanceof g))return new g(a,b,c);"string"==typeof a&&(a=document.querySelectorAll(a)),this.elements=f(a),this.options=d({},this.options),"function"==typeof b?c=b:d(this.options,b),c&&this.on("always",c),this.getImages(),j&&(this.jqDeferred=new j.Deferred);var e=this;setTimeout(function(){e.check()})}function h(a){this.img=a}function i(a,b){this.url=a,this.element=b,this.img=new Image}var j=a.jQuery,k=a.console,l=Object.prototype.toString;g.prototype=new b,g.prototype.options={},g.prototype.getImages=function(){this.images=[];for(var a=0;a<this.elements.length;a++){var b=this.elements[a];this.addElementImages(b)}},g.prototype.addElementImages=function(a){"IMG"==a.nodeName&&this.addImage(a),this.options.background===!0&&this.addElementBackgroundImages(a);var b=a.nodeType;if(b&&m[b]){for(var c=a.querySelectorAll("img"),d=0;d<c.length;d++){var e=c[d];this.addImage(e)}if("string"==typeof this.options.background){var f=a.querySelectorAll(this.options.background);for(d=0;d<f.length;d++){var g=f[d];this.addElementBackgroundImages(g)}}}};var m={1:!0,9:!0,11:!0};g.prototype.addElementBackgroundImages=function(a){for(var b=n(a),c=/url\(['"]*([^'"\)]+)['"]*\)/gi,d=c.exec(b.backgroundImage);null!==d;){var e=d&&d[1];e&&this.addBackground(e,a),d=c.exec(b.backgroundImage)}};var n=a.getComputedStyle||function(a){return a.currentStyle};return g.prototype.addImage=function(a){var b=new h(a);this.images.push(b)},g.prototype.addBackground=function(a,b){var c=new i(a,b);this.images.push(c)},g.prototype.check=function(){function a(a,c,d){setTimeout(function(){b.progress(a,c,d)})}var b=this;if(this.progressedCount=0,this.hasAnyBroken=!1,!this.images.length)return void this.complete();for(var c=0;c<this.images.length;c++){var d=this.images[c];d.once("progress",a),d.check()}},g.prototype.progress=function(a,b,c){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!a.isLoaded,this.emit("progress",this,a,b),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,a),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&k&&k.log("progress: "+c,a,b)},g.prototype.complete=function(){var a=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emit(a,this),this.emit("always",this),this.jqDeferred){var b=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[b](this)}},h.prototype=new b,h.prototype.check=function(){var a=this.getIsImageComplete();return a?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,c.bind(this.proxyImage,"load",this),c.bind(this.proxyImage,"error",this),c.bind(this.img,"load",this),c.bind(this.img,"error",this),void(this.proxyImage.src=this.img.src))},h.prototype.getIsImageComplete=function(){return this.img.complete&&void 0!==this.img.naturalWidth},h.prototype.confirm=function(a,b){this.isLoaded=a,this.emit("progress",this,this.img,b)},h.prototype.handleEvent=function(a){var b="on"+a.type;this[b]&&this[b](a)},h.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},h.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},h.prototype.unbindEvents=function(){c.unbind(this.proxyImage,"load",this),c.unbind(this.proxyImage,"error",this),c.unbind(this.img,"load",this),c.unbind(this.img,"error",this)},i.prototype=new h,i.prototype.check=function(){c.bind(this.img,"load",this),c.bind(this.img,"error",this),this.img.src=this.url;var a=this.getIsImageComplete();a&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},i.prototype.unbindEvents=function(){c.unbind(this.img,"load",this),c.unbind(this.img,"error",this)},i.prototype.confirm=function(a,b){this.isLoaded=a,this.emit("progress",this,this.element,b)},g.makeJQueryPlugin=function(b){b=b||a.jQuery,b&&(j=b,j.fn.imagesLoaded=function(a,b){var c=new g(this,a,b);return c.jqDeferred.promise(j(this))})},g.makeJQueryPlugin(),g});;
(function($){
    'use strict';

    var emailoctopus = {
        debug: window.location.href.indexOf('eoDebug=1') !== -1,
        isBotPost: function($form) {
            return $form.find('.emailoctopus-form-row-hp input').val();
        },
        consentRequired: function ($form) {
            var $checkbox = $form.find('input.emailoctopus-consent');

            return $checkbox.length && !$checkbox.is(':checked');
        },
        basicValidateEmail: function(email) {
            return /\S+@\S+\.\S+/.test(email);
        },
        ajaxSuccess: function($form) {
            $form.trigger('emailoctopus.success');

            var successRedirectUrl = $form.find('.emailoctopus-success-redirect-url').val();

            if (successRedirectUrl && successRedirectUrl.trim()) {
                // Redirect
                if (emailoctopus.debug) {
                    console.log('EmailOctopus: redirecting to ' + successRedirectUrl);
                }
                window.location.href = successRedirectUrl;
            } else {
                // Show confirmation
                if (emailoctopus.debug) {
                    console.log('EmailOctopus: no redirect URL found, showing confirmation');
                }
                $form.siblings('.emailoctopus-success-message').text(
                    emailoctopus_message.success
                );
                $form.hide();
            }
        },
        ajaxError: function($form, textStatus) {
            var response = $.parseJSON(textStatus.responseText);
            var $errorMessage = $form.siblings('.emailoctopus-error-message');

            if (response && response.error && response.error.code) {
                switch(response.error.code) {
                    case 'INVALID_PARAMETERS':
                        $errorMessage.text(
                            emailoctopus_message.invalid_parameters_error
                        );

                        return;
                    case 'BOT_SUBMISSION':
                        $errorMessage.text(
                            emailoctopus_message.bot_submission_error
                        );

                        return;
                }
            }

            $errorMessage.text(
                emailoctopus_message.unknown_error
            );

            $form.find(':submit').removeAttr('disabled');
        },
        ajaxSubmit: function($form) {
            $form.find(':submit').attr('disabled', true);

            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function() {
                    if (emailoctopus.debug) {
                        console.log('EmailOctopus: posted');
                    }
                    emailoctopus.ajaxSuccess($form);
                },
                error: function(textStatus) {
                    if (emailoctopus.debug) {
                        console.log('EmailOctopus: error while posting');
                    }
                    emailoctopus.ajaxError($form, textStatus);
                },
            });
        }
    };

    $(function () {
        if (emailoctopus.debug) {
            if (window.jQuery === undefined) {
                console.log('EmailOctopus: error, no jQuery');
            }

            var $form = $('.emailoctopus-form');

            if (!$form.length) {
                console.log('EmailOctopus: error, form missing');
            }

            if (!$form.siblings('.emailoctopus-error-message').length) {
                console.log('EmailOctopus: error, form missing error message section');
            }

            if (!$form.find('.emailoctopus-email-address').length) {
                console.log('EmailOctopus: error, form missing email address field');
            }
        }

        $('.emailoctopus-form:not(.bound)').submit(function () {
            if (emailoctopus.debug) {
                console.log('EmailOctopus: form submitted');
            }

            var $form = $(this);
            var $errorMessage = $form.siblings('.emailoctopus-error-message');
            var emailAddress = $form.find('.emailoctopus-email-address').val();

            $errorMessage.empty();

            if (emailoctopus.isBotPost($form)) {
                if (emailoctopus.debug) {
                    console.log('EmailOctopus: error, is bot post');
                }

                $errorMessage.text(
                    emailoctopus_message.bot_submission_error
                );
            } else if (!$.trim(emailAddress)) {
                if (emailoctopus.debug) {
                    console.log('EmailOctopus: error, missing email address');
                }

                $errorMessage.text(
                    emailoctopus_message.missing_email_address_error
                );
            } else if (!emailoctopus.basicValidateEmail(emailAddress)) {
                if (emailoctopus.debug) {
                    console.log('EmailOctopus: error, invalid email address');
                }

                $errorMessage.text(
                    emailoctopus_message.invalid_email_address_error
                );
            } else if (emailoctopus.consentRequired($form)) {
                if (emailoctopus.debug) {
                    console.error('EmailOctopus: error, consent required');
                }

                $errorMessage.text(emailoctopus_message.consent_required_error);
            } else {
                if (emailoctopus.debug) {
                    console.log('EmailOctopus: posting');
                }

                emailoctopus.ajaxSubmit($form);
            }

            return false;
        })
        // Mitigate duplicate bindings, in case this script is included multiple
        // times. More reliable than running 'unbind' or 'off' first, which doesn't
        // work if jQuery is also included multiple times.
        .addClass('bound');
    });
})(jQuery);
;
