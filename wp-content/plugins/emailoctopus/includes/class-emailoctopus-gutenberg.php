<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

/**
 * Gutenberg class for EmailOctopus
 *
 * @category EmailOctopus
 * @package  EmailOctopus
 * @author   EmailOctopus <tom@emailoctopus.com>
 * @license  GPL-2.0+
 * @link     https://emailoctopus.com
 *
 * @since 2.0.0
 */
class EmailOctopus_Gutenberg
{
    public function __construct()
    {
        if (!function_exists('register_block_type')) {
            return;
        }

        add_action('init', array($this, 'register_block'));
        add_action('enqueue_block_assets', array($this, 'add_gutenberg_styles'));
        add_action('enqueue_block_editor_assets', array($this,'add_gutenberg_scripts'));
        add_action('rest_api_init', array($this,'register_routes'));
    }

    public function register_routes()
    {
        register_rest_route('emailoctopus/v1', '/forms/', array(
            'methods' => 'GET',
            'callback' => array($this, 'rest_get_form_data'),
            'sanitize_callback' => 'absint',
            'validate_callback' => 'absint',
            'permission_callback' => array($this, 'rest_check_permissions'),
        ));
        register_rest_route('emailoctopus/v1', '/form/(?P<id>\d+)', array(
            'methods' => 'GET',
            'callback' => array($this, 'rest_get_form'),
            'sanitize_callback' => 'absint',
            'validate_callback' => 'absint',
            'permission_callback' => array($this, 'rest_check_permissions'),
        ));
    }

    public function rest_check_permissions()
    {
        if (current_user_can('edit_posts')) {
            return true;
        }

        return false;
    }

    public function rest_get_form($request)
    {
        $form_id = absint($request['id']);
        $custom_fields = EmailOctopus_Utils::get_custom_fields($form_id);
        $form_meta = EmailOctopus_Utils::get_all_form_meta($form_id);
        $form = EmailOctopus_Utils::get_form($form_id);

        wp_send_json(array('custom_fields' => $custom_fields, 'meta' => $form_meta, 'form' => $form));
    }

    public function rest_get_form_data()
    {
        wp_send_json(EmailOctopus_Utils::get_forms());
    }

    public function register_block()
    {
        register_block_type('emailoctopus/form', array(
            'attributes' => array(
                'form_id' => array(
                    'form_id' => 'number',
                    'default' => 0
                ),
                'show_title' => array(
                    'show_title' => 'bool',
                    'default' => true
                ),
                'show_summary' => array(
                    'show_summary' => 'bool',
                    'default' => true
                ),
                'branding' => array(
                    'branding' => 'bool',
                    'default' => true
                ),
                'consent_checkbox' => array(
                    'consent_checkbox' => 'bool',
                    'default' => false
                ),
                'appearance' => array(
                    'appearance' => 'string',
                    'default' => 'default'
                ),
                'background_color' => array(
                    'background_color' => 'string',
                    'default' => '#ebebeb'
                ),
                'text_color' => array(
                    'text_color' => 'string',
                    'default' => '#000000'
                ),
                'button_color' => array(
                    'button_color' => 'string',
                    'default' => 'inherit'
                ),
                'button_text_color' => array(
                    'button_text_color' => 'string',
                    'default' => '#000000'
                ),
                'override_appearance' => array(
                    'override_appearance' => 'bool',
                    'default' => false
                ),
                'override_consent' => array(
                    'override_consent' => 'bool',
                    'default' => false
                ),
                'override_branding' => array(
                    'override_branding' => 'bool',
                    'default' => false
                )
            ),
            'render_callback' => array($this, 'display_form'),
            'editor_script'   => 'emailoctopus_gutenberg'
        ));
    }

    public function display_form($attributes)
    {
        $form_id = $attributes['form_id'];
        $form_data = EmailOctopus_Utils::get_form_data($form_id);
        if (!empty($form_data)):
            $show_description = $attributes['show_summary'] ? 'true' : 'false';
        $appearance = $attributes['override_appearance'] ? $attributes['appearance'] : EmailOctopus_Utils::get_form_meta($form_id, 'appearance');
        $consent = $attributes['override_consent'] ? $attributes['consent_checkbox'] : EmailOctopus_Utils::get_form_meta($form_id, 'consent_checkbox');
        $redirect = EmailOctopus_Utils::get_form_meta($form_id, 'redirect_checkbox');
        $redirect_url = EmailOctopus_Utils::get_form_meta($form_id, 'redirect_url');
        $consent_content = EmailOctopus_Utils::get_form_meta($form_id, 'consent_content');
        $classname = EmailOctopus_Utils::get_form_meta($form_id, 'form_class');
        $branding = $attributes['override_branding'] ? $attributes['branding'] : EmailOctopus_Utils::get_form_meta($form_id, 'branding');
        $button_color = $attributes['button_color'];
        $button_text_color = $attributes['button_text_color'];
        $custom_fields = EmailOctopus_Utils::get_custom_fields($form_id);
        $list_id = $form_data['list_id'];
        $show_title = $attributes['show_title'] ? 'true' : 'false';

        // Get Messages
        $message_submit = EmailOctopus_Utils::get_form_meta($form_id, 'message_submit');
        $message_success = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_success'));
        $message_missing_email = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_missing_email'));
        $message_invalid_email = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_invalid_email'));
        $message_bot = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_bot'));
        $message_consent_required = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_consent_required'));
        $message_unknown = apply_filters('emailoctopus_the_content', EmailOctopus_Utils::get_form_meta($form_id, 'message_unknown'));

        // Get class names and allow them to be filterable
        $main_class_names = array('emailoctopus-form-wrapper');

        // Adding custom styles
        $main_class_styles = array();
        switch ($appearance) {
            case 'custom':
                $main_class_names[] = 'emailoctopus-custom-colors';
                $main_class_styles[] = sprintf(
                    'background: %s !important; ',
                    esc_attr($attributes['background_color'])
                );
                $main_class_styles[] = sprintf(
                    'color: %s !important;',
                    esc_attr($attributes['text_color'])
                );
                break;
            case 'light':
                $main_class_names[] = 'emailoctopus-theme-light';
                break;
            case 'dark':
                $main_class_names[] = 'emailoctopus-theme-dark';
                break;
        }
        if (!empty($classname)) {
            $classname = explode(' ', $classname);
            foreach ($classname as $class) {
                $main_class_names[] = $class;
            }
        }
        $tabindex = 200;
        ob_start();
        include EmailOctopus::get_plugin_dir('templates/form.php');

        return ob_get_clean();
        endif;
    }

    public function add_gutenberg_scripts()
    {
        // Ensure script debug allows non-minified scripts
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

        wp_enqueue_script('emailoctopus_gutenberg', EmailOctopus::get_plugin_url('js/gutenberg'.$is_minified.'.js'), array('wp-blocks', 'wp-element'), EMAILOCTOPUS_VERSION, true);

        /* For the Gutenberg plugin */
        if (function_exists('gutenberg_get_jed_locale_data')) {
            $locale  = gutenberg_get_jed_locale_data('emailoctopus');
            $content = 'wp.i18n.setLocaleData( ' . json_encode($locale) . ', "emailoctopus" );';
            wp_script_add_data('emailoctopus_gutenberg', 'data', $content);
        } elseif (function_exists('wp_get_jed_locale_data')) {
            /* for 5.0 */
            $locale  = wp_get_jed_locale_data('emailoctopus');
            $content = 'wp.i18n.setLocaleData( ' . json_encode($locale) . ', "emailoctopus" );';
            wp_script_add_data('emailoctopus_gutenberg', 'data', $content);
        }

        // Pass in REST URL
        wp_localize_script(
            'emailoctopus_gutenberg',
            'emailoctopus_gutenberg',
            array(
                'rest_url' => esc_url(rest_url('emailoctopus/v1')),
                'svg' => EmailOctopus_Utils::get_svg(),
                'svg_large' => EmailOctopus_Utils::get_svg('large'),
                'nonce' => wp_create_nonce('wp_rest'),
                'loading' => __('Loading...', 'emailoctopus')
            )
        );
    }

    public function add_gutenberg_styles()
    {
        // Ensure script debug allows non-minified scripts
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';
        wp_enqueue_style('emailoctopus_admin_styles', EmailOctopus::get_plugin_url('css/front-end'.$is_minified.'.css'), array(), EMAILOCTOPUS_VERSION);
    }
}
