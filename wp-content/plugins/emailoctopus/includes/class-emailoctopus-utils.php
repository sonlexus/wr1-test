<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

/**
 * Utility class for EmailOctopus.
 *
 * Utility class for EmailOctopus.
 *
 * @category EmailOctopus
 * @package  EmailOctopus
 * @author   EmailOctopus <contact@emailoctopus.com>
 * @license  GPL-2.0+
 * @link     https://emailoctopus.com
 *
 * @since 2.0.0
 */
class EmailOctopus_Utils
{
    /**
     * Checks for a valid API key
     *
     * @since  2.0.0
     * @access public
     *
     * @param string $api_key The API key to check
     * @param bool   Whether to force refresh the API check
     *
     * @return bool Whether the API key is valid or not
     */
    public static function is_valid_api_key($api_key, $force_refresh = false)
    {
        if (!$api_key) {
            return false;
        }

        if ($force_refresh) {
            delete_transient('emailoctopus_api_key_status');
            delete_option('emailoctopus_api_key');
        }

        // Check transient for api key status
        $saved_api_key_status = get_transient('emailoctopus_api_key_status', false);
        if ($saved_api_key_status && $saved_api_key_status === 'connected') {
            return true;
        }

        // Format API call
        $api_endpoint = 'https://emailoctopus.com/api/1.5/lists';
        $api_url = add_query_arg(
            array(
                'api_key' => $api_key,
                'limit' => 100,
                'page' => 1,
            ),
            $api_endpoint
        );
        $api_response = wp_remote_get($api_url);
        if (!is_wp_error($api_response)) {
            $body = json_decode(wp_remote_retrieve_body($api_response));
            if (isset($body->error)) {
                return false;
            }
            set_transient(
                'emailoctopus_api_key_status',
                'connected',
                6 * HOUR_IN_SECONDS
            );
            update_option('emailoctopus_api_key', $api_key);

            return true;
        }

        return false;
    }

    /**
     * Gets a list of the table names for the plugin
     *
     * @since  2.0.0
     * @access public
     *
     * @return array $tables Tables of the form
     */
    public static function get_table_names()
    {
        global $wpdb;

        $tables = array();
        $tables[] = $wpdb->prefix . 'emailoctopus_forms';
        $tables[] = $wpdb->prefix . 'emailoctopus_forms_meta';
        $tables[] = $wpdb->prefix . 'emailoctopus_custom_fields';

        return $tables;
    }

    /**
     * Get a piece of form meta
     *
     * @since  2.0.0
     * @access public
     *
     * @param int    $form_id  The form ID
     * @param string $meta_key The meta key to retrieve the value for
     *
     * @return mixed false, single meta value (string), or array of values
     */
    public static function get_form_meta($form_id, $meta_key)
    {
        // Make sure form ID is valid
        if (!is_numeric($form_id)) {
            return false;
        }
        $form_id = absint($form_id);
        if (!$form_id) {
            return false;
        }

        // Get cache
        $meta_cache = wp_cache_get($form_id, 'emailoctopus_meta_key_results');

        if (!$meta_cache) {
            global $wpdb;
            $meta_table_name = $wpdb->prefix . 'emailoctopus_forms_meta';
            $query = $wpdb->prepare(
                "SELECT * FROM {$meta_table_name} WHERE form_id = %d",
                $form_id
            );
            $results = $wpdb->get_results($query);
            $meta_value = '';
            foreach ($results as $index => $values) {
                if ($values->meta_key === $meta_key) {
                    $meta_value = $values->meta_value;

                    break;
                }
            }
            if (empty($meta_value)) {
                return false;
            }
            wp_cache_set($form_id, $results, 'emailoctopus_meta_key_results');

            return $meta_value;
        } else {
            foreach ($meta_cache as $index => $values) {
                if ($values->meta_key === $meta_key) {
                    $meta_value = $values->meta_value;

                    return $meta_value;

                    break;
                }
            }
        }

        return false;
    }

    /**
     * Get aal form meta
     *
     * @since  2.0.0
     * @access public
     *
     * @param int    $form_id  The form ID
     *
     * @return array the meta values
     */
    public static function get_all_form_meta($form_id)
    {
        // Make sure form ID is valid
        if (!is_numeric($form_id)) {
            return false;
        }
        $form_id = absint($form_id);
        if (!$form_id) {
            return false;
        }

        // Get cache
        $meta_cache = wp_cache_get($form_id, 'emailoctopus_meta_all');

        if (!$meta_cache) {
            global $wpdb;
            $meta_table_name = $wpdb->prefix . 'emailoctopus_forms_meta';
            $query = $wpdb->prepare(
                "SELECT * FROM {$meta_table_name} WHERE form_id = %d",
                $form_id
            );
            $results = $wpdb->get_results($query);
            wp_cache_set($form_id, $results, 'emailoctopus_meta_all');

            $meta_cache = $results;
        }

        return $meta_cache;
    }

    /**
     * Get form data
     *
     * @since  2.0.0
     * @access public
     *
     * @param int    $form_id  The form ID
     *
     * @return mixed false, single meta value (string), or array of values
     */
    public static function get_form_data($form_id)
    {
        // Make sure form ID is valid
        if (!is_numeric($form_id)) {
            return false;
        }
        $form_id = absint($form_id);
        if (!$form_id) {
            return false;
        }

        // Get cache
        $meta_cache = wp_cache_get($form_id, 'emailoctopus_form_table_results');

        if (!$meta_cache) {
            global $wpdb;
            $form_table_name = $wpdb->prefix . 'emailoctopus_forms';
            $query = $wpdb->prepare(
                "SELECT * FROM {$form_table_name} WHERE form_id = %d",
                $form_id
            );
            $results = $wpdb->get_results($query, ARRAY_A);
            if (empty($results)) {
                return false;
            }
            wp_cache_set($form_id, $results[0], 'emailoctopus_form_table_results');

            return $results[0];
        } else {
            return $meta_cache;
        }

        return false;
    }

    /**
     * Get form data
     *
     * @since  2.0.0
     * @access public
     *
     * @return mixed false, single meta value (string), or array of values
     */
    public static function get_forms()
    {
        // Get cache
        $meta_cache = wp_cache_get('emailoctopus_forms', 'emailoctopus_form_table_all');

        if (!$meta_cache) {
            global $wpdb;
            $form_table_name = $wpdb->prefix . 'emailoctopus_forms';
            $query = "SELECT * FROM {$form_table_name} ORDER BY `form_id` ASC";
            $results = $wpdb->get_results($query, ARRAY_A);
            if (empty($results)) {
                return false;
            }
            wp_cache_set('emailoctopus_forms', $results, 'emailoctopus_form_table_all');

            return $results;
        } else {
            return $meta_cache;
        }

        return false;
    }
    /*
    * Get form
    *
    * @since  2.0.0
    * @access public
    * @param int $form_id get form ID
    *
    * @return mixed false, single meta value (string), or array of values
    */
    public static function get_form($form_id)
    {
        // Get cache
        $meta_cache = wp_cache_get('emailoctopus_forms', 'emailoctopus_form_table_entry');

        if (!$meta_cache) {
            global $wpdb;
            $form_table_name = $wpdb->prefix . 'emailoctopus_forms';
            $query = sprintf("SELECT * FROM {$form_table_name} where 1=1 AND form_id = %d order by `form_id` ASC", absint($form_id));
            $results = $wpdb->get_results($query, ARRAY_A);
            if (empty($results)) {
                return false;
            }
            wp_cache_set('emailoctopus_forms', $results, 'emailoctopus_form_table_entry');

            return $results;
        } else {
            return $meta_cache;
        }

        return false;
    }

    /**
     * Get form custom fields
     *
     * @since  2.0.0
     * @access public
     *
     * @param int    $form_id  The form ID
     *
     * @return mixed false, single meta value (string), or array of values
     */
    public static function get_custom_fields($form_id)
    {
        // Make sure form ID is valid
        if (!is_numeric($form_id)) {
            return false;
        }
        $form_id = absint($form_id);
        if (!$form_id) {
            return false;
        }

        // Get cache
        $meta_cache = wp_cache_get($form_id, 'emailoctopus_custom_fields_results');

        if (!$meta_cache) {
            global $wpdb;
            $table = $wpdb->prefix . 'emailoctopus_custom_fields';
            $query = $wpdb->prepare("SELECT * FROM {$table} where form_id = %d order by `order` ASC", $form_id);
            $results = $wpdb->get_results($query, ARRAY_A);
            if (empty($results)) {
                return false;
            }
            wp_cache_set($form_id, $results, 'emailoctopus_custom_fields_results');

            return $results;
        } else {
            return $meta_cache;
        }

        return false;
    }

    /**
     * Deletes a form
     *
     * @since  2.0.0
     * @access public
     *
     * @param string $form_ids Form ID to delete
     * @param string $nonce    Nonce field
     * @param string $action   Nonce actionto check
     *
     * @return void
     */
    public static function delete_forms($form_ids, $nonce, $action = 'delete_emailoctopus_form')
    {
        if (!wp_verify_nonce($nonce, $action)) {
            wp_die('This link has expired.');
        }
        if (!current_user_can('manage_options')) {
            wp_die('You do not have sufficient privileges to delete forms.');
        }

        $form_ids = explode(',', $form_ids);

        // Delete the form
        global $wpdb;
        $tables = self::get_table_names();
        foreach ($form_ids as $form_id) {
            foreach ($tables as $table) {
                $query = "DELETE FROM {$table} where form_id = %d";
                $query = $wpdb->prepare($query, $form_id);
                $wpdb->query($query);
            }
        }
    }

    /**
     * Gets a list from the EmailOctopus API
     *
     * @since  2.0.0
     * @access public
     *
     * @param string $api_key The API key to check
     * @param bool   Whether to force refresh the API check
     *
     * @return array List content if successful
     */
    public static function get_list()
    {
        $api_key = get_option('emailoctopus_api_key', false);
        $saved_api_key_status = get_transient('emailoctopus_api_key_status', false);

        if ($api_key && $saved_api_key_status === 'connected') {
            // Format API call
            $api_endpoint = 'https://emailoctopus.com/api/1.5/lists';
            $api_url = add_query_arg(
                array(
                    'api_key' => $api_key,
                    'limit' => 100,
                    'page' => 1,
                ),
                $api_endpoint
            );
            $api_response = wp_remote_get($api_url);
            if (!is_wp_error($api_response)) {
                $body = json_decode(wp_remote_retrieve_body($api_response));
                if (isset($body->error)) {
                    return array();
                }

                return $body;
            }
        } else {
            return array();
        }
    }

    /**
     * Returns SVG for plugin
     *
     * @since  2.0.0
     * @access public
     *
     * @param string $size of the SVG - icon will return small SVG, anyhing else will return large svg
     * @return SVG Code
     */
    public static function get_svg($size = 'icon')
    {
        if ($size === 'icon') {
            return EmailOctopus::get_plugin_url('/images/admin-menu-logo.svg');
        }

        return EmailOctopus::get_plugin_url('/images/logo.svg');
    }
}
