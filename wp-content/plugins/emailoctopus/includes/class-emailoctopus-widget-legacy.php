<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

class EmailOctopus_Widget_Legacy extends WP_Widget
{
    /**
     * Unique identifier for the widget, used as the text domain when
     * internationalizing strings of text.
     *
     * @var string
     */
    protected $widget_slug = 'emailoctopus';

    /**
     * Specifies the classname and description, instantiates the widget, loads
     * localization files, and includes necessary stylesheets and JavaScript.
     */
    public function __construct()
    {
        // Load plugin text domain
        // Instantiate main class
        parent::__construct(
            $this->get_widget_slug(),
            __('EmailOctopus (Legacy)', 'emailoctopus'),
            array(
                'classname'  => $this->get_widget_slug().'-class',
                'description' => __(
                    'A form for subscribing to an EmailOctopus list.',
                    'emailoctopus'
                ),
            )
        );

        // Register admin styles and scripts
        add_action(
            'admin_echo_styles',
            array($this, 'register_admin_styles')
        );
        add_action(
            'admin_enqueue_scripts',
            array($this, 'register_admin_scripts')
        );

        // Register site styles and scripts
        add_action(
            'wp_enqueue_scripts',
            array($this, 'register_widget_styles')
        );
        add_action(
            'wp_enqueue_scripts',
            array($this, 'register_widget_scripts')
        );
    }


    /**
     * Returns the widget slug.
     *
     * @return Plugin slug.
     */
    public function get_widget_slug()
    {
        return $this->widget_slug;
    }

    /**
     * Outputs the content of the widget.
     *
     * @param array args     The array of form elements
     * @param array instance The current instance of the widget
     */
    public function widget($args, $instance)
    {
        // Check if there is a cached output
        $cache = wp_cache_get($this->get_widget_slug(), 'widget');

        if (!is_array($cache)) {
            $cache = array();
        }

        if (isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        if (isset($cache[$args['widget_id']])) {
            return print $cache[$args['widget_id']];
        }

        extract($args, EXTR_SKIP);

        /**
         * Handle any options set in an older version of the plugin, where the
         * user has not yet updated them.
         */
        if (array_key_exists('include_name_fields', $instance)) {
            $instance['include_first_name_field'] =
                $instance['include_name_fields'];
            $instance['include_last_name_field'] =
                $instance['include_name_fields'];
        }

        $widget_output = $before_widget;
        ob_start();
        include EmailOctopus::get_plugin_dir('templates/widget_legacy.php');
        $widget_output .= ob_get_clean();
        $widget_output .= $after_widget;

        $cache[$args['widget_id']] = $widget_output;

        wp_cache_set($this->get_widget_slug(), $cache, 'widget');

        echo $widget_output;
    }

    /**
     * Flushes the widget's cache.
     */
    public function flush_widget_cache()
    {
        wp_cache_delete($this->get_widget_slug(), 'widget');
    }

    /**
     * Processes the widget's options to be saved.
     *
     * @param array new_instance The new instance of values to be generated via the update.
     * @param array old_instance The previous instance of values before the update.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array(
            'title' => strip_tags($new_instance['title']),
            'list_id' => strip_tags($new_instance['list_id']),
            'include_first_name_field' =>
                filter_var($new_instance['include_first_name_field'], FILTER_VALIDATE_BOOLEAN),
            'include_last_name_field' =>
                filter_var($new_instance['include_last_name_field'], FILTER_VALIDATE_BOOLEAN),
            'include_referral' => (bool)$new_instance['include_referral'],
            'success_redirect_url' =>
                filter_var($new_instance['success_redirect_url'], FILTER_VALIDATE_BOOLEAN),
            'include_consent' =>
                filter_var($new_instance['include_consent'], FILTER_VALIDATE_BOOLEAN),
            'redirect_on_success' =>
                filter_var($new_instance['redirect_on_success'], FILTER_VALIDATE_BOOLEAN),
            'consent_message' => strip_tags($new_instance['consent_message']),
        );

        if (
            isset($new_instance['redirect_on_success']) &&
            $instance['redirect_on_success'] === true
        ) {
            $url = $new_instance['success_redirect_url'];

            if (empty($url)) {
                add_settings_error(
                    'success_redirect_url',
                    'success-redirect-url-missing',
                    'To redirect your subscriber, a URL is required'
                );
                $instance['success_redirect_url'] = '';
            } elseif (!filter_var($url, FILTER_VALIDATE_URL)) {
                add_settings_error(
                    'success_redirect_url',
                    'success-redirect-url-invalid',
                    'To redirect your subscriber, a full and valid URL is required, e.g. https://yoururl.com'
                );
                $instance['success_redirect_url'] = '';
            } else {
                $instance['success_redirect_url'] = $new_instance['success_redirect_url'];
            }
        } else {
            $instance['success_redirect_url'] = '';
        }

        return $instance;
    }

    /**
     * Generates the administration form for the widget.
     *
     * @param array instance The array of keys and values for the widget.
     */
    public function form($instance)
    {
        $instance = wp_parse_args((array)$instance);

        /**
         * Handle any options set in an older version of the plugin, where the
         * user has not yet updated them.
         */
        if (array_key_exists('include_name_fields', $instance)) {
            $instance['include_first_name_field'] =
                $instance['include_name_fields'];
            $instance['include_last_name_field'] =
                $instance['include_name_fields'];
        }

        // Display the admin form
        include EmailOctopus::get_plugin_dir('templates/admin_legacy.php');
    }

    /**
     * Registers and enqueues admin-specific styles.
     */
    public function register_admin_styles()
    {
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

        wp_enqueue_style(
            $this->get_widget_slug() . '-admin-styles',
            EmailOctopus::get_plugin_url('css/admin_legacy'.$is_minified.'.css'),
            array(),
            EMAILOCTOPUS_VERSION,
            'all'
        );
    }

    /**
     * Registers and enqueues admin-specific JavaScript.
     */
    public function register_admin_scripts()
    {
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

        wp_enqueue_script($this->get_widget_slug() . '-admin-script', EmailOctopus::get_plugin_url('js/admin_legacy'.$is_minified.'.js'), array('jquery'), EMAILOCTOPUS_VERSION, true);
        wp_localize_script(
            $this->get_widget_slug() . '-admin-script',
            $this->get_widget_slug() . '_message',
            array(
                'success' => __('Thanks for subscribing!', 'emailoctopus'),
                'missing_email_address_error' => __('Your email address is required.', 'emailoctopus'),
                'invalid_email_address_error' => __('Your email address looks incorrect, please try again.', 'emailoctopus'),
                'bot_submission_error' => __('This doesn\'t look like a human submission.', 'emailoctopus'),
                'invalid_parameters_error' => __('This form has missing or invalid fields.', 'emailoctopus'),
                'consent_required_error' => __('Please check the checkbox to indicate your consent.', 'emailoctopus'),
                'unknown_error' => __('Sorry, an unknown error has occurred. Please try again later.', 'emailoctopus'),
            )
        );
    }

    /**
     * Registers and enqueues widget-specific styles.
     */
    public function register_widget_styles()
    {
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

        wp_enqueue_style(
            $this->get_widget_slug() . '-admin-styles',
            EmailOctopus::get_plugin_url('css/widget_legacy'.$is_minified.'.css'),
            array(),
            EMAILOCTOPUS_VERSION,
            'all'
        );
    }

    /**
     * Registers and enqueues widget-specific scripts.
     */
    public function register_widget_scripts()
    {
        $handle = $this->get_widget_slug() . '-admin-script';
        wp_enqueue_script($handle, EmailOctopus::get_plugin_url('js/widget_legacy'.$is_minified.'.js'), array('jquery'), EMAILOCTOPUS_VERSION, true);
        wp_localize_script(
            $handle,
            $this->get_widget_slug() . '_message',
            array(
                'success' => __('Thanks for subscribing!', 'emailoctopus'),
                'missing_email_address_error' => __('Your email address is required.', 'emailoctopus'),
                'invalid_email_address_error' => __('Your email address looks incorrect, please try again.', 'emailoctopus'),
                'bot_submission_error' => __('This doesn\'t look like a human submission.', 'emailoctopus'),
                'invalid_parameters_error' => __('This form has missing or invalid fields.', 'emailoctopus'),
                'consent_required_error' => __('Please check the checkbox to indicate your consent.', 'emailoctopus'),
                'unknown_error' => __('Sorry, an unknown error has occurred. Please try again later.', 'emailoctopus'),
            )
        );
    }
}
