��    J      l  e   �      P     Q     Y  
   f  *   q     �     �     �     �     �     �     �  
             .     K  	   i     s     {     �     �  
   �     �     �     �     �     �     �     �             	     
   %     0  
   =     H     N     a  	   u          �     �  
   �     �     �     �     �     �     �  
   	     	     &	     .	  	   3	     =	     K	     Y	     h	     u	     }	  	   �	     �	     �	     �	  	   �	     �	  
   �	     �	     �	     �	     �	     
     $
  
   D
  �  O
            	   2  -   <     j     {     �     �     �     �     �     �                 	             $     2     H     V     d  	   p     z     �  %   �     �     �     �     �                    6     J  $   V     {     �     �     �     �     �     �     �     �  $   �  %     !   8     Z     h     w          �     �     �     �     �            	         *     1     9  	   H     R     g     x          �      �  )   �      �                 A                      6          @   E       I              #   "             3   /           ?             )   .   
       J   5   C                    1   &          	       4      :   $   0   ;      8         2             B   (   !             ,   '         >   F                      7   G   -       9       *      D      <          +   =      H   %           API Key Add new form Appearance Are you sure you want to delete this form? Available fields Background Color Background color Builder Button Color Button Text Color Button text color Class name Column header for formsID Column header for formsList Column header for formsTitle Connected Copied! Custom Custom class Custom colors Dark theme Default Delete Delete form Deleting... Display form description Display form title Edit Email address EmailOctopus Enter URL First name Form summary Form title Forms Inherit from theme Invalid email error Last name Light theme List List ID: Loading... Messages No title Not Connected Please enter an API key for %s Please select a form. Please select a list. Powered by Powered by  Preview Save Saving... Select a Form Select a list Select a theme Select color Sending Settings Shortcode Status Submit Submit button Subscribe Success message Text Color Theme Title: Unknown error Your API key is invalid. Your API key is now connected. Your email address is required. subscribed Project-Id-Version: EmailOctopus
Report-Msgid-Bugs-To: https://wordpress.org/plugins/emailoctopus/
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-11-14 17:53-0800
Language-Team: EmailOctopus <contact@emailoctopus.com>
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n > 1);
POT-Creation-Date: 
X-Generator: Poedit 2.2
Last-Translator: 
Language: fr
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Clé API Ajoutez nouveau formulaire Apparence Voulez-vous vraiment supprimer ce formulaire? Available fields Couleur d’arrière-plan Couleur de fond Constructeur Couleur du bouton Couleur du texte du bouton Couleur texte bouton Nom de classe ID Liste Titre Connecté Copié ! Personnalisé Classe personnalisée Custom colors Thème foncé Par défaut Supprimer Effacer le formulaire Supprimer ... Afficher la description du formulaire Afficher le titre du formulaire Modifier Adresse email EmailOctopus Entrer l’URL Prénom Résumé du formulaire Titre du formulaire Formulaires Hérité de la présentation du site Erreur Email non valide Nom Thème clair Liste ID de la liste : Chargement... Messages Pas de titre Non connecté Veuillez entrer une clé API pour %s Veuillez sélectionner un formulaire. Veuillez sélectionner une liste. Propulsé par Propulsé par  Aperçu Sauvegarder Enregistrement en cours... Sélectionner un formulaire Sélectionner une liste… Sélectionnez un thème Sélectionner la couleur Envoi Paramètres Shortcode Statut Envoyer Bouton Envoyer S'abonner Message de réussite Couleur du texte Thème Titre: Erreur inconnue Votre clé API n'est pas valide. Votre clé API est maintenant connectée. Votre adresse email est requise. abonné 