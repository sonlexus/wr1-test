<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}
?>
<div class="emailoctopus-forms-list wrap">
    <h1 class="wp-heading-inline">
        <img src="<?php echo EmailOctopus_Utils::get_svg();?>" alt="<?php esc_html_e('EmailOctopus logo', 'emailoctopus'); ?>" />&nbsp;
        <?php esc_html_e('EmailOctopus Forms', 'emailoctopus');?>
    </h1>
    <?php printf('<a href="%s" class="page-title-action">' . __('Add New', 'emailoctopus') . '</a>', add_query_arg(array('action' => 'edit', 'form_id' => 0), EmailOctopus::get_url()));?>

    <div class="emailoctopus-top-right">
        <?php
            $api_key = get_option('emailoctopus_api_key', false);
            $api_key_is_connected = EmailOctopus_Utils::is_valid_api_key($api_key);

            if ($api_key_is_connected) {
                ?>
                <div class="emailoctopus-api-connected is-connected">
                    <?php echo sprintf('<a href="%s">%s</a>', esc_url_raw(add_query_arg(array('page' => 'emailoctopus-api', ), admin_url('admin.php'))), esc_html__('API connected', 'emailoctopus')); ?>
                </div>
                <?php
            } else {
                ?>
                <div class="emailoctopus-api-connected is-not-connected">
                    <?php echo sprintf('<a href="%s">%s</a>', esc_url_raw(add_query_arg(array('page' => 'emailoctopus-api', ), admin_url('admin.php'))), esc_html__('API not connected', 'emailoctopus')); ?>
                </div>
                <?php
            }
        ?>
    </div>

    <?php
    if (isset($_GET['action']) && $_GET['action'] === 'delete') {
        $warning_message = get_option('emailoctopus_deleted_form', false);
        if ($warning_message === true) {
            ?>
            <div class="updated"><p><?php esc_html_e('Your forms have been deleted.', 'emailoctopus'); ?></p></div>
            <?php
            delete_option('emailoctopus_deleted_form');
        }
    }
    ?>

    <form id="emailoctopus-form-view" method="POST">
        <?php
        $forms_table = new EmailOctopus_List_Table_Forms();
        $forms_table->prepare_items();
        $forms_table->views();
        $forms_table->display();
        ?>
    </form>
</div>
