<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

/**
 * Admin class for EmailOctopus.
 *
 * @category EmailOctopus
 * @package  EmailOctopus
 * @author   EmailOctopus <contact@emailoctopus.com>
 * @license  GPL-2.0+
 * @link     https://emailoctopus.com
 *
 * @since 2.0.0
 */
class EmailOctopus_Admin
{
    public function __construct()
    {
        add_action(
            'admin_enqueue_scripts',
            array($this, 'add_admin_scripts')
        );
        add_action(
            'admin_menu',
            array($this,'add_admin_menu')
        );
        add_action(
            'wp_ajax_emailoctopus_check_api_key',
            array($this, 'ajax_check_api_key')
        );
        add_action(
            'wp_ajax_emailoctopus_get_form_options',
            array($this, 'ajax_get_form_options')
        );
        add_action(
            'wp_ajax_emailoctopus_save_form',
            array($this,'ajax_save_form_data')
        );
        add_action(
            'wp_ajax_emailoctopus_delete_form_ajax',
            array($this, 'ajax_delete_form')
        );
        add_action(
            'plugin_action_links_' . EmailOctopus::get_plugin_basename(),
            array($this, 'plugin_settings_link')
        );

        // Create tables
        new EmailOctopus_Tables();

        // Detect bulk actions and delete
        $this->check_delete();
    }

    /**
     * Checks for forms that need to be deleted
     *
     * @since  2.0.0
     * @access public
     */
    public function check_delete()
    {
        if (defined('DOING_AJAX') && DOING_AJAX) {
            return;
        }

        if (
            (
                isset($_POST['action']) &&
                $_POST['action'] === 'emailoctopus_delete_form'
            ) ||
            (
                isset($_POST['action2']) &&
                $_POST['action2'] === 'emailoctopus_delete_form'
            )
        ) {
            // Do a nonce check
            if (!wp_verify_nonce($_POST['_wpnonce'], 'bulk-emailoctopus_forms')) {
                wp_die('You do not have sufficient privileges to delete these forms.');
            }

            // Delete the forms
            $form_ids = implode(',', $_POST['form']);
            EmailOctopus_Utils::delete_forms($form_ids, $_POST['_wpnonce'], 'bulk-emailoctopus_forms');

            // Set option for delete warning
            update_option('emailoctopus_deleted_form', true);

            // Redirect
            $redirect = esc_url_raw(
                add_query_arg(
                    array('action' => 'delete'),
                    EmailOctopus::get_url()
                )
            );
            wp_redirect($redirect);
            exit();
        } elseif (
            isset($_GET['action']) &&
            $_GET['action'] === 'emailoctopus_delete_form'
        ) {
            // Do a nonce check
            if (!wp_verify_nonce($_GET['nonce'], 'delete_emailoctopus_form')) {
                wp_die('You do not have sufficient privileges to delete these forms.');
            }

            // Delete the form
            $form_ids = $_GET['form_ids'];
            EmailOctopus_Utils::delete_forms($form_ids, $_GET['nonce'], 'delete_emailoctopus_form');

            // Set option for delete warning
            update_option('emailoctopus_deleted_form', true);

            // Redirect
            $redirect = esc_url_raw(
                add_query_arg(
                    array('action' => 'delete'),
                    EmailOctopus::get_url()
                )
            );
            wp_redirect($redirect);
            exit();
        }
    }

    /**
     * Deletes a form
     *
     * @since  2.0.0
     * @access public
     */
    public function ajax_delete_form()
    {
        check_ajax_referer('emailoctopus_ajax_nonce', 'nonce');

        // Set option for delete warning
        update_option('emailoctopus_deleted_form', true);

        // Delete the form
        $form_ids = $_REQUEST['form_id'];
        EmailOctopus_Utils::delete_forms(
            $form_ids,
            $_REQUEST['nonce'],
            'emailoctopus_ajax_nonce'
        );

        wp_send_json(array(
            'form_deleted' => true,
        ));
    }

    /**
     * Saves the form data
     *
     * @since  2.0.0
     * @access public
     */
    public function ajax_save_form_data()
    {
        check_ajax_referer('emailoctopus_ajax_nonce', 'nonce');
        global $wpdb;
        $loop_form_data = array();
        $fields = array();
        $options = array();

        // Get the list from the API
        $list = EmailOctopus_Utils::get_list();
        $list_data = $list->data;

        // Get data from json object
        foreach ($_REQUEST['formData'] as $index => $form_data) {
            array_push($loop_form_data, json_decode(stripslashes($form_data)));
        }

        // Format into object/value pairs
        foreach ($loop_form_data as $index => $object) {
            if (preg_match('/options\[(.*)\]/', $object->name, $matches)) {
                $options[sanitize_text_field($matches[1])] =
                    sanitize_text_field($object->value);
            }
            if (preg_match('/field\[(.*)\]/', $object->name, $matches)) {
                $fields[] = sanitize_text_field($matches[1]);
            }
        }

        // Get current list
        $current_list_fields = array();
        foreach ($list_data as $data) {
            if ($data->id === $options['list']) {
                $current_list_fields = $data->fields;
                $options['list_name'] = $data->name;
                $options['list_id'] = $data->id;

                break;
            }
        }

        // Map fields
        $new_custom_fields = array();
        foreach ($fields as $index => $value) {
            foreach ($current_list_fields as $field) {
                if ($field->tag === $value) {
                    $new_custom_fields[] = array(
                        'tag' => $field->tag,
                        'type' => $field->type,
                        'label' => $field->label,
                    );
                }
            }
        }

        // Save or update the form version
        $form_id = absint($_REQUEST['form_id']);
        $table_forms = $wpdb->prefix . 'emailoctopus_forms';
        $table_meta = $wpdb->prefix . 'emailoctopus_forms_meta';
        $table_custom_fields = $wpdb->prefix . 'emailoctopus_custom_fields';
        if ($form_id === 0) {
            // We're creating a form
            $wpdb->insert(
                $table_forms,
                array(
                    'user_id' => $this->get_user_id(),
                    'title' => $options['form_title'],
                    'description' => $options['form_summary'],
                    'list' => $options['list_name'],
                    'list_id' => $options['list_id'],
                    'status' => 'published',
                    'date' => current_time('mysql'),
                ),
                array('%d', '%s', '%s', '%s', '%s', '%s', '%s')
            );
            $form_id = $wpdb->insert_id;
        } else {
            // We're updating a form

            // Clear form meta
            $meta_delete_query = $wpdb->prepare(
                "DELETE FROM $table_meta WHERE form_id = %d",
                $form_id
            );
            $wpdb->query($meta_delete_query);

            // Clear custom fields
            $custom_fields_delete_query = $wpdb->prepare(
                "DELETE FROM $table_custom_fields WHERE form_id = %d",
                $form_id
            );
            $wpdb->query($custom_fields_delete_query);

            // Update form table
            $wpdb->update(
                $table_forms,
                array(
                    'user_id' => $this->get_user_id(),
                    'title' => $options['form_title'],
                    'description' => $options['form_summary'],
                    'list' => $options['list_name'],
                    'list_id' => $options['list_id'],
                ),
                array('form_id' => $form_id),
                array('%d', '%s', '%s', '%s', '%s'),
                array('%d')
            );
        }

        // Add form meta
        foreach ($options as $key => $value) {
            if (in_array($key, ['list', 'form_title', 'form_summary'])) {
                continue;
            }

            $wpdb->insert(
                $table_meta,
                array(
                    'form_id' => $form_id,
                    'meta_key' => $key,
                    'meta_value' => $value,
                ),
                array('%d', '%s', '%s')
            );
        }

        // Add custom fields
        foreach ($new_custom_fields as $index => $value) {
            $wpdb->insert(
                $table_custom_fields,
                array(
                    'form_id' => $form_id,
                    'tag' => $value['tag'],
                    'type' => $value['type'],
                    'label' => $value['label'],
                    'order' => $index,
                ),
                array('%d', '%s', '%s', '%s', '%d')
            );
        }

        wp_send_json($this->ajax_get_form_options($form_id));

        exit;
    }

    /**
     * Checks the API and gets a list and form options
     *
     * @since  2.0.0
     * @access public
     */
    public function ajax_get_form_options($form_id = 0)
    {
        check_ajax_referer('emailoctopus_ajax_nonce', 'nonce');
        $list = EmailOctopus_Utils::get_list();

        $data = $this->get_form_options($form_id);
        $data['api'] = $list;

        wp_send_json($data);
    }

    /**
     * Get the options for a form
     *
     * @since  2.0.0
     * @access private
     *
     * @return array Form options
     */
    private function get_form_options($form_id = 0)
    {
        global $wpdb;
        $form_data_table = $wpdb->prefix . 'emailoctopus_forms';
        $form_meta_table = $wpdb->prefix . 'emailoctopus_forms_meta';
        $form_custom_fields_table = $wpdb->prefix . 'emailoctopus_custom_fields';

        // Perform form data query
        $query = $wpdb->prepare(
            "SELECT form_id, title, description, list_id FROM {$form_data_table} WHERE form_id = %d",
            $form_id
        );
        $results = $wpdb->get_results($query);
        if ($results) {
            $data['form_data'] = $results;
        } else {
            $data['form_data'] = array();
        }

        // Perform form meta query
        $query = $wpdb->prepare(
            "SELECT meta_key, meta_value FROM {$form_meta_table} WHERE form_id = %d",
            $form_id
        );
        $results = $wpdb->get_results($query);
        if ($results) {
            $data['form_meta'] = $results;
        } else {
            $data['form_meta'] = array();
        }

        // Perform custom fields query
        $query = $wpdb->prepare(
            "SELECT tag, type, label, `order` FROM {$form_custom_fields_table} WHERE form_id = %d ORDER BY `order` ASC",
            $form_id
        );
        $results = $wpdb->get_results($query);
        if ($results) {
            $data['custom_fields'] = $results;
        } else {
            $data['custom_fields'] = array();
        }

        $data['form_id'] = absint($form_id);

        return $data;
    }

    /**
     * Checks the API and gets a list and form options
     *
     * @since  2.0.0
     * @access public
     *
     * @return array Form data
     */
    public function get_localized_form_data($form_id = 0)
    {
        $list = EmailOctopus_Utils::get_list();

        // Get all data from form tables
        if (isset($_REQUEST['form_id'])) {
            $form_id = $form_id === 0 ? absint($_REQUEST['form_id']) : $form_id;
        }

        $data = $this->get_form_options($form_id);
        $data['api'] = $list;

        return $data;
    }

    /**
     * Checks the API for a valid key
     *
     * @since  2.0.0
     * @access public
     */
    public function ajax_check_api_key()
    {
        check_ajax_referer('emailoctopus_save_api_key');
        if (EmailOctopus_Utils::is_valid_api_key($_REQUEST['api_key'], true)) {
            $data = array(
                'errors' => false,
                'success' => true,
                'message' => __(
                    'Your API key is now connected.',
                    'emailoctopus'
                ),
            );
        } else {
            $data = array(
                'errors' => true,
                'success' => false,
                'message' => __(
                    'Your API key is invalid.',
                    'emailoctopus'
                ),
            );
        }

        wp_send_json($data);
    }

    /**
     * get_user_id - Get a user ID
     *
     * Get a logged in user's ID
     *
     * @access private
     * @since 1.5.0
     *
     * @return int user id
     */
    private function get_user_id()
    {
        $user_id = 0;
        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
        }

        return $user_id;
    }

    /**
     * Add admin menu and sub-menus
     *
     * @since  2.0.0
     * @access public
     */
    public function add_admin_menu()
    {
        // Add top level menu item
        add_menu_page(__('EmailOctopus', 'emailoctopus'), __('EmailOctopus', 'emailoctopus'), 'manage_options', 'emailoctopus', array($this, 'add_form_template'), 'none');

        add_submenu_page('emailoctopus', __('Forms', 'emailoctopus'), __('Forms', 'emailoctopus'), 'manage_options', 'emailoctopus', array($this, 'add_form_template'));

        add_submenu_page('emailoctopus', __('API Key', 'emailoctopus'), __('API Key', 'emailoctopus'), 'manage_options', 'emailoctopus-api', array($this, 'add_api_template'));
    }

    public function add_admin_scripts()
    {
        // Ensure scripts are loaded just on emailoctopus pages
        if (
            isset($_GET['page']) &&
            $_GET['page'] !== 'emailoctopus' &&
            $_GET['page'] !== 'emailoctopus-api'
        ) {
            return;
        }

        // Ensure script debug allows non-minified scripts
        $is_minified = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

        wp_enqueue_script('alpha-color-picker', EmailOctopus::get_plugin_url('js/alpha-color-picker'.$is_minified.'.js'), array('jquery', 'wp-color-picker'), EMAILOCTOPUS_VERSION, true);

        wp_enqueue_style('alpha-color-picker', EmailOctopus::get_plugin_url('css/alpha-color-picker'.$is_minified.'.css'), array('wp-color-picker'), EMAILOCTOPUS_VERSION, 'all');
        wp_enqueue_script('emailoctopus_admin', EmailOctopus::get_plugin_url('js/admin'.$is_minified.'.js'), array('jquery', 'wp-ajax-response', 'alpha-color-picker'), EMAILOCTOPUS_VERSION, true);
        wp_enqueue_script('emailoctopus_react', EmailOctopus::get_plugin_url('js/main'.$is_minified.'.js'), array('jquery', 'wp-ajax-response', 'alpha-color-picker'), EMAILOCTOPUS_VERSION, true);
        $react_i18n = array(
            'form_title' => __('Form title', 'emailoctopus'),
            'form_summary' => __('Form summary', 'emailoctopus'),
            'select_list' => __('List', 'emailoctopus'),
            'select_list_heading' =>  __('Select a list', 'emailoctopus'),
            'select_list_reminder' => __('Please select a list.', 'emailoctopus'),
            'tab_builder' => __('Builder', 'emailoctopus'),
            'tab_settings' => __('Settings', 'emailoctopus'),
            'tab_appearance' => __('Appearance', 'emailoctopus'),
            'tab_messages' => __('Messages', 'emailoctopus'),
            'available_fields' => __('Available fields', 'emailoctopus'),
            'preview' => __('Preview', 'emailoctopus'),
            'submit' => __('Submit', 'emailoctopus'),
            'subscribe' => __('Subscribe', 'emailoctopus'),
            'email_address' => __('Email address', 'emailoctopus'),
            'empty_custom_fields' => __('It looks like you\'ve already added all your fields to your form', 'emailoctopus'),
            'double_opt_in_label' => __('Double opt-in enabled', 'emailoctopus'),
            'double_opt_in_description' => __('Double opt-in must be enabled in your account for this to work', 'emailoctopus'),
            'redirect' => __('Instead of thanking the user for subscribing, redirect them to a URL', 'emailoctopus'),
            'redirect_placeholder' => __('Enter URL', 'emailoctopus'),
            'consent_checkbox' => __('Include a consent checkbox', 'emailoctopus'),
            'consent_checkbox_default' => __('I consent to receiving your weekly newsletter and special offers via email', 'emailoctopus'),
            'branding' => __('Show EmailOctopus branding', 'emailoctopus'),
            'branding_url' => 'https://emailoctopus.com',
            'branding_name' => 'EmailOctopus',
            'branding_text' => __('Powered by', 'emailoctopus'),
            'theme_select' => __('Select a theme', 'emailoctopus'),
            'theme_select_inherit' => __('Inherit from theme', 'emailoctopus'),
            'theme_select_light' => __('Light theme', 'emailoctopus'),
            'theme_select_dark' => __('Dark theme', 'emailoctopus'),
            'theme_select_custom' => __('Custom colors', 'emailoctopus'),
            'class_label' => __('Add a class to your form', 'emailoctopus'),
            'class_placeholder' => __('Class name', 'emailoctopus'),
            'theme_custom_background' => __('Background color', 'emailoctopus'),
            'theme_custom_text' => __('Text color', 'emailoctopus'),
            'theme_custom_button' => __('Button color', 'emailoctopus'),
            'theme_custom_button_text' => __('Button text color', 'emailoctopus'),
            'messages_header_submit' => __('Submit button', 'emailoctopus'),
            'messages_header_success' => __('Success message', 'emailoctopus'),
            'messages_header_missing_email' => __('Missing email error', 'emailoctopus'),
            'messages_header_invalid_email' => __('Invalid email error', 'emailoctopus'),
            'messages_header_bot_invalid' => __('Bot submission error', 'emailoctopus'),
            'messsages_header_consent' => __('Missing consent error', 'emailoctopus'),
            'messsages_header_unknown' => __('Unknown error', 'emailoctopus'),
            'messages_submit' => __('Subscribe', 'emailoctopus'),
            'messages_success' => __('Thanks for subscribing!', 'emailoctopus'),
            'messages_missing_email' => __('Your email address is required.', 'emailoctopus'),
            'messages_email_invalid' => __('Your email address looks incorrect. Please try again.', 'emailoctopus'),
            'messages_bot_invalid' => __('This doesn\'t look like a human submission.', 'emailoctopus'),
            'messages_consent' => __('Please check the checkbox to indicate your consent.', 'emailoctopus'),
            'messages_unknown' => __('Sorry, an unknown error has occurred. Please try again later.', 'emailoctopus'),
            'title_required' => __('Please enter a title before saving.', 'emailoctopus'),
            'save' => __('Save', 'emailoctopus'),
            'saving' => __('Saving...', 'emailoctopus'),
            'save_form' => __('Save form', 'emailoctopus'),
            'delete_form' => __('Delete form', 'emailoctopus'),
            'delete_form_active' => __('Deleting...', 'emailoctopus'),
            'shortcode_label' => __('Shortcode', 'emailoctopus'),
            'no_form_id' => __('This form ID does not exist.', 'emailoctopus'),
            'no_form_create' => __('Create new form.', 'emailoctopus'),
            'no_form_url' => esc_url_raw(add_query_arg(array('action' => 'edit', 'form_id' => 0), EmailOctopus::get_url())),
            'new_form_url' => esc_url_raw(add_query_arg(array('action' => 'edit'), EmailOctopus::get_url())),
            'deleted_form_url' => esc_url_raw(add_query_arg(array('action' => 'delete'), EmailOctopus::get_url())),
            'add_new_form' => __('Add New', 'emailoctopus'),
            'add_new_form_url' => esc_url_raw(add_query_arg(array('action' => 'edit', 'form_id' => 0), EmailOctopus::get_url())),
            'copied' => __('Copied!', 'emailoctopus'),
            'double_opt_in_enabled_front' => __('Double opt-in is enabled on this list. Go to', 'emailoctopus'),
            'double_opt_in_disabled_front' => __('Double opt-in is disabled on this list. Go to', 'emailoctopus'),
            'double_opt_in_anchor_text' => __('your dashboard', 'emailoctopus'),
            'double_opt_in_changes' => __('to make changes.', 'emailoctopus'),
            'select_color' => __('Select color', 'emailoctopus'),
            'clear_list_warning' => __('WARNING: This will clear your form field selection.', 'emailoctopus'),
            'no_list_warning' => __('You have not created any lists yet. Please create a list and refresh.', 'emailoctopus'),
            'theme' => __('Theme', 'emailoctopus'),
            'custom_class' => __('Custom class', 'emailoctopus'),
            'subscriber_singular' => __('subscribed', 'emailoctopus'),
            'subscriber_plural' => __('subscribed', 'emailoctopus'),
        );

        $form_id = (isset($_GET['form_id'])) ? absint($_GET['form_id']) : 0;
        $form_data = $this->get_localized_form_data($form_id);

        wp_localize_script('emailoctopus_admin', 'emailoctopus', array(
            'api_check_saving' => __('Saving...', 'emailoctopus'),
            'api_check_save' => __('Save Changes', 'emailoctopus'),
            'api_check' => __('Checking Your API Key', 'emailoctopus'),
            'api_success' => __(
                    'Your API key has been saved successfully',
                    'emailoctopus'
                ),
            'api_failure' => __('Your API key is not valid', 'emailoctopus'),
            'api_blank' => __('Your API key cannot be blank', 'emailoctopus'),
            'api_connected' => __('Connected', 'emailoctopus'),
            'api_not_connected' => __('Not Connected', 'emailoctopus'),
            'ajax_nonce' => wp_create_nonce('emailoctopus_ajax_nonce'),
            'svg' => EmailOctopus_Utils::get_svg('large'),
            'svg_small' => EmailOctopus_Utils::get_svg('icon'),
            'svg_alt' => __('EmailOctopus logo', 'emailoctopus'),
            'form_editor' => __('EmailOctopus Form Editor', 'emailoctopus'),
            'react_i18n' => $react_i18n,
            'form_data' => $form_data,
            'delete_confirmation' => __(
                    'Are you sure you want to delete this form?',
                    'emailoctopus'
                ),
        ));

        wp_enqueue_style(
            'emailoctopus_admin_styles',
            EmailOctopus::get_plugin_url('css/admin' . $is_minified . '.css'),
            array(),
            EMAILOCTOPUS_VERSION
        );
    }

    /**
     * Adds plugin settings page link to plugin links in WordPress Dashboard Plugins Page
     *
     * @since 2.0.0
     * @access public
     * @see __construct
     * @param array $settings Uses $prefix . "plugin_action_links_$plugin_file" action
     * @return array Array of settings
     */
    public function plugin_settings_link($settings)
    {
        $admin_anchor = sprintf(
            '<a href="%s">%s</a>',
            esc_url(EmailOctopus::get_url()),
            esc_html__('Settings', 'emailoctopus')
        );

        if (!is_array($settings)) {
            return array($admin_anchor);
        } else {
            return array_merge(array($admin_anchor), $settings);
        }
    }

    public function add_form_template()
    {
        // Get API key
        $api_key = get_option('emailoctopus_api_key', false);
        $is_valid_key = EmailOctopus_Utils::is_valid_api_key($api_key);

        if (!$is_valid_key) {
            include_once(EmailOctopus::get_plugin_dir('/templates/api-key.php'));
        } elseif (isset($_GET['action']) && $_GET['action'] === 'edit') {
            include_once(
                EmailOctopus::get_plugin_dir('/templates/form-edit.php')
            );
        } else {
            if (
                isset($_GET['action']) &&
                isset($_GET['form_ids']) &&
                $_GET['action'] === 'delete'
            ) {
                $form_ids = urldecode($_GET['form_ids']);
                EmailOctopus_Utils::delete_forms($form_ids);
            }

            include_once(
                EmailOctopus::get_plugin_dir('/templates/forms-list.php')
            );
        }
    }

    public function add_api_template()
    {
        include_once(EmailOctopus::get_plugin_dir('/templates/api-key.php'));

        return;
    }
}
