<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

/**
 * EmailOctopus Forms List Table.
 *
 * @package WordPress
 * @subpackage EmailOctopus_List_Table
 * @since 2.0.0
 * @access private
 */
class EmailOctopus_List_Table_Forms extends EmailOctopus_List_Table
{
    private $order = 'DESC';
    private $page = '1';

    /**
     * Constructor.
     *
     * @since 3.1.0
     * @access public
     *
     * @see WP_List_Table::__construct() for more information on default arguments.
     *
     * @global object $post_type_object
     * @global wpdb   $wpdb
     *
     * @param array $args An associative array of arguments.
     */
    public function __construct($args = array())
    {
        parent::__construct(array(
            'singular'=> 'emailoctopus_form',
            'plural' => 'emailoctopus_forms',
            'screen' => isset($args['screen']) ? $args['screen'] : null,
            'ajax' => true
        ));

        $this->page = isset($args['paged']) ? $args['paged'] : '1';
        $this->order = isset($args['order']) ? $args['order'] : 'DESC';
    }

    /**
     * Prepare items
     *
     * @return void
     */
    public function prepare_items()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'emailoctopus_forms';
        $where = '';
        $select = "SELECT form_id, title, description, list FROM $table";
        $order_by = ' ORDER BY ' . sanitize_sql_orderby("form_id DESC");
        $limit = " LIMIT %d,%d";
        $per_page = 100;
        $offset = ($this->page - 1) * $per_page;

        // Calculate no. of logs separately, because filters may be on
        $query = $select . $where . $order_by;
        $wpdb->get_results($query);

        $form_count = $wpdb->num_rows;

        $query = $select . $where . $order_by . $limit;
        $query = $wpdb->prepare($query, $offset, $per_page);
        $this->items = $wpdb->get_results($query, ARRAY_A);

        // Register the columns
        $this->_column_headers = array(
            $this->get_columns(), // Columns
            array(), // Hidden
            $this->get_sortable_columns(), // Sortable
        );

        $this->set_pagination_args(array(
            'total_items' => $form_count,
            'per_page' => 100,
            'total_pages' => ceil($form_count / $per_page),
            'paged' => isset($this->page) ? $this->page : '1',
        ));
    }

    /**
     * Generate the table navigation above or below the table
     *
     * @since 8.0.1
     * @access protected
     * @param string $which Which extra table nav to use
     */
    protected function display_tablenav($which)
    {
        if ($which === 'top') {
            wp_nonce_field('bulk-' . $this->_args['plural']);
        } ?>
    <div class="tablenav <?php echo esc_attr($which); ?>">
        <div class="alignleft actions bulkactions">
            <?php $this->bulk_actions($which); ?>
        </div>
        <?php
        $this->extra_tablenav($which);
        $this->pagination($which); ?>
    </div>
<?php
    }

    /**
     * Get table classes
     *
     * @return array
     */
    protected function get_table_classes()
    {
        return array('widefat', 'fixed', 'striped');
    }

    /**
     * Get columns
     *
     * @return array
     */
    public function get_columns()
    {
        $columns = array(
            'cb'                  =>  '<input type="checkbox />',
            'form_name'           => _x('Title', 'Column header for forms', 'emailoctopus'),
            'form_description'    => _x('Description', 'Column header for forms', 'emailoctopus'),
            'list'                => _x('List', 'Column header for forms', 'emailoctopus'),
            'form_id'             => _x('ID', 'Column header for forms', 'emailoctopus'),
            'shortcode'           => _x('Shortcode', 'Column header for forms', 'emailoctopus'),
        );

        return $columns;
    }

    /**
     * No items
     *
     * @return void
     */
    public function no_items()
    {
        echo __('You appear to have no forms.', 'emailoctopus');
        echo '&nbsp;';
        echo sprintf('<a href="%s">' . __('Let\'s create one.', 'emailoctopus') . '</a>', add_query_arg(array('action' => 'edit', 'form_id' => 0), EmailOctopus::get_url()));
    }

    /**
     * Get bulk actions
     *
     * @return array
     */
    protected function get_bulk_actions()
    {
        $actions = array();

        $actions[ 'emailoctopus_delete_form' ] = esc_html__('Delete', 'emailoctopus');

        return $actions;
    }

    /**
     * Display rows
     *
     * @global WP_Query $wp_query
     * @global int $per_page
     * @param array $posts posts to be displayed
     * @param int   $level Row level
     */
    public function display_rows($posts = array(), $level = 0)
    {
        foreach ($this->items as $record) {
            $this->single_row($record);
        }
    }

    /**
     * Single row
     *
     * @param array $record log record
     * @return void
     */
    public function single_row($record)
    {
        ?>
        <tr id="form-<?php echo esc_attr($record['form_id']); ?>">
            <?php
            $columns = $this->get_columns();
        foreach ($columns as $column_name => $description) {
            switch ($column_name) {
                    case 'cb':
                        echo sprintf('<th scope="row" class="check-column"><input type="checkbox" name="form[]" value="%d" /></th>', absint($record['form_id']));

                        break;
                    case 'form_id':
                        echo '<td>' . absint($record['form_id']) . '</td>';

                        break;
                    case 'form_name':
                        $edit_url = add_query_arg(array('action' => 'edit', 'form_id' => absint($record['form_id'])), EmailOctopus::get_url());
                        $delete_url = add_query_arg(array('action' => 'emailoctopus_delete_form', 'form_ids' => implode(',', array(absint($record['form_id']))), 'nonce' => wp_create_nonce('delete_emailoctopus_form')), EmailOctopus::get_url());
                        echo '<td>';
                        if (empty($record['title'])) {
                            echo sprintf('<a href="%s">' . esc_html('(no title)', 'emailoctopus') . '</a>', esc_url($edit_url));
                        } else {
                            echo sprintf('<a href="%s">' . esc_html($record['title']) . '</a>', esc_url($edit_url));
                        }
                        echo '<div class="row-actions">';
                        echo sprintf('<span class="edit"><a href="%s">%s</a></span> | ', esc_url($edit_url), esc_html__('Edit', 'emailoctopus'));
                        echo sprintf('<span class="delete"><a href="%s">%s</a></span>', esc_url($delete_url), esc_html__('Delete', 'emailoctopus'));
                        echo '</div>';
                        echo '</td>';

                        break;
                    case 'form_description':
                        echo '<td>' . esc_html($record['description']) . '</td>';

                        break;
                    case 'list':
                        echo '<td>' . esc_html($record['list']) . '</td>';
                        break;
                    case 'shortcode':
                        echo sprintf('<td>[emailoctopus form_id=%u]</td>', absint($record['form_id']));
                    break;
                }
        } ?>
        </tr>
    <?php
    }

    /**
     * Display the pagination.
     *
     * @since 3.1.0
     * @access protected
     *
     * @param string $which Which extra table nav to use
     */
    protected function pagination($which)
    {
        if (empty($this->_pagination_args)) {
            return;
        }

        $total_items = $this->_pagination_args['total_items'];
        $total_pages = $this->_pagination_args['total_pages'];
        $current = $this->_pagination_args['paged'];

        $infinite_scroll = false;
        if (isset($this->_pagination_args['infinite_scroll'])) {
            $infinite_scroll = $this->_pagination_args['infinite_scroll'];
        }

        $output = '<span class="displaying-num">' . sprintf(_n('%s item', '%s items', $total_items), number_format_i18n($total_items)) . '</span>';

        $current_url = set_url_scheme('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

        $current_url = remove_query_arg(
            array('hotkeys_highlight_last', 'hotkeys_highlight_first'),
            $current_url
        );

        $page_links = array();

        $total_pages_before = '<span class="paging-input">';
        $total_pages_after  = '</span>';

        $disable_first = $disable_last = $disable_prev = $disable_next = false;

        if ($current === 1) {
            $disable_first = true;
            $disable_prev = true;
        }
        if ($current === 2) {
            $disable_first = true;
        }
        if ($current === $total_pages || $total_items === 0) {
            $disable_last = true;
            $disable_next = true;
        }
        if ($current === $total_pages - 1) {
            $disable_last = true;
        }

        if ($disable_first) {
            $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='first-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url(add_query_arg(array('tab' => $tab, 'view' => $view, 'term' => $term,  'paged' => 1), $current_url)),
                __('First page'),
                '&laquo;'
            );
        }

        if ($disable_prev) {
            $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='prev-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url(add_query_arg(array('paged' => max(1, $current-1), 'tab' => $tab, 'view' => $view, 'term' => $term), $current_url)),
                __('Previous page'),
                '&lsaquo;'
            );
        }

        if ($which === 'bottom') {
            $html_current_page  = $current;
            $total_pages_before = '<span class="screen-reader-text">' . __('Current Page') . '</span><span id="table-paging" class="paging-input">';
        } else {
            $html_current_page = sprintf(
                "%s<input class='current-page' id='current-page-selector' type='text' name='paged' value='%s' size='%d' aria-describedby='table-paging' />",
                '<label for="current-page-selector" class="screen-reader-text">' . __('Current Page') . '</label>',
                $current,
                strlen($total_pages)
            );
        }
        $html_total_pages = sprintf("<span class='total-pages'>%s</span>", number_format_i18n($total_pages));
        if ($total_items === 0) {
            $html_current_page = 0;
            $html_total_pages = 0;
        }
        $page_links[] = $total_pages_before . sprintf(_x('%1$s of %2$s', 'paging'), $html_current_page, $html_total_pages) . $total_pages_after;

        if ($disable_next) {
            $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='next-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url(add_query_arg(array('paged' => min($total_pages, $current+1), 'tab' => $tab, 'view' => $view, 'term' => $term), $current_url)),
                __('Next page'),
                '&rsaquo;'
            );
        }

        if ($disable_last) {
            $page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='last-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url(add_query_arg(array('paged' => $total_pages, 'tab' => $tab, 'view' => $view, 'term' => $term), $current_url)),
                __('Last page'),
                '&raquo;'
            );
        }

        $pagination_links_class = 'pagination-links';
        if (!empty($infinite_scroll)) {
            $pagination_links_class = ' hide-if-js';
        }
        $output .= "\n<span class='$pagination_links_class'>" . join("\n", $page_links) . '</span>';

        if ($total_pages) {
            $page_class = $total_pages < 2 ? ' one-page' : '';
        } else {
            $page_class = ' no-pages';
        }
        $this->_pagination = "<div class='tablenav-pages{$page_class}'>$output</div>";

        echo $this->_pagination;
    }
}
