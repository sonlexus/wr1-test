'use strict';

(function ($) {
    'use strict';

    function toggleFieldsInWidget(widget) {
        var $includeConsent = widget.find('.include-consent').first();
        var $consentMessage = widget.find('.consent-message').first();
        $consentMessage.toggle($includeConsent.is(':checked'));

        var $redirectOnSuccess = widget.find('.redirect-on-success').first();
        var $successRedirectUrl = widget.find('.success-redirect-url').first();
        $successRedirectUrl.toggle($redirectOnSuccess.is(':checked'));
    }
    // Hide errors when user enters text
    $('#emailoctopus-api-key').on('input', function (e) {
        $('.emailoctopus-status-notice').removeClass('emailoctopus-fadein emailoctopus-error').addClass('emailoctopus-fadeout').find('strong').html();
        $('#emailoctopus-api-key').removeClass('emailoctopus-input-error');
    });

    // Form submission, perform sanity checks and submit api key via Ajax
    $('#api_key_form').on('submit', function (e) {
        e.preventDefault();
        var $api_key = $.trim($('#emailoctopus-api-key').val());
        if ($api_key === '') {
            $('.emailoctopus-status-notice').removeClass('emailoctopus-fadeout').addClass('emailoctopus-fadein emailoctopus-error').find('strong').html(emailoctopus.api_blank);
            $('#emailoctopus-api-key').addClass('emailoctopus-input-error');
            return;
        }

        $('#emailoctopus-api-key-submit').prop('disabled', 'disabled').val(emailoctopus.api_check_saving);
        $.ajax(ajaxurl, {
            type: 'POST',
            data: {
                action: 'emailoctopus_check_api_key',
                _ajax_nonce: $('#_ajax_nonce').val(),
                api_key: $api_key
            }
        }).done(function (data) {
            $('#emailoctopus-api-key-submit').prop('disabled', false).val(emailoctopus.api_check_save);
            if (true == data.errors) {
                $('#emailoctopus-api-key').addClass('emailoctopus-input-error');
                $('.emailoctopus-status-notice').removeClass('emailoctopus-fadeout emailoctopus-success').addClass('emailoctopus-fadein emailoctopus-error').find('strong').html(data.message);
                $('.emailoctopus-api-connected').removeClass('is-connected').addClass('is-not-connected').html(emailoctopus.api_not_connected);
            } else {
                $('.emailoctopus-status-notice').removeClass('emailoctopus-fadeout emailoctopus-error').addClass('emailoctopus-fadein emailoctopus-success').find('strong').html(data.message);
                $('.emailoctopus-api-connected').removeClass('is-not-connected').addClass('is-connected').html(emailoctopus.api_connected);

                var url_params = wpAjax.unserialize(window.location.href);
                if (url_params.page === 'emailoctopus') {
                    window.location.href = window.location.href;
                }
            }
        });
    });

    $(document).on('click', '.emailoctopus-notice .notice-dismiss', function () {
        $.ajax(ajaxurl, {
            type: 'POST',
            data: {
                action: 'emailoctopus_dismissed_notice'
            }
        });
    });

    $(document).on('change', '.emailoctopus-widget-options .include-consent', function () {
        var $widget = $(this).closest('.emailoctopus-widget-options');
        toggleFieldsInWidget($widget);
    });

    $(document).on('change', '.emailoctopus-widget-options .redirect-on-success', function () {
        var $widget = $(this).closest('.emailoctopus-widget-options');
        toggleFieldsInWidget($widget);
    });

    $(document).ready(function () {
        $('.emailoctopus-widget-options').each(function () {
            toggleFieldsInWidget($(this));
        });
    });

    $(document).on('widget-updated', function (e, widget) {
        $('.emailoctopus-widget-options').each(function () {
            toggleFieldsInWidget($(this));
        });
    });
})(jQuery);