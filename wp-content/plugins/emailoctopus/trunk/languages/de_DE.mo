��    C      4  Y   L      �     �  
   �  *   �     �                     '  
   9  #   D     h     �     �  	   �     �     �     �     �  
   �     �                    1     D     I     W  	   p  
   z  
   �     �     �  	   �     �     �     �  
   �     �     �     �     �  
             %     -     2  	   ?  	   I     S     a     o     }     �     �  	   �     �     �     �  	   �     �  
   �     �     �     �     	  
   !	  �  ,	     �     �  :        ?     P     X     j     w     �     �     �     �     �  	   �     �     �     �     �                    (     :     X  
   o     z     �     �     �     �  	   �     �     �     �        
               
   %     0     @  
   Z     e     q  	   z     �     �     �     �     �     �     �            	   #     -     4     ;  
   L     W  	   f     p     v     }     �  
   �     =               *      9      	               <      .   >   '                     !   @   ;       8           A         5   $   +   4   )      7          0   C              6   B                 (   %                  1   ?   ,                       &   -   "          #                     
   3         2   :      /       API Key Appearance Are you sure you want to delete this form? Background color Builder Button Text Color Button color Button text color Class name Column header for formsDescription Column header for formsID Column header for formsList Column header for formsTitle Connected Copied! Custom Custom class Custom colors Dark theme Default Delete Deleting... Display form description Display form title Edit Email address Email address (required) Enter URL First name Form title Forms Inherit from theme Last name Light theme List List ID: Loading... Messages No title Not Connected Please select a list. Powered by Powered by  Preview Save Save Changes Save form Saving... Select a Form Select a form Select a list Select color Sending Settings Shortcode Status Submit Submit button Subscribe Success message Text color Theme Title: Unknown error Your email address is required. subscribed Project-Id-Version: EmailOctopus
Report-Msgid-Bugs-To: https://wordpress.org/plugins/emailoctopus/
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-11-14 17:54-0800
Language-Team: EmailOctopus <contact@emailoctopus.com>
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
POT-Creation-Date: 
X-Generator: Poedit 2.2
Last-Translator: 
Language: de
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 API-Schlüssel Aussehen Sind Sie sicher, dass Sie dieses Formular löschen wollen? Hintergrundfarbe Builder Button Text Farbe Button Farbe Schaltfläche Textfarbe Klassenname Beschreibung ID Liste Titel Verbunden Kopiert! Benutzerdefiniert Eigene CSS-Klasse Benutzerdefinierte Farben Dunkles Thema Standard Löschen Wird gelöscht... Formularbeschreibung anzeigen Formulartitel anzeigen Bearbeiten E-Mail-Adresse E-Mail-Adresse (erforderlich) URL eingeben Vorname Formulartitel Formulare Übernehmen vom Theme Nachname Helles Theme Liste Listen ID: Lade... Nachrichten Kein Titel Nicht verbunden Bitte eine Liste wählen. Powered by Powered by  Vorschau Speichern Änderungen speichern Formular speichern Speichern... Wähle ein Formular Wählen Sie ein Formular Wähle eine Liste aus Farbe auswählen Senden Einstellungen Shortcode Status Senden ﻿Senden Button Abonnieren Erfolgsmeldung Textfarbe Theme Titel: Unbekannter Fehler E-Mailadresse erforderlich Abonnement 