<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}
?>
<div class="emailoctopus-api wrap">
    <h1 class="wp-heading-inline">
        <img src="<?php echo EmailOctopus_Utils::get_svg();?>" alt="<?php esc_html_e('EmailOctopus logo', 'emailoctopus'); ?>" />&nbsp;
        <?php esc_html_e('EmailOctopus API Key', 'emailoctopus');?>
    </h1>
</div>
<div class="emailoctopus-status-notice"><p><strong></strong></p></div>
<form id="api_key_form" method="POST" action="#">
<?php wp_nonce_field('emailoctopus_save_api_key', '_ajax_nonce'); ?>
<table class="form-table">
    <tbody>
        <tr>
            <th scope="row"><?php esc_html_e('Status', 'emailoctopus'); ?></th>
            <td>
                <?php
                $api_key = get_option('emailoctopus_api_key', false);
                $api_key_status = EmailOctopus_Utils::is_valid_api_key($api_key);
                if ($api_key_status) {
                    ?>
                    <div class="emailoctopus-api-connected is-connected">
                        <?php esc_html_e('Connected', 'emailoctopus'); ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="emailoctopus-api-connected is-not-connected">
                        <?php esc_html_e('Not Connected', 'emailoctopus'); ?>
                    </div>
                    <?php
                }
                ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?php esc_html_e('API key', 'emailoctopus'); ?></th>
            <td>
                <input class="regular-text <?php echo ($api_key && !$api_key_status) ? 'emailoctopus-input-error' : ''; ?>" type="text" value="<?php echo $api_key ? esc_attr($api_key) : ''; ?>" name="emailoctopus-api-key" id="emailoctopus-api-key"/>
                <p>
                    <?php echo sprintf(__('Your %sEmailOctopus API key%s, used to connect to your account.', 'emailoctopus'), '<a href="https://emailoctopus.com/api-documentation/" target="_blank" rel="noopener">', '</a>'); ?>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<?php submit_button(__('Save Changes', 'emailoctopus'), 'primary', 'emailoctopus-api-key-submit'); ?>
</form>
