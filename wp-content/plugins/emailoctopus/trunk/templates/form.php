<?php
if (!defined('ABSPATH')) {
    die('No direct access.');
}

/**
 * Filter: emailoctopus_class_names
 *
 * Add more class names.
 *
 * @since 2.0.0
 *
 * @param array $main_class_names Array of class names
 * @param int   $form_id          The form ID
 */
$main_class_names = apply_filters('emailoctopus_class_names', $main_class_names, $form_id);
?>
<div class="<?php echo esc_attr(implode(' ', $main_class_names));?>" <?php echo $appearance === 'custom' ? sprintf('style="%s"', implode(' ', $main_class_styles)) : '';?>>
<form method="post" action="https://emailoctopus.com/lists/<?php echo esc_attr($list_id); ?>/members/external-add" class="emailoctopus-form">
    <div class="emailoctopus-form-textarea-hidden" aria-hidden="true">
        <textarea class="emailoctopus-form-textarea-hidden" name="message_consent_required" aria-hidden="true"><?php echo esc_textarea($message_consent_required); ?></textarea>
        <textarea class="emailoctopus-form-textarea-hidden" name="message_missing_email" aria-hidden="true"><?php echo esc_textarea($message_missing_email); ?></textarea>
        <textarea class="emailoctopus-form-textarea-hidden" name="message_invalid_email" aria-hidden="true"><?php echo esc_textarea($message_invalid_email); ?></textarea>
        <textarea class="emailoctopus-form-textarea-hidden" name="message_bot" aria-hidden="true"><?php echo esc_textarea($message_bot); ?></textarea>
        <textarea class="emailoctopus-form-textarea-hidden" name="message_success" aria-hidden="true"><?php echo esc_textarea($message_success); ?></textarea>
    </div>
    <?php
    if (isset($show_widget_title) && !empty($show_widget_title)) {
        printf('<h2 class="emailoctopus-heading">%s</h2>', esc_html($show_widget_title));
    }
    if (isset($show_title) && $show_title === 'true' && !empty($form_data['title'])) {
        printf('<h2 class="emailoctopus-heading">%s</h2>', esc_html($form_data['title']));
    } else {
        if (!empty($form_data['title']) && isset($show_title) && $show_title !== 'false') {
            printf('<h2 class="emailoctopus-heading">%s</h2>', esc_html($form_data['title']));
        }
    }
    if (isset($show_description) && $show_description === 'true' && !empty($form_data['description'])) {
        printf('<p>%s</p>', esc_html($form_data['description']));
    } elseif (isset($show_description) && $show_description && !empty($form_data['description'])) {
        printf('<p>%s</p>', esc_html($form_data['description']));
    }
    ?>
    <p class="emailoctopus-success-message"></p>
    <p class="emailoctopus-error-message"></p>
        <div class="emailoctopus-form-copy-wrapper">
            <input type="hidden" name="emailoctopus_form_id" value="<?php echo absint($form_id); ?>" />
            <input type="hidden" name="emailoctopus_list_id" value="<?php echo esc_attr($list_id); ?>" />
            <?php
            foreach ($custom_fields as $custom_field) {
                echo '<div class="emailoctopus-form-row">';
                echo sprintf('<label><span class="emailoctopus-label">%s %s</span><br /><input type="%s" placeholder="%s" name="%s" class="emailoctopus-custom-fields" tabindex="%d" /></label>', esc_html($custom_field['label']), $custom_field['tag'] === 'EmailAddress' ? '<span class="required">*</span>' : '', esc_attr(strtolower($custom_field['type'])), esc_attr($custom_field['label']), esc_attr($custom_field['tag']), absint($tabindex));
                echo '</div>';
                $tabindex += 1;
            }
            ?>
            <?php
            if ($consent) {
                ?>
                <div class="emailoctopus-form-row">
                        <label>
                            <input type="checkbox" name="consent" class="emailoctopus-consent" tabindex="<?php echo absint($tabindex); ?>" />&nbsp;<?php echo esc_html($consent_content); ?>
                        </label>
                    </div>
                <?php
                $tabindex += 1;
            }
            ?>
            <div class="emailoctopus-form-row-hp" aria-hidden="true">
                <!-- Do not remove this field, otherwise you risk bot signups -->
                <input type="text" name="hp<?php echo esc_attr($list_id); ?>" tabindex="-1" autocomplete="nope">
            </div>
            <div class="emailoctopus-form-row-subscribe">
                <?php
                if ($redirect) {
                    echo sprintf('<input type="hidden" name="successRedirectUrl" class="emailoctopus-success-redirect-url" value="%s" />', esc_attr($redirect_url));
                }
                ?>
                <button type="submit" tabindex="<?php echo absint($tabindex); ?>" style="<?php echo $appearance === 'custom' ? sprintf('background: %s; color: %s;', esc_attr($button_color), esc_attr($button_text_color)) : ''; ?>"><?php echo esc_html($message_submit); ?></button>
            </div>
            <?php
            if ($branding) {
                ?>
                <div class="emailoctopus-referral">
                <?php echo sprintf(__('Powered by %sEmailOctopus%s', 'emailoctopus'), '<a href="https://emailoctopus.com?utm_source=form&utm_medium=wordpress_plugin" target="_blank" rel="noopener">', '</a>'); ?>
                </div>
                <?php
            }
            ?>
        </div>
    </form>
</div>
