<?php
$title = isset($instance['title']) ? $instance['title'] : __('Subscribe to my list', 'emailoctopus');
$list_id = isset($instance['list_id']) ? $instance['list_id'] : '';
$include_referral = isset($instance['include_referral']) ? (bool)$instance['include_referral'] : false;
$success_redirect_url = isset($instance['success_redirect_url']) ? $instance['success_redirect_url'] : '';
$redirect_on_success = isset($instance['redirect_on_success']) ? (bool)$instance['redirect_on_success'] : false;
$consent_message = isset($instance['consent_message']) ? $instance['consent_message'] : '';
$include_consent = isset($instance['include_consent']) ? (bool)$instance['include_consent'] : false;

$include_first_name_field = false;
$include_last_name_field = false;

if (array_key_exists('include_first_name_field', $instance)) {
    $include_first_name_field = (bool) $instance['include_first_name_field'];
}

if (array_key_exists('include_last_name_field', $instance)) {
    $include_last_name_field = (bool) $instance['include_last_name_field'];
}

if (is_user_logged_in() && strlen($list_id) !== 36):
    ?>
    <p class="emailoctopus-error-message">
        <?php
        _e('Before you can use the EmailOctopus widget, you need to provide a valid list for it to connect to.', 'emailoctopus');
        echo '<br><br>';
        _e('Take a look at the <a href="https://wordpress.org/plugins/emailoctopus/installation/" target="_blank" rel="noopener">installation page</a> for more information.', 'emailoctopus');
        ?>
    </p>
    <?php
else:
    ?>
    <div class="emailoctopus-form-wrapper">
        <?php
        if (trim($title)) {
            echo sprintf(
                '<h2 class="emailoctopus-heading">%s</h2>',
                esc_attr($title)
            );
        }
        ?>
        <p class="emailoctopus-legacy-success-message"></p>
        <p class="emailoctopus-legacy-error-message"></p>
        <form method="post" action="https://emailoctopus.com/lists/<?php echo esc_attr($list_id); ?>/members/external-add" class="emailoctopus-form">
            <div class="emailoctopus-form-row">
                <label><?php _e('Email address (required)', 'emailoctopus'); ?></label>
                <input type="email" id="<?php echo $args['widget_id']; ?>-email-address" name="emailAddress" class="emailoctopus-email-address" />
            </div>
            <?php
            if ($include_first_name_field):
                ?>
                <div class="emailoctopus-form-row">
                    <label><?php _e('First name', 'emailoctopus'); ?></label>
                    <input type="text" id="<?php echo $args['widget_id']; ?>-first-name" name="firstName" class="emailoctopus-first-name" />
                </div>
                <?php
            endif;
            if ($include_last_name_field):
                ?>
                <div class="emailoctopus-form-row">
                    <label><?php _e('Last name', 'emailoctopus'); ?></label>
                    <input type="text" id="<?php echo $args['widget_id']; ?>-last-name" name="lastName" class="emailoctopus-last-name" />
                </div>
                <?php
            endif;
            ?>
            <?php if ($include_consent): ?>
                <div class="emailoctopus-form-row">
                    <input type="checkbox" id="<?php echo $args['widget_id']; ?>-consent" name="consent" class="emailoctopus-consent">
                    <label for="<?php echo $args['widget_id']; ?>-consent"><?php echo esc_attr($consent_message); ?></label>
                </div>
            <?php endif; ?>
            <div class="emailoctopus-form-row-hp" aria-hidden="true">
                <!-- Do not remove this field, otherwise you risk bot signups -->
                <input type="text" id="<?php echo $args['widget_id']; ?>-hp" name="hp<?php echo esc_attr($list_id); ?>" tabindex="-1" autocomplete="nope">
            </div>
            <div class="emailoctopus-form-row-subscribe">
                <?php
                if ($redirect_on_success && !empty($success_redirect_url)):
                ?>
                <input type="hidden" id="<?php echo $args['widget_id']; ?>-success-redirect-url" name="successRedirectUrl" class="emailoctopus-success-redirect-url" value="<?php echo esc_attr($success_redirect_url); ?>" />
                <?php
                endif;
                ?>
                <button type="submit"><?php _e('Subscribe', 'emailoctopus'); ?></button>
            </div>
        </form>
        <?php
        if ($include_referral):
            ?>
            <div class="emailoctopus-referral">
                <?php echo sprintf(__('Powered by %sEmailOctopus%s', 'emailoctopus'), '<a href="https://emailoctopus.com?utm_source=form&utm_medium=wordpress_plugin" target="_blank" rel="noopener">', '</a>'); ?>
            </div>
            <?php
        endif;
        ?>
    </div>
    <?php
endif;
?>
