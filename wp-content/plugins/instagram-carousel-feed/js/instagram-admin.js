/******************************************
* Instagram Feed Carousel - Admin page only
* Author: Ryan Mahabir
* Date: 2017-20-11
* Icon: Bogdan Rosu - https://www.flaticon.com/authors/bogdan-rosu
******************************************/
function parseURL(url) {
    var parser = document.createElement('a'),
        searchObject = {},
		queries, split, i;
		
    // Let the browser do the work
    parser.href = url;
	
	// Convert query string to object
    queries = parser.search.replace(/^\?/, '').split('&');
	
	for( i = 0; i < queries.length; i++ ) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
	}
	
    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
    };
}




function instagramCarousel(element){
    
    /******************************************
	* Variables
	******************************************/
    this.username = 'username';
    this.$element = jQuery( element );
    this.$domNode = '.wrap form p.submit';
	this.$instagramCarousel = this.$element.find( this.$domNode );
	
	var path = parseURL(window.location.href);
	//console.log(path.search);


    /******************************************
	* WordPress user settings
	******************************************/
    this.init = function(name, feed) {
    	//console.log('username: ' + name + ' feed: ' + feed);
    	var flag = true;

    	//WIP - Need to solidify booleans.
    	if (feed === 'false') {
    		flag = false;
    		return;
		}
		
		// Check to see if we are on thge correct plug-in page
		if (path.search === '?page=InstagramCarouselFeed_PluginSettings') {
			console.log('Instagram page');
			this.getInstagram(name, flag);
		}

        return;
    };


    /******************************************
	* Get JSON from Instagram API
	******************************************/
    this.getInstagram = function(username, flag) {

    	var url = 'https://api.instagram.com/'+ username + '?__a=1',
    		self = this;

    	jQuery.getJSON( url, {
			format: 'json'
		})
		.fail(function() {
			//console.log( 'Instagram error' );
			self.$instagramCarousel.prepend('<p style="color: white; margin: 0; padding: 10px; background-color: red;">Instagram account was NOT FOUND. To activate, please check the spelling of the username.</p><p style="color: white; margin: 10px 0; padding: 10px; background-color: black;">* After updating, please refresh the page to see if error still exists.</p>');
			return;

		})
		.done(function( data ) {
			//console.log(data);

			/******************************************
			* Instagram data
			******************************************/
			var userID = data.user.id,
				username = data.user.username,
				userImages = data.user.media.nodes,
				isPrivate = data.user.is_private;

			/******************************************
			* Private Instagram account message.
			******************************************/
			if (isPrivate) {
				//console.log('Private account: ', isPrivate);
				self.$instagramCarousel.prepend('<p style="color: white; margin: 0; padding: 10px; background-color: red;">Instagram account setting is PRIVATE. To activate, please use a public account.</p><p style="color: white; margin: 10px 0; padding: 10px; background-color: black;">* After updating, please refresh the page to see if error still exists.</p>');
				return;
			}

			/******************************************
			* Instagram plug-in message for errors.
			******************************************/
			self.$instagramCarousel.prepend('<p style="color: white; margin: 0 0 20px 0; padding: 10px; background-color: black;">* After updating, please refresh the page to check if there are any errors.</p>');
			return;
			
		});
    };
};


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
		//console.log('ready');
		/*var username,
		  	feed;

		$.each( instagram_options, function( key, value ) {
		  	//console.log( key + ": " + value );

		  	if (key === 'username') {
		  		//console.log(value);
		  		username = value;
		  	}

		  	if (key === 'showFeed') {
		  		//console.log(value);
		  		feed = value;
		  	}
		});

		function init(el, username, feed) {
			var newInstagramFeed = new instagramCarousel( el );
			newInstagramFeed.init(username, feed);
		}

		init('body', username, feed);*/

	});
}(jQuery));



