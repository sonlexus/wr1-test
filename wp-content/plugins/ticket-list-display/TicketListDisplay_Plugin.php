<?php


include_once('TicketListDisplay_LifeCycle.php');

class TicketListDisplay_Plugin extends TicketListDisplay_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            /*'ATextInput' => array(__('Enter in some text', 'my-awesome-plugin')),
            'AmAwesome' => array(__('I like this awesome plugin', 'my-awesome-plugin'), 'false', 'true'),
            'CanDoSomething' => array(__('Which user role can do something', 'my-awesome-plugin'),
                                        'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')*/

            
            /******************************************
            * Custom Admin field to enter data 
            ******************************************/
            'category' => array(__('Enter your product category name (e.g. "tickets")', 'my-awesome-plugin'))

        
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function getPluginDisplayName() {
        return 'Ticket List Display';
    }

    protected function getMainPluginFileName() {
        return 'ticket-list-display.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }


    /******************************************
    * Shop Product Display - Get all data 
    ******************************************/
    public function getShopProductDisplayInfo() {

        /******************************************
        * Call .js file 
        ******************************************/
        /*wp_enqueue_script(
            'shop-product-display-js',
            plugins_url( 'js/shop-product-display.js', __FILE__)
        );*/

        /******************************************
        * Database labels - located in wp-options table
        ******************************************/
        $category = 'TicketListDisplay_Plugin_category';
        $templateStyle = 'TicketListDisplay_Plugin_template';

        /******************************************
        * Check database to see if a username exists
        ******************************************/
        if ( isset($category) ) {
            //echo 'value exists';

            // Rendering array
            $ticket_list = array();

            /******************************************
            * Add all data to an array
            * get_option('') => wp native db function
            ******************************************/
            $script_params = array(
                'category' => get_option($category),
                'templateStyle' => get_option($templateStyle)
            );


            /******************************************
            * Query database for category
            ******************************************/
            $cat = get_option($category);
            //echo $cat;

            
            // Only works for post categories
            /*$term = term_exists( $cat, 'category' );
            if ( $term !== 0 && $term !== null ) {
                echo $cat." category exists!";
            }*/


            // If a value was entered in dashboard
            if (term_exists($cat)) {
                //echo 'True ';

                // Set up array for printing
                $args = array(
                    'post_type'         => 'product',
                    'posts_per_page'    => -1,
                    'offset'            => 0,
                    'product_cat'       => $cat,
                    'orderby'           => 'title',
	                'order'             => 'ASC'
                );
            
                // New query with array
                $loop = new WP_Query( $args );

                
            
                // Looking for posts with category
                while ( $loop->have_posts() ) : $loop->the_post();
                    
                    global $product;
                    //echo '<br /><a href="'.get_permalink().'">' . woocommerce_get_product_thumbnail().' '.get_the_title().'</a>';

                    $post_id = $product->get_id();

                    // Show all meta fields
                    $meta = get_post_meta( $post_id );
                    //echo $meta;
                    /*foreach($meta as $key => $value) {
                        echo $key . ' : ' . $value[0] . '<br/>';
                    }*/

                    $event_permalink = $product->get_permalink();
                    $event_start_date = get_post_meta( $post_id, '_start_date_picker', 'single' );
                    $event_end_date = get_post_meta( $post_id, '_end_date_picker', 'single' );

                    $event_start_time = get_post_meta( $post_id, '_start_time_picker', 'single' );
                    $event_end_time = get_post_meta( $post_id, '_end_time_picker', 'single' );
                    $event_name = $product->get_name();
                    $event_description = $product->get_description();
                    $event_price = get_post_meta( $post_id, '_regular_price', 'single' );
                    $event_location = get_post_meta( $post_id, 'ticket_event_location', 'single' );
                    $event_venue_name = get_post_meta( $post_id, 'ticket_event_venue_name', 'single' );
                    $event_venue_link = get_post_meta( $post_id, 'ticket_event_venue_link', 'single' );
                    $event_buynow_link = get_post_meta( $post_id, 'buy_now_link', 'single' );

                    $cleanStartDate = $this->formatDate($event_start_date);
                    $cleanEndDate = $this->formatDate($event_end_date);

                    foreach($cleanStartDate as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/>';
                        
                        if ( $key === 0 ) {
                            $event_month = $value;
                        } else if ( $key === 1 ) {
                            $event_date = $value;
                        } else if ( $key === 2 ) {
                            $event_year = $value;
                        } else {
                            return;
                        }
                    }
                    //echo $event_month.$event_date.$event_year;

                    $cleanStartTime = $this->formatTime($event_start_time);
                    $cleanEndTime = $this->formatTime($event_end_time);


                    // Echo wanted attributes
                    //echo 'Permalink: '.$event_permalink.'</br>Start Date: '.$cleanStartDate.'</br>End Date: '.$cleanEndDate.'</br>Start Time: '.$cleanStartTime.'</br>End Time: '.$cleanEndTime.'</br>Event Name: '.$event_name.'</br>Description: '.$event_description.'</br>Location: '.$event_location.'</br>Venue Name: '.$event_venue_name.'</br>Venue Link: '.$event_venue_link.'</br>Buy Now Link: '.$event_buynow_link;


                    // Echo HTML with attributes
                    //echo '<li><div class="event-date-container"><h5 class="event-month">'.$event_month.'</h5>'.'<h4 class="event-date">'.$event_date.'</h4>'.'<h5 class="event-year">'.$event_year.'</h5></div><div class="event-information"><p class="event-location"><a href="'.$event_permalink.'" title="'.$event_name.'" target="_self">'.$event_location.'</a></p><p class="event-venue"><a href="'.$event_venue_link.'" title="'.$event_venue_name.'" target="_self">'.$event_venue_name.'</a></p></div><div class="event-cta"><a class="button" href="'.$event_permalink.'">Meet & Greet</a><a class="button" href="'.$event_buynow_link.'">Buy Now</a></div></li>';


                    $today_date = date("Y-m-d");
                    $stock_status = get_post_meta( $post_id, '_stock_status', 'single' );
                    //echo $event_start_date.'</br>';
                    //echo $stock_status.'</br>';

                    if ($stock_status == 'outofstock') {
                        //echo 'out of stock';

                        $event_item = '<li class="sold-out-event"><div class="event-date-container"><h5 class="event-month">'.$event_month.'</h5>'.'<h4 class="event-date">'.$event_date.'</h4>'.'<h5 class="event-year">'.$event_year.'</h5></div><div class="event-information"><p class="event-location"><a href="" title="'.$event_name.'" target="_self">'.$event_location.'</a></p><p class="event-venue"><a href="" title="'.$event_venue_name.'" target="_self">'.$event_venue_name.'</a></p></div><div class="event-price"><span>$ '.$event_price.'</span></div><div class="event-cta"><p>Sold Out</p></div></li>';

                    } else if ($today_date > $event_start_date) {
                        //echo 'expired ticket';
                        
                        $event_item = '<li class="expired-event"><div class="event-date-container"><h5 class="event-month">'.$event_month.'</h5>'.'<h4 class="event-date">'.$event_date.'</h4>'.'<h5 class="event-year">'.$event_year.'</h5></div><div class="event-information"><p class="event-location"><a href="" title="'.$event_name.'" target="_self">'.$event_location.'</a></p><p class="event-venue"><a href="" title="'.$event_venue_name.'" target="_self">'.$event_venue_name.'</a></p></div><div class="event-price"><span>$ '.$event_price.'</span></div><div class="event-cta"><p>Event Passed</p></div></li>';

                    } else {

                        $event_item = '<li><div class="event-date-container"><h5 class="event-month">'.$event_month.'</h5>'.'<h4 class="event-date">'.$event_date.'</h4>'.'<h5 class="event-year">'.$event_year.'</h5></div><div class="event-information"><p class="event-location"><a href="'.$event_permalink.'" title="'.$event_name.'" target="_self">'.$event_location.'</a></p><p class="event-venue"><a href="'.$event_venue_link.'" title="'.$event_venue_name.'" target="_self">'.$event_venue_name.'</a></p></div><div class="event-price"><span>$ '.$event_price.'</span></div><div class="event-cta"><a class="cta-button meet-greet" title="Meet & Greet" href="'.$event_permalink.'">Meet & Greet</a><a class="cta-button buy-ticket" title="Buy Now" href="'.$event_buynow_link.'" target="_blank">Buy Now</a></div></li>';

                    }

                    array_push( $ticket_list, $event_item );

                endwhile;
            
                wp_reset_query();

            } else {
                //echo 'Category does not exist, please confirm and try again.';
                $errorMsg = '<li><h3 class="error-msg">Category does not exist, please confirm and try again.</h3></li>';
                array_push( $ticket_list, $errorMsg );
            }
            

            /******************************************
            * Make php array avaialbe to front-end or admin page
            * wp_localize_script('', '', ''); => wp native function
            ******************************************/
            //echo json_encode($script_params);
            //echo $event_item;
            //wp_localize_script('admin-js', 'instagram_options', $script_params);
            wp_localize_script('ticket-list-display-js', 'shop_options', $ticket_list);


        }


        ob_start();
        echo '<ul id="event-ticket-display"></ul>';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;

        
    }


    /******************************************
    * Format Date
    ******************************************/
    public function formatDate($dateFromDB) {
        $cleanDate = date("M-d-Y", strtotime($dateFromDB));
        $newDate = str_replace("-", " ", $cleanDate);
        $seperator = explode(" ", $newDate);

        return $seperator;
    }


    /******************************************
    * Format Time
    ******************************************/
    public function formatTime($timeFromDB) {
        $cleanTime = date('h:i a', strtotime($timeFromDB));
        return $cleanTime;
    }


    /******************************************
    * Shop Product Display - Add actions for plug-in
    ******************************************/
    public function addActionsAndFilters() {

        /******************************************
        * Add options ADMIN page
        * http://plugin.michael-simpson.com/?page_id=47
        ******************************************/
        // Examples:
            //add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));
            //add_action('admin_menu', array(&$this, 'addSettingsSubMenuPageToSettingsMenu'));

        add_action('admin_menu', array(&$this, 'addSettingsToDashboardMenuPage'));
        

        /******************************************
        * Enqueue scripts and styles for ADMIN pages
        ******************************************/ 
        add_action('admin_enqueue_scripts', array(&$this, 'enqueueAdminPageStylesAndScripts'));
        //add_action('admin_enqueue_scripts', array(&$this, 'getShopProductDisplayInfo'));
        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        //        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
        //            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        //            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        }


        /******************************************
        * Enqueue scripts and styles for Plug/In or Front-End pages
        ******************************************/
        add_action('wp_enqueue_scripts', array(&$this, 'enqueueStylesAndScripts'));
        add_action('wp_enqueue_scripts', array(&$this, 'getShopProductDisplayInfo'));


        /******************************************
        * Add Actions & Filters 
        * Adding scripts & styles to all pages
        * http://plugin.michael-simpson.com/?page_id=37
        ******************************************/
        // Examples:
                //wp_enqueue_script('jquery');
                //wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
                //wp_enqueue_script('my-script', plugins_url('/js/instagram-feed.js', __FILE__));


        /******************************************
        * Register short codes
        * http://plugin.michael-simpson.com/?page_id=39
        ******************************************/
        add_shortcode('ticket-list-display', array($this, 'addShortCode'));

        
        
        /******************************************
        * Register AJAX hooks
        * http://plugin.michael-simpson.com/?page_id=41
        ******************************************/
 
    }


    /******************************************
    * Shop Display Products - ALL
    * Add styles & scripts to ALL pages where feed will be rendered
    ******************************************/
    public function enqueueStylesAndScripts() {

        // Examples:
            //wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
            //wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        
        // CSS files:
        //wp_enqueue_style('slick-css', plugins_url('/js/vendor/slick/slick.css', __FILE__));
        
        // BUG => Not sure why 'custom-css' does not work
        //wp_enqueue_style('custom-css', plugins_url('/css/style.css', __FILE__));
        wp_enqueue_style('custom-style', plugins_url('/css/style.css', __FILE__));

        // JS files:
        //wp_enqueue_script('jquery');
        //wp_enqueue_script('slick', plugins_url('/js/vendor/slick/slick.min.js', __FILE__));
        wp_enqueue_script('ticket-list-display-js', plugins_url('/js/ticket-list-display.js', __FILE__));

    }


    /******************************************
    * Shop Display Products - ADMIN
    * Add styles & scripts to ADMIN pages that plugin-in will be rendered
    ******************************************/
    public function enqueueAdminPageStylesAndScripts() {
        
        // Examples:
            //wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
            //wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));

        // CSS files:
        //wp_enqueue_style('slick-css', plugins_url('/js/vendor/slick/slick.css', __FILE__));
        wp_enqueue_style('custom-style', plugins_url('/css/style.css', __FILE__));

        // JS files:
        //wp_enqueue_script('jquery');
        wp_enqueue_script('admins-js', plugins_url('/js/ticket-list-display-admin.js', __FILE__));
        //wp_enqueue_script('slick', plugins_url('/js/vendor/slick/slick.min.js', __FILE__));

    }


}
