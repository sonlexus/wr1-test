<?php
/**
 * Claue child theme functions.
 *
 * @since   1.0.0
 * @package Claue
 */

 
/************************************
 * Enqueue style of child theme
************************************/
function jas_claue_enqueue_script() {

	wp_enqueue_style( 'jas-claue-parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'custom-header-style', get_stylesheet_directory_uri() . '/custom_css/header.css' );
	wp_enqueue_style( 'custom-shop-style', get_stylesheet_directory_uri() . '/custom_css/shop.css' );
	wp_enqueue_style( 'custom-music-page-style', get_stylesheet_directory_uri() . '/custom_css/music_page.css' );
	wp_enqueue_style('star-ratings-css', get_stylesheet_directory_uri() . '/custom_css/star_ratings.css');
	
	// WIP - Share page css styles
	//wp_enqueue_style( 'custom-share-page-style', get_stylesheet_directory_uri() . '/custom_css/share_page.css' );

	wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/custom_js/custom.js');
	
}

add_action( 'wp_enqueue_scripts', 'jas_claue_enqueue_script' );


/************************************
 * Enqueue Admin style of child theme
************************************/
function load_admin_style() {
	wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/admin_css/revolution-slider.css', false, '1.0.0' );
}

//add_action( 'admin_enqueue_scripts', 'load_admin_style' );


/************************************
 * Redirect Shop Manager to dashboard page on login
************************************/
function wc_custom_user_redirect( $redirect, $user ) {

	// Get the first of all the roles assigned to the user
	$role = $user->roles[0];
	$dashboard = admin_url();
	//$myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );
	//$vendorDashboard = get_permalink(WC_Vendors::$pv_options->get_option( 'vendor_dashboard_page' ));

	if ( $role == 'administrator' ) {
		//Redirect administrators to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'shop_manager' ) {
		//Redirect shop managers to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'editor' ) {
		//Redirect editors to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'author' ) {
		//Redirect authors to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'customer' || $role == 'subscriber' ) {
		//Redirect customers and subscribers to the "My Account" page
		$redirect = $myaccount;
	} else {
		//Redirect any other role to the previous visited page or, if not available, to the home
		$redirect = wp_get_referer() ? wp_get_referer() : home_url();
	}

	return $redirect;
}

add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );


/************************************
 * Redirect Vendors and Customers to My Account page on logout
************************************/
function logging_out(){
	$myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );
	//wp_redirect( home_url() );
	wp_redirect( $myaccount );
	exit();
}

add_action( 'wp_logout', 'logging_out' );


/************************************
 * Restrict customers from changing their email
************************************/
function disable_customer_edit_email( $fields ) {
	//echo 'To update email, please email: hello@wr1.com';

	?>
        <script>
            jQuery(document).ready( function($) {
                if ( $('input[name=account_email]').length ) {
                    $('input[name=account_email]').attr("disabled", "disabled");
                }
            });
        </script>
    <?php
}

add_action( 'woocommerce_edit_account_form', 'disable_customer_edit_email' );


/************************************
 * Add smart app banner for iTunes app download
************************************/
function add_meta_tags() {
	$appID = '975442491';
	$affiliateData = '';
	$appWebURL = 'https://www.wr1.com/';

	echo '<meta name="apple-itunes-app" content="app-id=' . $appID . ', affiliate-data=' . $affiliateData . ', app-argument=' . $appWebURL . '">';
	
}

add_action( 'wp_head', 'add_meta_tags' , 2 );


/************************************
 * Add Facebook Pixel Code
************************************/
function facebookPixel() {
	//echo '<script type="text/javascript" src="/myscript.js"></script>';
	wp_enqueue_script('facebook-pixel-js', get_stylesheet_directory_uri() . '/custom_js/vendor/facebookPixel.js');
}
// Add hook for admin <head></head>
//add_action( 'admin_head', 'facebookPixel' );

// Add hook for front-end <head></head>
//add_action( 'wp_head', 'facebookPixel' );


/************************************
 * Remove Query Strings From Static Resources
************************************/
function _remove_script_version( $src ) {
	$parts = explode( '?', $src );
	return $parts[0];
}

add_filter( 'script_loader_src', '_remove_script_version', 15, 1 ); 
//add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


/************************************
 * Combine all scripts to one
************************************/

// Appear on the top, before the header
function show_head_scripts(){
	global $wp_scripts;
	echo '<pre>'; print_r($wp_scripts->done); echo '</pre>';
}

// Appear on the bottom, after the footer
function show_footer_scripts(){
	global $wp_scripts;
	echo '<pre>'; print_r($wp_scripts->done); echo '</pre>';
}
//add_action( 'wp_head', 'show_head_scripts', 999 );
//add_action( 'wp_footer', 'show_footer_scripts', 999 );


function merge_all_scripts() {
	global $wp_scripts;
	
	/*
		#1. Reorder the handles based on its dependency, 
			The result will be saved in the to_do property ($wp_scripts->to_do)
	*/
	$wp_scripts->all_deps($wp_scripts->queue);	
	
	// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
	$merged_file_location = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'merged-script.js';
	
	$merged_script	= '';
	
	// Loop javascript files and save to $merged_script variable
	foreach( $wp_scripts->to_do as $handle) 
	{
		/*
			Clean up url, for example wp-content/themes/wdc/main.js?v=1.2.4
			become wp-content/themes/wdc/main.js
		*/
		$src = strtok($wp_scripts->registered[$handle]->src, '?');
		
		/*
			#2. Combine javascript file.
		*/
		// If src is url http / https		
		if (strpos($src, 'http') !== false)
		{
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();
		
			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if (strpos($src, $site_url) !== false)
				$js_file_path = str_replace($site_url, '', $src);
			else
				$js_file_path = $src;
			
			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$js_file_path = ltrim($js_file_path, '/');
		}
		else 
		{			
			$js_file_path = ltrim($src, '/');
		}
		
		// Check wether file exists then merge
		if  (file_exists($js_file_path)) 
		{
			// #3. Check for wp_localize_script
			$localize = '';
			if (@key_exists('data', $wp_scripts->registered[$handle]->extra)) {
				$localize = $obj->extra['data'] . ';';
			}
			$merged_script .=  $localize . file_get_contents($js_file_path) . ';';
		}
	}
	
	// write the merged script into current theme directory
	file_put_contents ( $merged_file_location , $merged_script);
	
	// #4. Load the URL of merged file
	wp_enqueue_script('merged-script',  get_stylesheet_directory_uri() . '/merged-script.js');
	
	// 5. Deregister handles
	foreach( $wp_scripts->to_do as $handle ) 
	{
		wp_deregister_script($handle);
	}
}
//add_action( 'wp_enqueue_scripts', 'merge_all_scripts', 999 );


/************************************
 * Combine all styles to one
************************************/
function show_all_styles() {
    // use global to call variable outside function
    global $wp_styles;
     
    // arrange the queue based on its dependency
    $wp_styles->all_deps($wp_styles->queue);  
     
    // The result
    $handles = $wp_styles->to_do;
     
	echo '<pre>'; 
	print_r($handles); 
	echo '</pre>';
}
//add_action('wp_enqueue_scripts', 'show_all_styles');


/************************************
 * Add customer to Emailing list - Octopus Email
************************************/
function send_customer_to_email_list( $order_id ) {
	$order = new WC_Order( $order_id );
	//echo $order;
	//echo $order->billing_first_name;

	$orderID = $order->id;
	$userEmail = $order->billing_email;
	$userFname = $order->billing_first_name;
	$userLname = $order->billing_last_name;

	//echo 'order id: '.$orderID.'first: '.$userFname.' last: '.$userLname.' email: '.$userEmail;

	$accessToken = '85cffe69-d965-11e8-a3c9-06b79b628af2';
	$listID = '01869fa9-682f-11e8-a3c9-06b79b628af2';
	$url = 'https://emailoctopus.com/api/1.5/lists/'.$listID.'/contacts';
	//$JSONapi = 'https://emailoctopus.com/api/1.5/lists/01869fa9-682f-11e8-a3c9-06b79b628af2/contacts?api_key=85cffe69-d965-11e8-a3c9-06b79b628af2';
	//$url = 'https://emailoctopus.com/api/1.5/lists/'.$listID.'/contacts?api_key='.$accessToken;

	$opts = array('http' =>
		array(
			//'method'  => 'POST',
			//'header'  => 'Content-type: application/x-www-form-urlencoded'
		),
		"api_key"	=> $accessToken,
		"email_address"	=> $userEmail,
		"fields"	=> array(
			"FirstName"	=> $userFname,
			"LastName"	=> $userLname
		),
		"status"	=> "UNSUBSCRIBED"
	);

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($opts)
		)
	);

	$context  = stream_context_create($options);
	
	$result = file_get_contents($url, false, $context);

	if ($result === FALSE) { 
		//Handle error
		//echo 'error sending to email octopus';
	}

	//var_dump($result);
}
add_action( 'woocommerce_thankyou', 'send_customer_to_email_list', 10, 1 );


/************************************
 * Send to Email Octopus - OLD
************************************/
/*add_action( 'woocommerce_thankyou', function( $order_id ){
	$order = new WC_Order( $order_id );
	//echo $order;
	//echo $order->billing_first_name;

	$orderID = $order->id;
	$userEmail = $order->billing_email;
	$userFname = $order->billing_first_name;
	$userLname = $order->billing_last_name;

	//echo 'order id: '.$orderID.'first: '.$userFname.' last: '.$userLname.' email: '.$userEmail;

	$accessToken = '85cffe69-d965-11e8-a3c9-06b79b628af2';
	$listID = '01869fa9-682f-11e8-a3c9-06b79b628af2';
	$url = 'https://emailoctopus.com/api/1.5/lists/'.$listID.'/contacts';
	//$JSONapi = 'https://emailoctopus.com/api/1.5/lists/01869fa9-682f-11e8-a3c9-06b79b628af2/contacts?api_key=85cffe69-d965-11e8-a3c9-06b79b628af2';
	//$url = 'https://emailoctopus.com/api/1.5/lists/'.$listID.'/contacts?api_key='.$accessToken;


	$opts = array('http' =>
		array(
			//'method'  => 'POST',
			//'header'  => 'Content-type: application/x-www-form-urlencoded'
		),
		"api_key"	=> $accessToken,
		"email_address"	=> $userEmail,
		"fields"	=> array(
			"FirstName"	=> $userFname,
			"LastName"	=> $userLname
		),
		"status"	=> "SUBSCRIBED"
	);

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($opts)
		)
	);

	$context  = stream_context_create($options);
	
	$result = file_get_contents($url, false, $context);

	if ($result === FALSE) { 
		//Handle error
		echo 'error sending to email octopus';
	}

	var_dump($result);

});*/


function wc_register_users_to_email_list() {
	
	$accessToken = '85cffe69-d965-11e8-a3c9-06b79b628af2';
	$listID = '01869fa9-682f-11e8-a3c9-06b79b628af2';
	$url = 'https://emailoctopus.com/api/1.5/lists/'.$listID.'/contacts';
	//$JSONapi = 'https://emailoctopus.com/api/1.5/lists/01869fa9-682f-11e8-a3c9-06b79b628af2/contacts?api_key=85cffe69-d965-11e8-a3c9-06b79b628af2';
	//$url = 'https://emailoctopus.com/api/1.5/lists/'.$listID.'/contacts?api_key='.$accessToken;


	$opts = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => 'Content-type: application/x-www-form-urlencoded'
		),
		"api_key"	=> $accessToken,
		"email_address"	=> 'ray2322@guest.com',
		"fields"	=> array(
			"FirstName"	=> 'ray',
			"LastName"	=> 'guest'
		),
		"status"	=> "SUBSCRIBED"
	);

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			//'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			//'method'  => 'POST',
			'content' => http_build_query($opts)
		)
	);

	$context  = stream_context_create($options);
	
	$result = file_get_contents($url, false, $context);

	if ($result === FALSE) { 
		//Handle error
		echo 'error sending to email octopus';
	}

	var_dump($result);

}
// Before submit
//add_action( 'woocommerce_after_checkout_billing_form', 'wc_register_users_to_email_list', 10, 1 );
// After submit
//add_action( 'woocommerce_review_order_after_submit', 'wc_register_users_to_email_list', 10, 1 );


/************************************
 * Register Guests
************************************/
function wc_register_guests( $order_id ) {
	// get all the order data
	$order = new WC_Order($order_id);

	//get the user email from the order
	$order_email = $order->billing_email;
		
	// check if there are any users with the billing email as user or email
	$email = email_exists( $order_email );  
	$user = username_exists( $order_email );

	// if the UID is null, then it's a guest checkout
	if( $user == false && $email == false ){
		
		// random password with 12 chars
		$random_password = wp_generate_password();
		
		// create new user with email as username & newly created pw
		$user_id = wp_create_user( $order_email, $random_password, $order_email );
		
		//WC guest customer identification
		update_user_meta( $user_id, 'guest', 'yes' );

		//user's billing data
		update_user_meta( $user_id, 'billing_address_1', $order->billing_address_1 );
		update_user_meta( $user_id, 'billing_address_2', $order->billing_address_2 );
		update_user_meta( $user_id, 'billing_city', $order->billing_city );
		update_user_meta( $user_id, 'billing_company', $order->billing_company );
		update_user_meta( $user_id, 'billing_country', $order->billing_country );
		update_user_meta( $user_id, 'billing_email', $order->billing_email );
		update_user_meta( $user_id, 'billing_first_name', $order->billing_first_name );
		update_user_meta( $user_id, 'billing_last_name', $order->billing_last_name );
		update_user_meta( $user_id, 'billing_phone', $order->billing_phone );
		update_user_meta( $user_id, 'billing_postcode', $order->billing_postcode );
		update_user_meta( $user_id, 'billing_state', $order->billing_state );

		// user's shipping data
		update_user_meta( $user_id, 'shipping_address_1', $order->shipping_address_1 );
		update_user_meta( $user_id, 'shipping_address_2', $order->shipping_address_2 );
		update_user_meta( $user_id, 'shipping_city', $order->shipping_city );
		update_user_meta( $user_id, 'shipping_company', $order->shipping_company );
		update_user_meta( $user_id, 'shipping_country', $order->shipping_country );
		update_user_meta( $user_id, 'shipping_first_name', $order->shipping_first_name );
		update_user_meta( $user_id, 'shipping_last_name', $order->shipping_last_name );
		update_user_meta( $user_id, 'shipping_method', $order->shipping_method );
		update_user_meta( $user_id, 'shipping_postcode', $order->shipping_postcode );
		update_user_meta( $user_id, 'shipping_state', $order->shipping_state );
		
		// link past orders to this newly created customer
		wc_update_new_customer_past_orders( $user_id );

	}

}
//add this newly created function to the thank you page
//add_action( 'woocommerce_after_checkout_billing_form', 'wc_register_guests', 10, 1 );


/************************************
 * Custom Email
************************************/
function my_woo_email($order_id){

	$order = new WC_Order( $order_id );
	$to = $order->billing_email;
	$subject = 'this is my subject';
	$message = 'Hi '.$order->billing_first_name.' '.$order->billing_email;$order->billing_last_name.', thanks for the order!';

	woocommerce_mail( $to, $subject, $message, $headers = "Content-Type: text/htmlrn", $attachments = "" );

}
//add_action('woocommerce_order_status_completed','my_woo_email');