<?php
/**
 * Template Name: Music Album Template
 * Template Post Type: music_album
 * 
 * Credit: Smashicons
 * Instagram share creative: https://www.instagram.com/p/BemFFcsF-5c/
 */

include 'mp3file.class.php';
 
    get_header();

    // Enqueue the styles
    /*wp_enqueue_style('slick-carousel-css', get_stylesheet_directory_uri() . '/custom_css/vendor/slick.css');
    wp_enqueue_style('music-page-css', get_stylesheet_directory_uri() . '/custom_css/music_page.css');
    wp_enqueue_style('star-ratings-css', get_stylesheet_directory_uri() . '/custom_css/star_ratings.css');*/

?>
 
<div id="main-content" class="main-content">
	<div id="primary" class="content-area jas-container">
		<div id="content" class="site-content vc_row" role="main">
        

        <!-- ************************************************* 
        * WR1 Custom code-break 
        ************************************************* -->
        <?php 
            //echo 'Main content';
            
            // Get post slug from url
            global $post;
            $post_slug = $post->post_name;
            //echo $post_slug;

            // Get all subscription orders
            $music_albums = get_posts( array(
                //'numberposts' => -1,
                'name'  => $post_slug,
                'post_type'   => 'music_album', // Subscription post type
                //'post_status' => 'wc-active', // Active subscription
                'orderby' => 'post_date', // ordered by date
                'order' => 'DESC',
                /*'date_query' => array( // Start & end date
                    array(
                        'after'     => $from_date,
                        'before'    => $to_date,
                        'inclusive' => true,
                    ),
                ),*/
            ) );

            $server_host = $_SERVER['HTTP_HOST'];
            //echo 'Server Host: ' . $server_host;

            $page_url = $_SERVER['REQUEST_URI'];
            //echo 'Page URL: ' . $page_url;

            $album_count = sizeOf( $music_albums );
            //echo 'Album Count: ' . $album_count . '<br/><br/>';


            // Going through each current album order
            foreach ( $music_albums as $album ) {
                
                // Get Album ID
                $album_id = $album->ID; 
                //echo 'Album ID: '.$album_id.'<br/>';

                // Get Album Permalink
                $album_permalink = get_permalink($album_id); 
                //echo 'Album Permalink: '.$album_permalink.'<br/>';

                // Get album date
                $album_date = array_shift( explode( ' ', $album->post_date ) );
                //echo 'Album Date: '.$album_date.'<br/>';

                // Get album title data
                $album_title = $album->post_title;
                //echo 'Album Title: '.$album_title.'<br/>';

                // Get album content
                $album_content = $album->post_content;
                //echo 'Album Content: '.$album_content.'<br/><br/>';

                // Get album meta data
                $album_meta_data = get_post_meta( $album_id );
                //echo $album_meta_data;
                /*foreach($album_meta_data as $key => $value) {
                    echo $key . ' : ' . $value . '<br/><br/>';
                    
                    foreach($value as $key2 => $value2) {
                        //echo $key2 . ' : ' . $value2 . '<br/><br/>';
                    }
                }*/




                /**********************************************
                 * Get album type
                **********************************************/
                $album_type_data = $album_meta_data['type_of_album'];
                if ( is_array($album_type_data) ) {
                    foreach($album_type_data as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_type = $value;
                        //echo 'Album Type: ' . $album_type . '<br/>';
                    }
                }




                /**********************************************
                 * Get album release date
                **********************************************/
                $album_release_date_key = $album_meta_data['album_release_date'];
                if ( is_array($album_release_date_key) ) {

                    foreach($album_release_date_key as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_release_date_raw = $value;
                        $album_release_date = date("F j, Y", strtotime($album_release_date_raw));
                        //echo 'Album Release Date: ' . $album_release_date . '<br/>';
                    }
                }




                /**********************************************
                 * Get album featured image
                **********************************************/
                $album_featured_image = $album_meta_data['album_featured_image'];
                if ( is_array($album_featured_image) ) {

                    foreach($album_featured_image as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_image_url = $value;
                        $album_image = wp_get_attachment_image_src($album_image_url, $size = 'full');
                        //echo 'Album Featured Image: ' . $album_image[0] . '<br/>';
                    }
                }




                /**********************************************
                 * Get paralax background image
                **********************************************/
                $album_featured_image2 = $album_meta_data['_thumbnail_id'];
                if ( is_array($album_featured_image2) ) {

                    foreach($album_featured_image2 as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_image_url = $value;       
                        $album_image2 = wp_get_attachment_image_src($album_image_url, $size = 'full');
                        //echo 'Album Featured Image: ' . $album_image2[0] . '<br/>';
                    }

                    //$parallax_container = '<div class="parallax" style="background-image: url(' . $album_image2[0] . ');"></div>';
                    $parallax_container = '<div class="parallax" data-parallax="scroll" data-image-src="' . $album_image2[0] . '" data-speed="1.4"></div>';
                    echo $parallax_container;

                }




                /**********************************************
                 * Start content
                **********************************************/  
                $content_container = '<div class="parallax-content">';




                /**********************************************
                 * Get album sleeve image
                **********************************************/
                //$music_album_sleeve = '<div class="music-album-cover"><img src="' . $album_image[0] . '" alt="' . $album_title . '" /></div>';
                $music_album_sleeve = '<div class="music-album-cover" style="background-image: url(' .$album_image[0] . ')"></div>';
                echo $content_container . $music_album_sleeve;
                



                  
                /**********************************************
                 * Get album title
                **********************************************/
                $music_album_title = '<div id="album-title"><h2>' . $album_title . '</h2></div>';
                echo $music_album_title;




                  
                /**********************************************
                 * Get album tracks
                **********************************************/
                $album_tracks = $album_meta_data['album_tracks'];
                if ( is_array($album_tracks) ) {




                    /**********************************************
                     * Start left-aligned content
                    **********************************************/
                    $music_album_container = '<div id="album-content">';
                    echo $music_album_container;



                    /**********************************************
                     * Get amount of tracks
                    **********************************************/
                    foreach ( $album_tracks as $key => $value ) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_tracks_amount = $value[0];
                    }




                    /**********************************************
                     * Render album tracks table
                    **********************************************/
                    $album_table_header = '<table class="album-tracks"><tbody>
                        <tr>
                            <th>Tracks</th>
                            <th class="track-lyric-link"></th>
                            <th class="track-time"></th>
                            <th class="track-share-links">Listen</th>
                            <th class="track-share-links">Buy</th>
                            <th class="track-share-links">Watch</th>
                        </tr>';
                    echo $album_table_header;




                    /**********************************************
                     * Render for each track
                    **********************************************/
                    for ( $i = 0; $i < $album_tracks_amount; $i++ ) {
                        //echo "The number is: $i <br>";


                        /**********************************************
                         * Variables
                        **********************************************/
                        $album_track_number = $i + 1;



                        /**********************************************
                         * Get album track title
                        **********************************************/
                        $album_track_title = $album_meta_data['album_tracks_'. $i . '_track_title'];
                        if ( is_array($album_track_title) ) {
                            foreach($album_track_title as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/>';
                                $track_title = $value;
                                //echo 'Track title: ' . $track_title . '<br/>';
                            }
                        }


                        /**********************************************
                         * Get album mp3 file
                        **********************************************/
                        $album_track_mp3_file = $album_meta_data['album_tracks_'. $i . '_mp3_audio_file'];
                        if ( is_array($album_track_mp3_file) ) {

                            /**********************************************
                             * Get album mp3 file duration
                            **********************************************/
                            foreach($album_track_mp3_file as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/>';
                                
                                $mp3_url = wp_get_attachment_url( $value );
                                //echo 'Album MP3 Url: ' . $mp3_url . '<br/>';
                                $mp3file = new MP3File( $mp3_url );
                                /*$duration1 = $mp3file->getDurationEstimate(); //(faster) for CBR only
                                echo "duration: $duration1 seconds"."\n";*/
                                $duration = $mp3file->getDuration(); //(slower) for VBR (or CBR)
                                //echo 'MP3 Time Duration: ' . $duration2 . ' seconds.<br/>';
                                //echo 'MP3 Time Duration: ' . MP3File::formatTime($duration) . ' seconds.<br/>';
                                $timestamp = MP3File::formatTime($duration);
                                $cleanTime = date("i:s", strtotime($timestamp));
                                //echo $cleanTime . '</br>';
                            }   
                            
                        }


                        /* 
                        * Start of track row
                        */
                        echo '<tr class="track" id="track-' . $i . '">';


                        /**********************************************
                         * Start rendering
                        **********************************************/
                        $audioString = '<td><a id="playbutton-' . $i . '"  class="play playbutton"></a><h4><span class="track-number" id="track-number-' . $i . '">' . $album_track_number . '.</span> ' . $track_title . '</h4></td>
                            <td class="track-lyric-link"><a class="more-icon">More</a><p class="view-track-lyrics">Lyrics</p></td>
                            <td class="track-time"><span>' . $cleanTime . '</span></td>';
                        echo $audioString;


                        /**********************************************
                         * Render track share links - Listen now
                        **********************************************/
                        $track_links_count = $album_meta_data['album_tracks_'. $i . '_track_external_links_listen_now'];
                        $track_link_amount = $track_links_count[0];
                        //echo 'Track links count: ' . $track_link_amount . '<br/><br/>';
                        
                        echo '<td class="track-share-links">';
                        if ( is_array($track_links_count) ) {
                            for ( $v = 0; $v < $track_link_amount; $v++ ) {
                            
                                $track_link_hostname = $album_meta_data['album_tracks_'. $i . '_track_external_links_listen_now_'. $v . '_external_track_link_hostname'];

                                $track_link_classname = strtolower($track_link_hostname[0]);

                                if ($track_link_classname == 'google play') {
                                    //echo 'google play';
                                    $track_link_classname = 'google';
                                }

                                $track_link = $album_meta_data['album_tracks_'. $i . '_track_external_links_listen_now_'. $v . '_external_track_link'];

                                if ( empty($track_link[0]) ) {
                                    // list is empty.
                                } else {
                                    echo '<div class="share-links ' . $track_link_classname . '"><a class="track-link-' . $track_link_classname . '" href="' . $track_link[0] . '" title="' . $track_link_hostname[0] . '" target="_blank">' . $track_link_hostname[0] . '</a></div>';
                                }

                                //echo 'Track Link Hostname: ' . $track_link_hostname[0] . '<br/>';
                                //echo 'Track URL: ' . $track_link[0] . '<br/><br/>';

                                // Push to array
                                /*array_push( $album_track_external_links_hostname_array, $track_link_hostname[0] );
                                array_push( $album_track_external_links_url_array, $track_link[0] );*/ 
                            }
                        }
                        echo '</td>';




                        /**********************************************
                         * Render track share links - Buy now
                        **********************************************/
                        $track_links_count_buy = $album_meta_data['album_tracks_'. $i . '_track_external_links_buy_now'];
                        $track_link_amount_buy = $track_links_count_buy[0];
                        //echo 'Track links count: ' . $track_link_amount_buy . '<br/><br/>';
                        
                        echo '<td class="track-share-links">';
                        if ( is_array($track_links_count_buy) ) {
                            for ( $v = 0; $v < $track_link_amount_buy; $v++ ) {
                            
                                $track_link_hostname_buy = $album_meta_data['album_tracks_'. $i . '_track_external_links_buy_now_'. $v . '_external_track_link_hostname_buy'];

                                $track_link_classname_buy = strtolower($track_link_hostname_buy[0]);

                                if ($track_link_classname_buy == 'google play') {
                                    //echo 'google play';
                                    $track_link_classname_buy = 'google';
                                }

                                $track_link_buy = $album_meta_data['album_tracks_'. $i . '_track_external_links_buy_now_'. $v . '_external_track_link_buy'];

                                //echo 'Track Link Hostname Buy: ' . $track_link_hostname_buy[0] . '<br/>';
                                //echo 'Track URL Buy: ' . $track_link_buy[0] . '<br/><br/>';

                                // Push to array
                                /*array_push( $album_track_external_links_hostname_array, $track_link_hostname[0] );
                                array_push( $album_track_external_links_url_array, $track_link[0] );*/

                                echo '<div class="share-links ' . $track_link_classname_buy . '"><a class="track-link-buy-' . $track_link_classname_buy . '" href="' . $track_link_buy[0] . '" title="' . $track_link_hostname_buy[0] . '" target="_blank">' . $track_link_hostname_buy[0] . '</a></div>';
                            }
                        }
                        echo '</td>';




                        /**********************************************
                         * Render track share links - Watch now
                        **********************************************/
                        $track_links_count_watch = $album_meta_data['album_tracks_'. $i . '_track_external_links_watch_now'];
                        $track_link_amount_watch = $track_links_count_watch[0];
                        //echo 'Track links count: ' . $track_link_amount_buy . '<br/><br/>';
                        
                        echo '<td class="track-share-links">';
                        if ( is_array($track_links_count_watch) ) {
                            for ( $v = 0; $v < $track_link_amount_watch; $v++ ) {
                            
                                $track_link_hostname_watch = $album_meta_data['album_tracks_'. $i . '_track_external_links_watch_now_'. $v . '_external_track_link_hostname_watch'];

                                $track_link_classname_watch = strtolower($track_link_hostname_watch[0]);

                                if ($track_link_classname_watch == 'google play') {
                                    //echo 'google play';
                                    $track_link_classname_watch = 'google';
                                }

                                $track_link_watch = $album_meta_data['album_tracks_'. $i . '_track_external_links_watch_now_'. $v . '_external_track_link_watch'];

                                //echo 'Track Link Hostname Watch: ' . $track_link_hostname_watch[0] . '<br/>';
                                //echo 'Track URL Watch: ' . $track_link_watch[0] . '<br/><br/>';

                                // Push to array
                                /*array_push( $album_track_external_links_hostname_array, $track_link_hostname[0] );
                                array_push( $album_track_external_links_url_array, $track_link[0] );*/

                                echo '<div class="share-links ' . $track_link_classname_watch . '"><a class="track-link-watch-' . $track_link_classname_watch . '" href="' . $track_link_watch[0] . '" title="' . $track_link_hostname_watch[0] . '" target="_blank">' . $track_link_hostname_watch[0] . '</a></div>';
                            }
                        }
                        echo '</td>';

                       


                        
                        /* 
                        * End of track row
                        */
                        echo '</tr>';





                        /* 
                        * Start of audio & lyrics track row
                        */
                        echo '<tr>';


                        /**********************************************
                         * Render track MP3 file
                        **********************************************/
                        $mp3Dom = '<td colspan="6"><audio id="audio-' . $i . '" class="audio" preload="true"><source src="' . $mp3_url . '"></audio>';
                        echo $mp3Dom;

                        $playerString = '<div id="audioplayer-' . $i . '" class="audioplayer"><div id="timeline-' . $i . '" class="timeline"><div id="playhead-' . $i . '" class="playhead"></div></div></div>';
                        echo $playerString;


                        /*
                        * Start of track lyrics
                        */
                        echo '<div class="track-lyrics">';


                        /**********************************************
                         * Render mobile track share links
                        **********************************************/
                        if ( is_array($track_links_count) ) {
                            
                            /*
                            * Start of mobile track share links
                            */
                            echo '<div class="mobile-share-links">';
                            
                            /**********************************************
                            * Listen now links
                            **********************************************/
                            for ( $e = 0; $e < $track_link_amount; $e++ ) {
                                
                                $track_link_hostname = $album_meta_data['album_tracks_'. $i . '_track_external_links_listen_now_'. $e . '_external_track_link_hostname'];

                                $track_link_listen = $album_meta_data['album_tracks_'. $i . '_track_external_links_listen_now_'. $e . '_external_track_link'];

                                $track_link_classname = strtolower($track_link_hostname[0]);
                                $track_link_title = ucfirst( $track_link_classname );

                                // Dynamic width css style
                                $container_width = $track_link_amount * 30;
                                //echo $container_width;

                                if ($track_link_classname == 'google play') {
                                    //echo 'google play';
                                    $track_link_classname = 'google';
                                }

                                if ( is_array($track_link_listen) && $e === 0 ) {
                                    echo '<p>Listen</p><div class="share-track-mobile" style="width:' . $container_width . 'px;">';
                                }

                                if ( is_array($track_link_listen) ) {

                                    $mobile_share_links = '<div class="share-links ' . $track_link_classname . '"><a class="track-link-' . $track_link_classname . '" href="' . $track_link_listen[0] . '" title="' . $track_link_hostname[0] . '" target="_blank">' . $track_link_hostname[0] . '</a></div>';
                                    echo $mobile_share_links;
                                }

                                // Close </div> at last element
                                $lastElement = $e + 1;
                                $int_track_link_amount = (int)$track_link_amount;
                                //echo $int_track_link_amount . $lastElement;

                                if ( $lastElement === $int_track_link_amount ) {
                                    //echo 'last element in array';
                                    echo '</div>';
                                }

                            }

                            


                            /**********************************************
                            * Buy now links
                            **********************************************/
                            if ( is_array($track_links_count_buy) ) {
                                for ( $v = 0; $v < $track_link_amount_buy; $v++ ) {
                                
                                    $track_link_hostname_buy = $album_meta_data['album_tracks_'. $i . '_track_external_links_buy_now_'. $v . '_external_track_link_hostname_buy'];

                                    $track_link_buy = $album_meta_data['album_tracks_'. $i . '_track_external_links_buy_now_'. $v . '_external_track_link_buy'];

                                    $track_link_classname_buy = strtolower($track_link_hostname_buy[0]);

                                    // Dynamic width css style
                                    $container_width = $track_link_amount_buy * 30;
                                    //echo $container_width;

                                    if ($track_link_classname_buy == 'google play') {
                                        //echo 'google play';
                                        $track_link_classname_buy = 'google';
                                    }

                                    if ( is_array($track_link_buy) && $v === 0 ) {
                                        echo '<p>Buy</p><div class="share-track-mobile" style="width:' . $container_width . 'px;">';
                                    }


                                    if ( is_array($track_link_buy) ) {

                                        $mobile_share_links_buy = '<div class="share-links ' . $track_link_classname_buy . '"><a class="track-link-buy-' . $track_link_classname_buy . '" href="' . $track_link_buy[0] . '" title="' . $track_link_classname_buy . '" target="_blank">' . $track_link_hostname_buy[0] . '</a></div>';
                                        echo $mobile_share_links_buy;
                                    }


                                    // Close </div> at last element
                                    $lastElement_buy = $v + 1;
                                    $int_track_link_amount_buy = (int)$track_link_amount_buy;
                                    //echo $int_track_link_amount . $lastElement;

                                    if ( $lastElement_buy === $int_track_link_amount_buy ) {
                                        //echo 'last element in array';
                                        echo '</div>';
                                    }
                                    
                                }
                            }


                            /**********************************************
                            * Watch now links
                            **********************************************/
                            if ( is_array($track_links_count_watch) ) {
                                for ( $t = 0; $t < $track_link_amount_watch; $t++ ) {
                                
                                    $track_link_hostname_watch = $album_meta_data['album_tracks_'. $i . '_track_external_links_watch_now_'. $t . '_external_track_link_hostname_watch'];

                                    $track_link_watch = $album_meta_data['album_tracks_'. $i . '_track_external_links_watch_now_'. $t . '_external_track_link_watch'];
    
                                    $track_link_classname_watch = strtolower($track_link_hostname_watch[0]);

                                    // Dynamic width css style
                                    $container_width = $track_link_amount_watch * 30;
                                    //echo $container_width;
    
                                    if ($track_link_classname_watch == 'google play') {
                                        //echo 'google play';
                                        $track_link_classname_watch = 'google';
                                    }

                                    if ( is_array($track_link_watch) && $t === 0 ) {
                                        echo '<p>Watch</p><div class="share-track-mobile" style="width:' . $container_width . 'px;">';
                                    }

                                    if ( is_array($track_link_watch) ) {

                                        $mobile_share_links_watch = '<div class="share-links ' . $track_link_classname_watch . '"><a class="track-link-watch-' . $track_link_classname_watch . '" href="' . $track_link_watch[0] . '" title="' . $track_link_hostname_watch[0] . '" target="_blank">' . $track_link_hostname_watch[0] . '</a></div>';
                                        echo $mobile_share_links_watch;
                                    }
                                    

                                    // Close </div> at last element
                                    $lastElement_watch = $t + 1;
                                    $int_track_link_amount_watch = (int)$track_link_amount_watch;
                                    //echo $track_link_amount_watch . $lastElement_watch;

                                    if ( $lastElement_watch === $int_track_link_amount_watch ) {
                                        //echo 'last element in array';
                                        echo '</div>';
                                    }
    
                                    //echo '<div class="share-links ' . $track_link_classname_watch . '"><a href="' . $track_link_watch[0] . '" title="' . $track_link_hostname_watch[0] . '" target="_blank">' . $track_link_hostname_watch[0] . '</a></div>';
                                }
                            }


                            /*
                            * End of mobile track share links
                            */
                            echo '</div>';
                        }


                        /**********************************************
                         * Get track lyrics
                        **********************************************/
                        $track_lyrics = $album_meta_data['album_tracks_'. $i . '_track_lyrics'];

                        //echo '<div class="track-lyrics">';
                        if ( is_array($track_lyrics) ) {
                            foreach($track_lyrics as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/>';
                                $track_lyric = $value;
                                //echo 'Track lyric: ' . $track_lyric . '<br/>';

                                //array_push( $album_track_lyrics_array, $track_lyric );

                                //echo '<p>' . $track_lyric . '</p>';

                                $lyrics_container = '<p>' . $track_lyric . '</p>';
                                echo $lyrics_container;
                            }
                        }

                        /*
                        * End of track lyrics
                        */
                        echo '</div>';



                        /* 
                        * End of audio & lyrics track row
                        */
                        echo '</td></tr>';

                        
                    }




                    /* 
                    * End of track table
                    */
                    echo '</tbody></table>';   




                }




                /**********************************************
                 * Get album share links
                **********************************************/
                $album_links = $album_meta_data['album_external_link'];
                echo '<div class="album-share-links"><h5>Share this album</h5>';
                if ( is_array($album_links) ) {

                    // Get amount of share links
                    foreach ( $album_links as $key => $value ) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_links_amount = $value[0];
                        //echo '<br/><br/>Album External Links Count: ' . $album_links_amount . '<br/>';
                    }

                    for ( $i = 0; $i < $album_links_amount; $i++ ) {
                        //echo "The number is: $i <br>";
                        
                        // Get album external link icon
                        $album_link_hostname = $album_meta_data['album_external_link_'. $i . '_album_external_link_hostname'];
                        if ( is_array($album_link_hostname) ) {
                            foreach($album_link_hostname as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/>';

                                //$icon = wp_get_attachment_url( $value );
                                //echo 'Link icon: ' . $icon . '<br/>';
                                $album_external_link_hostname = $value;
                            }
                        }
                        //echo $album_external_link_hostname;

                        // Get album external link url
                        $album_link = $album_meta_data['album_external_link_'. $i . '_album_external_link'];
                        if ( is_array($album_link) ) {
                            foreach($album_link as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/>';
                                $link_url = $value;
                            }
                        }
                        //echo $link_url;


                        // Get album external share content
                        $album_share_content_meta = $album_meta_data['album_external_link_'. $i . '_album_share_content'];
                        if ( is_array($album_share_content_meta) ) {
                            foreach($album_share_content_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/>';
                                $album_share_content = $value;
                            }
                        }
                        //echo $album_share_content;


                        $album_link_classname = strtolower($album_external_link_hostname);


                        // Share album on Google Play
                        /*if ($album_link_classname == 'google play') {
                            //echo 'google play';
                            $album_link_classname = 'google';
                        }*/


                        // Share album on Facebook
                        if ($album_link_classname == 'facebook') {
                            //echo 'google play';
                            $album_link_classname = 'facebook';

                            $facebook_url = "https://www.facebook.com/sharer/sharer.php?u=https://" . $server_host . $page_url . "&src=sdkpreparse";
                            //echo urlencode($facebook_url);

                            //$fb_button = '<div class="fb-share-button share-links ' . $album_link_classname . '" data-href="' . $page_url . '" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fthedriverera.com%2Fmusic&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>';

                            $fb_button = '<div class="fb-share-button share-links ' . $album_link_classname . '" data-href="' . $server_host . $page_url . '" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" title="Facebook" href="' . $facebook_url . '" class="fb-xfbml-parse-ignore album-share-link-facebook">Share</a></div>';

                            echo $fb_button;
                        }


                        // Share album on Twitter
                        if ($album_link_classname == 'twitter') {
                            //echo 'google play';
                            $album_link_classname = 'twitter';

                            /*$twitter_share_meta = '<meta name="twitter:card" content="photo" />
                            <meta name="twitter:site" content="@example" />
                            <meta name="twitter:title" content="My picture" />
                            <meta name="twitter:description" content="A description" />
                            <meta name="twitter:image" content="' . $album_image[0] . '" />
                            <meta name="twitter:url" content="https:/' . $page_url . '" />';
                            echo $twitter_share_meta;*/

                            $content_encoded = urlencode($album_share_content);
                            //echo $content_encoded;

                            $twitter_url = 'http://twitter.com/share?text=' . $content_encoded . '&url=https:/' . $server_host . $page_url . '&hashtags=' . $album_title;
                            //echo urlencode($twitter_url);

                            $twitter_button = '<div class="share-links ' . $album_link_classname . '"><a class="album-share-link-twitter" target="_blank" title="Twitter" href="' . $twitter_url . '">Share</a></div>';
                            echo $twitter_button;
                            
                        }


                        // Share album on LinkedIn
                        if ($album_link_classname == 'linked in') {
                            //echo 'google play';
                            $album_link_classname = 'linkedin';

                            $content_encoded = urlencode($album_share_content);
                            //echo $content_encoded;

                            $linkedIn_url = 'http://www.linkedin.com/shareArticle?mini=true&url=https://' . $server_host . $page_url . '&title=' . $album_title . '&summary=' . $content_encoded . '&source=https://' . $server_host . $page_url;
                            //echo urlencode($linkedIn_url);

                            $linkedIn_button = '<div class="share-links ' . $album_link_classname . '"><a class="album-share-link-linkedIn" target="_blank" title="Linked In" href="' . $linkedIn_url . '">Share</a></div>';
                            echo $linkedIn_button;
                            
                        }


                        // Share album on Pinterest
                        if ($album_link_classname == 'pinterest') {
                            //echo 'google play';
                            $album_link_classname = 'pinterest';

                            //$pinterest_url = '<a href="//pinterest.com/pin/create/link/?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F&media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg&description=Next%20stop%3A%20Pinterest">Pin it</a>';

                            $content_encoded = urlencode($album_share_content);

                            $pinterest_url = 'http://pinterest.com/pin/create/link/?url=https:/' . $server_host . $page_url . '&media=' . $album_image[0] . '&description=' . $content_encoded;
                            //echo urlencode($pinterest_url);

                            $pinterest_button = '<div class="share-links ' . $album_link_classname . '"><a class="album-share-link-pinterest" target="_blank" title="Pinterest" href="' . $pinterest_url . '">Share</a></div>';
                            echo $pinterest_button;                            
                        }


                        // Share album on Tumblr
                        if ($album_link_classname == 'tumblr') {
                            //echo 'google play';
                            $album_link_classname = 'tumblr';

                            //$tumblr_url = '<a id="tumblr-link" href="" title="Share in Tumblr">Share on Tumblr</a>';
                            //http://profitquery.com/add-to/tumblr/?url=[url]&title=[title]&img=[img]&image_sharer=1



                            $content_encoded = urlencode($album_share_content);

                            $tumblr_url = 'http://www.tumblr.com/share/link?url=https:/' . $server_host . $page_url . '&title=' . $album_title . '&img=' . $album_image[0] . '&image_sharer=1&content=' . $content_encoded;
                            //echo urlencode($tumblr_url);

                            $tumblr_button = '<div class="share-links ' . $album_link_classname . '"><a class="album-share-link-tumblr" target="_blank" title="Tumblr" href="' . $tumblr_url . '">Share</a></div>';
                            echo $tumblr_button;                            
                        }


                        /*$album_links_dom2 = '<div class="share-links ' . $album_link_classname . '"><a href="' . $link_url . '" title="' . $album_external_link_hostname . '" target="_blank">' . $album_external_link_hostname . '</a></div>';
                        echo $album_links_dom2;*/ 

                    }

                }
                echo '</div>';




                /* 
                * End of left-aligned content
                */
                echo '</div>';




                /**********************************************
                 * Get about album
                 * Start right-aligned content
                **********************************************/
                if ( !empty($album_content) ) {
                    //echo "Yes, about album is set"; 
                    $album_type_lowercase = strtolower($album_type); 
                    //echo '<div id="album-about"><div class="about-content"><h3>About this ' . $album_type_lowercase . '</h3><p><strong>Release Date: </strong>' . $album_release_date . '</p><p>' . $album_content . '</p></div>';
                    
                    echo '<div id="album-about"><div class="about-content"><h3>About this ' . $album_type_lowercase . '</h3><p><strong>Release Date: </strong>' . $album_release_date . '</p><p>' . $album_content . '</p></div></div>';  
                }




                /**********************************************
                 * Get buy now links for album
                **********************************************/
                $album_buy_album_link_meta = $album_meta_data['album_external_buy_link'];
                if ( is_array($album_buy_album_link_meta) ) {
                    foreach($album_buy_album_link_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_buy_album_link_amount = $value;
                        //echo 'Album Buy Now Link Count: ' . $album_buy_album_link_amount . '<br/>';
                    }

                    /* 
                    * Start of buy now content
                    */
                    echo '<div id="buy-album"><h3>Buy this ' . $album_type_lowercase . '</h3>';

                    for ( $c = 0; $c < $album_buy_album_link_amount; $c++ ) {

                        // Buy album Hostname
                        $album_buy_album_hostname_meta = $album_meta_data['album_external_buy_link_' . $c . '_album_external_buy_link_hostname'];
                        if ( is_array($album_buy_album_hostname_meta) ) {
                            foreach($album_buy_album_hostname_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_buy_album_hostname = $value;
                                $album_buy_album_hostname_lowercase = strtolower($album_buy_album_hostname);
                                //echo 'Album Buy Now Hostname: ' . $album_buy_album_hostname . '<br/>';
                            }
                        }


                        // Buy album URL
                        $album_buy_album_url_meta = $album_meta_data['album_external_buy_link_' . $c . '_album_external_buy_link_url'];
                        if ( is_array($album_buy_album_url_meta) ) {
                            foreach($album_buy_album_url_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_buy_album_url = $value;
                                //echo 'Album Buy Now URL: ' . $album_buy_album_url . '<br/>';
                            }
                        }

                        $buy_album_el = '
                            <a class="album-buy-' . $album_buy_album_hostname_lowercase . '" href="' . $album_buy_album_url . '" title="' . $album_buy_album_hostname . '" target="_blank">' . $album_buy_album_hostname . '</a>
                        ';
                        echo $buy_album_el;

                    }

                    /* 
                    * End of buy now content
                    */
                    echo '</div>';

                }




            /* 
            * End of left-right aligned content
            */
            echo '<div class="clearfix"></div>';




            /**********************************************
             * Get album addition information tab
            **********************************************/
            $album_credit_tab = $album_meta_data['album_credit_tab'];
            if ( is_array($album_credit_tab) ) {
                
                
                /*foreach($album_credit_tab as $key => $value) {
                    echo $key . ' : ' . $value . '<br/><br/>';
                    
                    //$album_type = $value;
                    //echo 'Album Type: ' . $album_type . '<br/>';
                }*/


                /**********************************************
                 * Get album credit button text
                **********************************************/
                $album_credit_button = $album_meta_data['album_credit_tab_button_text'];
                if ( is_array($album_credit_button) ) {
                    foreach($album_credit_button as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_credit_button_text = $value;
                        //echo 'Album Credit Button Text: ' . $album_credit_button_text . '<br/>';
                    }
                }

                
                /**********************************************
                 * Get album contributor tab title
                **********************************************/
                $album_credit_page_title_meta = $album_meta_data['album_credit_tab_section_title'];
                if ( is_array($album_credit_page_title_meta) ) {
                    foreach($album_credit_page_title_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_credit_page_title = $value;
                        //echo 'Album Credit Title: ' . $album_credit_page_title . '<br/>';
                    }
                }


                /**********************************************
                 * Get album credts tab section title
                **********************************************/
                $album_credit_section_title_meta = $album_meta_data['album_credit_tab_contributor_title'];
                if ( is_array($album_credit_section_title_meta) ) {
                    foreach($album_credit_section_title_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_credit_section_title = $value;
                        //echo 'Album Credit Section Title: ' . $album_credit_section_title . '<br/>';
                    }
                }


                /**********************************************
                 * Get album video button text
                **********************************************/
                $album_video_button = $album_meta_data['album_video_tab_button_text'];
                if ( is_array($album_video_button) ) {
                    foreach($album_video_button as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_video_button_text = $value;
                        //echo 'Album Video Button Text: ' . $album_video_button_text . '<br/>';
                    }
                }
                

                /**********************************************
                 * Get album review button text
                **********************************************/
                $album_review_button = $album_meta_data['album_review_tab_button_text'];
                if ( is_array($album_review_button) ) {
                    foreach($album_review_button as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_review_button_text = $value;
                        //echo 'Album Review Button Text: ' . $album_review_button_text . '<br/>';
                    }
                }


                
                /**********************************************
                 * Get album credit contributors - Advanced
                **********************************************/
                /*$album_credit_contributors = $album_meta_data['album_credit_tab_contributor_information'];
                if ( is_array($album_credit_contributors) ) {
                    foreach($album_credit_contributors as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_contributors_amount = $value;
                        //echo 'Album Credit Contributors Amount: ' . $album_contributors_amount . '<br/>';
                    }


                    echo '<div id="album-additional-info">';
                    
                    $tab_buttons = '<div class="tab">
                            <button class="tablinks active" data-value="' . $album_credit_button_text . '">' . $album_credit_button_text . '</button>
                        </div>';
                    echo $tab_buttons;


                    $tab_class_lowercase = strtolower($album_credit_button_text);

                    $tab_content = '<div id="value-' . $tab_class_lowercase . '" class="tabcontent active"><h3>' . $album_credit_page_title . '</h3><h4>' . $album_credit_section_title . '</h4><ul class="contributor-list">';
                    echo $tab_content;


                    for ( $c = 0; $c < $album_contributors_amount; $c++ ) {

                        // Contributor Name
                        $album_credit_contributor_name_meta = $album_meta_data['album_credit_tab_contributor_information_' . $c . '_contributor_name'];
                        if ( is_array($album_credit_contributor_name_meta) ) {
                            foreach($album_credit_contributor_name_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_credit_contributor_name = $value;
                                //echo 'Album Credit Contributors Name: ' . $album_credit_contributor_name . '<br/>';
                            }
                        }


                        // Contributor Image
                        $album_credit_contributor_image_meta = $album_meta_data['album_credit_tab_contributor_information_' . $c . '_contributor_image'];
                        if ( is_array($album_credit_contributor_image_meta) ) {
                            foreach($album_credit_contributor_image_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_credit_contributor_image_url = $value;
                                $album_credit_contributor_image = wp_get_attachment_image_src($album_credit_contributor_image_url, $size = 'full');
                                //echo 'Album Contributor Image: ' . $album_credit_contributor_image[0] . '<br/>';
                            }
                        }


                        // Contributor Profile Link
                        $album_credit_contributor_profile_meta = $album_meta_data['album_credit_tab_contributor_information_' . $c . '_contributor_profile_link'];
                        if ( is_array($album_credit_contributor_profile_meta) ) {
                            foreach($album_credit_contributor_profile_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_credit_contributor_profile_url = $value;
                                //echo 'Album Credit Contributors Profile URL: ' . $album_credit_contributor_profile_url . '<br/>';
                            }
                        }


                        // Contributor Bio Text
                        $album_credit_contributor_bio_meta = $album_meta_data['album_credit_tab_contributor_information_' . $c . '_contributor_bio'];
                        if ( is_array($album_credit_contributor_bio_meta) ) {
                            foreach($album_credit_contributor_bio_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_credit_contributor_bio_text = $value;
                                //echo 'Album Credit Contributors Bio: ' . $album_credit_contributor_bio_text . '<br/>';
                            }
                        }


                        $contributor_el = '<li><div class="contents"><div class="contributor-img" style="background-image: url(' . $album_credit_contributor_image[0] . ');"></div><a href="' . $album_credit_contributor_profile_url . '" title="' . $album_credit_contributor_name . '" target="_blank">' . $album_credit_contributor_name . '</a> - ';
                        echo $contributor_el;

                        // Contributor Type
                        $album_credit_contributor_type_meta = $album_meta_data['album_credit_tab_contributor_information_' . $c . '_contributor_type'];
                        if ( is_array($album_credit_contributor_type_meta) ) {
                            foreach($album_credit_contributor_type_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $serialized_array = unserialize($value);
                                //print_r($serialized_array);

                                foreach($serialized_array as $key2 => $value2) {
                                    //echo $key2 . ' : ' . $value2 . '<br/><br/>';
                                    $album_credit_contributor_type = $value2;
                                    //echo 'Album Credit Contributor Type: ' . $album_credit_contributor_type . '<br/>';
                                    $album_credit_contributor_type_lowercase = strtolower($album_credit_contributor_type);

                                    if ( $album_credit_contributor_type === 'synth bass and effects' ) {
                                        $album_credit_contributor_type = 'synth';
                                    }

                                    $contributed_icon = '<span class="contributed-icon ' . $album_credit_contributor_type_lowercase . '"></span>';
                                    echo $contributed_icon;
                                }
                            }
                        }

                        $contributed_tracks_start = '<div class="contributed-tracks"><p>Tracks: </p>';
                        echo $contributed_tracks_start;

                        // Contributor Tracks
                        $album_credit_contributor_track_meta = $album_meta_data['album_credit_tab_contributor_information_' . $c . '_contributed_to_track_number'];
                        if ( is_array($album_credit_contributor_track_meta) ) {
                            foreach($album_credit_contributor_track_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $serialized_array2 = unserialize($value);
                                //print_r($serialized_array);

                                //$numItems = count($serialized_array2);
                                $last_key = end(array_keys($serialized_array2));

                                foreach($serialized_array2 as $key2 => $value2) {
                                    //echo $key2 . ' : ' . $value2 . '<br/><br/>';
                                    $album_credit_contributor_track = $value2;
                                    //echo 'Album Credit Contributor Tracks: ' . $album_credit_contributor_track . '<br/>';

                                    if ($key2 == $last_key) {
                                        //echo 'last item';
                                        //echo '<span>' . $album_credit_contributor_track . '</span>';
                                    } else {
                                        //echo '<span>' . $album_credit_contributor_track . ', </span>';
                                    }
                                    

                                    // if ( $c === $numItems ) {
                                    //     //echo 'last item';
                                    //     echo '<span>' . $album_credit_contributor_track . '</span>';
                                    // } else {
                                    //     echo '<span>' . $album_credit_contributor_track . ', </span>';
                                    // }
                                    
                                    
                                }
                            }
                        }

                        
                        //echo '</div></div></li>';



                    // 
                    // End of contributors for loop
                    //
                    }

                    //echo '</ul></div></div>';

                }*/



                
                /**********************************************
                 * Start tab section
                 * Get album credit contributors
                **********************************************/
                $album_credit_contributors_simple = $album_meta_data['album_credit_tab_contributor_info'];
                if ( is_array($album_credit_contributors_simple) ) {
                    foreach($album_credit_contributors_simple as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_contributors_amount_simple = $value;
                        //echo 'Album Credit Contributors Amount Simple: ' . $album_contributors_amount_simple . '<br/>';
                    }

                    echo '<div id="album-additional-info">';
                    
                    $tab_buttons = '<div class="tab">
                            <button class="tablinks active album-credits" data-value="' . $album_credit_button_text . '">' . $album_credit_button_text . '</button><button class="tablinks album-videos" data-value="' . $album_video_button_text . '">' . $album_video_button_text . '</button><button class="tablinks album-reviews" data-value="' . $album_review_button_text . '">' . $album_review_button_text . '</button>
                        </div>';
                    echo $tab_buttons;


                    $tab_class_lowercase = strtolower($album_credit_button_text);

                    //$video_tab_class_lowercase = strtolower($album_video_button_text);

                    $tab_content = '<div id="value-' . $tab_class_lowercase . '" class="tabcontent active"><div class="left-container"><h3>' . $album_credit_page_title . '</h3><h4>' . $album_credit_section_title . '</h4><ul class="contributor-list">';
                    echo $tab_content;

                    for ( $c = 0; $c < $album_contributors_amount_simple; $c++ ) {

                        // Contributor Info
                        $album_credit_contributor_name_meta_simple = $album_meta_data['album_credit_tab_contributor_info_' . $c . '_add_contributor'];
                        if ( is_array($album_credit_contributor_name_meta_simple) ) {
                            foreach($album_credit_contributor_name_meta_simple as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_credit_contributor = $value;
                                //echo 'Album Credit Contributors Info: ' . $album_credit_contributor . '<br/>';
                            }
                        }

                        $contributor_el_simple = '<li><div class="contents"><p>' . $album_credit_contributor . '</p></div></li>';
                        echo $contributor_el_simple;

                    }

                    echo '</ul></div>';

                }



                
                /**********************************************
                 * Get album credits tab section title
                **********************************************/
                $album_credits_section_title_meta = $album_meta_data['album_credit_tab_credits_title'];
                if ( is_array($album_credits_section_title_meta) ) {
                    foreach($album_credits_section_title_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_credits_section_title = $value;
                        //echo 'Album Credits Section Title: ' . $album_credits_section_title . '<br/>';
                    }
                }



                
                /**********************************************
                 * Get album credit info - Plain Text
                **********************************************/
                $album_credits_simple = $album_meta_data['album_credit_tab_credits_info'];
                if ( is_array($album_credits_simple) ) {
                    foreach($album_credits_simple as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_credits_amount_simple = $value;
                        //echo 'Album Credits Amount Simple: ' . $album_credits_amount_simple . '<br/>';
                    }
                    
                    
                    $tab_class_lowercase = strtolower($album_credit_button_text);

                    $credits_content = '<div class="right-container"><h4>' . $album_credits_section_title . '</h4><ul class="contributor-list">';
                    echo $credits_content;

                    for ( $c = 0; $c < $album_credits_amount_simple; $c++ ) {

                        // Contributor Info
                        $album_credits_meta_simple = $album_meta_data['album_credit_tab_credits_info_' . $c . '_add_credit'];
                        if ( is_array($album_credits_meta_simple) ) {
                            foreach($album_credits_meta_simple as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_credits_text = $value;
                                //echo 'Album Credit Contributors Info: ' . $album_credit_contributor . '<br/>';
                            }
                        }

                        $credits_el_simple = '<li><div class="contents"><p>' . $album_credits_text . '</p></div></li>';
                        echo $credits_el_simple;

                    }

                    echo '</ul>';

                }



                /**********************************************
                 * Get album notes list section title
                **********************************************/
                $album_notes_section_title_meta = $album_meta_data['album_credit_tab_notes_title'];
                if ( is_array($album_notes_section_title_meta) ) {
                    foreach($album_notes_section_title_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_notes_section_title = $value;
                        //echo 'Album Notes List Title: ' . $album_notes_section_title . '<br/>';
                    }
                }



                /**********************************************
                 * Get album credit info - Plain Text
                **********************************************/
                $album_notes_simple = $album_meta_data['album_credit_tab_notes_info'];
                if ( is_array($album_notes_simple) ) {
                    foreach($album_notes_simple as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_notes_amount_simple = $value;
                        //echo 'Album Notes Amount Simple: ' . $album_notes_amount_simple . '<br/>';
                    }
                    

                    $notes_content = '<h4>' .$album_notes_section_title . '</h4><ul class="contributor-list">';
                    echo $notes_content;

                    for ( $c = 0; $c < $album_notes_amount_simple; $c++ ) {

                        // Contributor Info
                        $album_notes_meta_simple = $album_meta_data['album_credit_tab_notes_info_' . $c . '_add_note'];
                        if ( is_array($album_notes_meta_simple) ) {
                            foreach($album_notes_meta_simple as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_notes_text = $value;
                                //echo 'Album Note Info: ' . $album_notes_text . '<br/>';
                            }
                        }

                        $notes_el_simple = '<li><div class="contents"><p>' . $album_notes_text . '</p></div></li>';
                        echo $notes_el_simple;

                    }

                    echo '</ul></div><div class="clearfix"></div></div>';

                }



                /**********************************************
                 * Get album videos tab section title
                **********************************************/
                $album_video_section_title_meta = $album_meta_data['album_video_tab_section_title'];
                if ( is_array($album_video_section_title_meta) ) {
                    foreach($album_video_section_title_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_video_section_title = $value;
                        //echo 'Album Video Section Title: ' . $album_video_section_title . '<br/>';
                    }
                }



                /**********************************************
                 * Get album videos - Plain Text
                **********************************************/
                $album_videos_simple = $album_meta_data['album_video_tab_video_info'];
                if ( is_array($album_videos_simple) ) {
                    foreach($album_videos_simple as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_videos_amount_simple = $value;
                        //echo 'Album Video Amount Simple: ' . $album_videos_amount_simple . '<br/>';
                    }

                    $video_tab_class_lowercase = strtolower($album_video_button_text);

                    $video_tab_content = '<div id="value-' . $video_tab_class_lowercase . '" class="tabcontent"><div class="video-section"><h3>' . $album_video_section_title . '</h3><div id="video-carousel">';
                    echo $video_tab_content;

                    for ( $c = 0; $c < $album_videos_amount_simple; $c++ ) {

                        
                        // Video Hosting Name - Youtube or Vimeo
                        $album_video_hostname_meta_simple = $album_meta_data['album_video_tab_video_info_' . $c . '_video_host_name'];
                        if ( is_array($album_video_hostname_meta_simple) ) {
                            foreach($album_video_hostname_meta_simple as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_video_hostname = $value;
                                //echo 'Album Video Hostname: ' . $album_video_hostname . '<br/>';
                            }
                        }


                        // Video Name
                        $album_video_name_meta_simple = $album_meta_data['album_video_tab_video_info_' . $c . '_video_name'];
                        if ( is_array($album_video_name_meta_simple) ) {
                            foreach($album_video_name_meta_simple as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_video_name = $value;
                                //echo 'Album Video Name: ' . $album_video_name . '<br/>';
                            }
                        }


                        // Video Poster Image
                        $album_video_image_meta = $album_meta_data['album_video_tab_video_info_' . $c . '_video_poster_image'];
                        if ( is_array($album_video_image_meta) ) {
                            foreach($album_video_image_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_video_image_url = $value;
                                $album_video_image = wp_get_attachment_image_src($album_video_image_url, $size = 'full');
                                //echo 'Album Video Poster Image: ' . $album_video_image[0] . '<br/>';
                            }
                        }


                        // Video URL Link
                        $album_video_url_meta = $album_meta_data['album_video_tab_video_info_' . $c . '_video_url_id'];
                        if ( is_array($album_video_url_meta) ) {
                            foreach($album_video_url_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_video_id = $value;
                                //echo 'Album Video ID: ' . $album_video_id . '<br/>';

                                if ( $album_video_hostname === 'Youtube' ) {
                                    // http://www.youtube.com/embed/okqEVeNqBhc?html5=1&rel=0&showinfo=0&&autoplay=1&origin=http://localhost.com
                                    $album_embed_video_src = 'https://www.youtube.com/embed/' . $album_video_id . '?html5=1&autoplay=0&rel=0&showinfo=0&modestbranding=0&origin=http://localhost.com';

                                    //echo $album_embed_video_src;
                                    $album_video_embed = '<div class="video-container youtube"><h4>' . $album_video_name . '</h4><iframe type="text/html" src="' . $album_embed_video_src . '" frameborder="0"></iframe></div>';
                                    //$album_video_embed = '<div class="video-container"><h4>' . $album_video_name . '</h4></div>';
                                    echo $album_video_embed;

                                }

                                if ( $album_video_hostname === 'Vimeo' ) {
                                    $album_embed_video_src = 'https://player.vimeo.com/video/' . $album_video_id . '?title=0&byline=0&portrait=0';

                                    $vimeo_embed_player_src = '<div class="video-container vimeo"><h4>' . $album_video_name . '</h4><iframe type="text/html" src="' . $album_embed_video_src . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
                                    //$vimeo_embed_player_src = '<div class="video-container"><h4>' . $album_video_name . '</h4></div>';
                                    echo $vimeo_embed_player_src;
                                
                                }

                                
                                
                            }
                        }



                        //$contributor_el_simple = '<li><div class="contents"><p>' . $album_credit_contributor . '</p></div></li>';
                        //echo $contributor_el_simple;

                    }

                    echo '</div></div></div>';

                }




                /**********************************************
                 * Get album reviews
                **********************************************/
                $album_reviews_meta = $album_meta_data['album_review_tab_review_info'];
                if ( is_array($album_reviews_meta) ) {
                    foreach($album_reviews_meta as $key => $value) {
                        //echo $key . ' : ' . $value . '<br/><br/>';
                        $album_reviews_amount = $value;
                        //echo 'Album Review Amount: ' . $album_reviews_amount . '<br/>';
                    }


                    // Review section title
                    $album_review_section_title_meta = $album_meta_data['album_review_tab_section_title'];
                    if ( is_array($album_review_section_title_meta) ) {
                        foreach($album_review_section_title_meta as $key => $value) {
                            //echo $key . ' : ' . $value . '<br/><br/>';
                            $album_review_section_title = $value;
                            //echo 'Album Review Section Title: ' . $album_review_section_title . '<br/>';
                        }
                    }


                    $review_tab_class_lowercase = strtolower($album_review_button_text);

                    $review_tab_content = '<div id="value-' . $review_tab_class_lowercase . '" class="tabcontent"><div class="reviews-section"><h3>' . $album_review_section_title . '</h3><div id="review-carousel">';
                    echo $review_tab_content;

                    for ( $c = 0; $c < $album_reviews_amount; $c++ ) {


                        // Review Name
                        $album_review_name_meta = $album_meta_data['album_review_tab_review_info_' . $c . '_review_source_name'];
                        if ( is_array($album_review_name_meta) ) {
                            foreach($album_review_name_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_review_name = $value;
                                //echo 'Album Review Name: ' . $album_review_name . '<br/>';
                            }
                        }

                        
                        // Review content
                        $album_review_content_meta = $album_meta_data['album_review_tab_review_info_' . $c . '_review_content'];
                        if ( is_array($album_review_content_meta) ) {
                            foreach($album_review_content_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_review_content = $value;
                                //echo 'Album Review Content: ' . $album_review_content . '<br/>';
                            }
                        }


                        // Reveiw URL Link
                        $album_review_url_meta = $album_meta_data['album_review_tab_review_info_' . $c . '_review_source_url'];
                        if ( is_array($album_review_url_meta) ) {
                            foreach($album_review_url_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_review_url = $value;
                                //echo 'Album Review URL: ' . $album_review_url . '<br/>';
                            }
                        }


                        // Reveiw ratings
                        $album_review_rating_meta = $album_meta_data['album_review_tab_review_info_' . $c . '_review_rating'];
                        if ( is_array($album_review_rating_meta) ) {
                            foreach($album_review_rating_meta as $key => $value) {
                                //echo $key . ' : ' . $value . '<br/><br/>';
                                $album_review_rating = $value;
                                //echo 'Album Review rating: ' . $album_review_rating . '<br/>';
                            }
                        }


                        // Reveiw rating stars
                        echo '<div class="album-review"><div class="rating" data-vote="0">';
                        for ( $r = 0; $r < 5; $r++ ) {
                        
                            echo '<div class="star">';

                            if ($r < $album_review_rating) {
                                echo '<span class="full star-colour" data-value="0"></span>';
                            } else {
                                echo '<span class="full" data-value="0"></span>';
                            }
                            echo '</div>';  
                        }


                        $review_el = '</div><p>&ldquo;' . $album_review_content . '&rdquo;</p><a class="album-review-link" href="' . $album_review_url . '" title="' . $album_review_name . '" target="_blank">' . $album_review_name . '</a></div>';
                        echo $review_el;


                    }


                    echo '</div><div class="clearfix"></div></div>';


                }





 
            /* 
            * End of addition information tab section
            */   
            echo '</div>';     
            }



                
            /* 
            * End of album
            */
            echo '</div>';
            
            }
 


                
            /******************************************
            * Make php array avaialbe to front-end or admin page
            * wp_localize_script('', '', ''); => wp native function
            ******************************************/
            //wp_enqueue_style('slick-css', get_stylesheet_directory_uri() . '/custom_css/vendor/slick.css');
            

            // Register the script
            wp_register_script( 'slick-carousel-js', get_stylesheet_directory_uri() . '/custom_js/vendor/slick.min.js' );
            wp_register_script( 'music-detail-page-js', get_stylesheet_directory_uri() . '/custom_js/music-detail-page.js' );
            wp_register_script( 'parallax-js', get_stylesheet_directory_uri() . '/custom_js/vendor/parallax.min.js' );
            

            //wp_enqueue_script('jquery');
            wp_enqueue_script('slick-carousel-js', get_stylesheet_directory_uri() . '/custom_js/vendor/slick.min.js');
            wp_enqueue_script('music-detail-page-js', get_stylesheet_directory_uri() . '/custom_js/music-detail-page.js');
            wp_enqueue_script( 'parallax-js', get_stylesheet_directory_uri() . '/custom_js/vendor/parallax.min.js' );


            // Give JS the data
            wp_localize_script('music-detail-page-js', 'videos_count', $album_videos_amount_simple);



        ?>


		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->




<?php
    //get_sidebar();
    get_footer();