<?php
/**
 * Template Name: Share Page Template
 * 
 * Credit: Smashicons
 * Instagram share creative: https://www.instagram.com/p/BemFFcsF-5c/
 */
 
get_header(); ?>
 
<div id="main-content" class="main-content">
 
<!--?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?-->
 
	<div id="primary" class="content-area jas-container">
		<div id="content" class="site-content vc_row" role="main">
            




        <!-- ************************************************* 
        * Unwanted
        ************************************************* -->
		<!--?php
			$custom_posts = new WP_Query( array(
				'order_by' => 'title',
				'order'    => 'asc'
			));
			if ( $custom_posts->have_posts() ) :

				while ( $custom_posts->have_posts() ) : $custom_posts->the_post();

					get_template_part( 'content', get_post_format() );

				endwhile;

				//twentyfourteen_paging_nav();

                echo 'Main content';

			else :

				get_template_part( 'content', 'none' );

			endif;
        ?-->
        




        <!-- ************************************************* 
        * WR1 Custom code-break 
        ************************************************* -->
        <?php 
            //echo 'Main content';


            /************************************************* 
            * Get subscribed user 
            *************************************************/
            $user_ID = get_current_user_id();
            //echo 'User ID: '.$user_ID.'</br>';

            $has_sub = wcs_user_has_subscription( 10, '', 'active' );
            if ( $has_sub) {
                //echo 'User have active subscription';
            } else {
                //echo 'User does not have active subscription';
            }


            /************************************************* 
            * Get from backend API 
            *************************************************/
            //$image_URL = 'http://localhost/wp-content/uploads/2018/02/share_test.jpg';
            $image_URL = 'https://wr1test.imgix.net/clubs/164/090CD02E-3F54-4A67-812C-046F386810672018-01-08T19-15-23Z.JPG';
            $image_format = '?fit=crop&w=600&h=600';
            $image = $image_URL.$image_format;

            $post_owner = 'Brad Pit';
            $post_date = '2017-';
            $title = 'Test is the test title';
            $caption = 'Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post Here is my comment for the post ';

            // Likes
            $post_likes = 45;

            // Comments
            $user_name = 'Ryan Mahabir';
            $user_comment = 'What a great post! What a great post! What a great post! What a great post! What a great post! What a great post! What a great post! What a great post! What a great post! What a great post! What a great post! ';

            
            /************************************************* 
            * Render share HTML 
            *************************************************/
            //$postContent = '<div id="post-content"><h2>'.$title.'</h2><div class="post-image"><img src="'.$image_URL.'" alt="'.$title.'" /></div><div class="post-caption"><p>'.$caption.'</p></div></div>';
            

            /************************************************* 
            * Post
            *************************************************/
            $post_container_start = '<div id="post-content">';

                //$post_title = '<h2>'.$title.'</h2>';

                // Place image in HTML img tag
                //$post_image = '<div class="post-image"><img src="'.$image.'" alt="'.$title.'" /></div>';
                $post_image = '<div class="post-image" style="background-image: url('.$image.');"></div>';

                //$post_caption = '<div class="post-caption"><p>'.$caption.'</p></div>';

            $post_container_end = '</div>';


            /************************************************* 
            * Post info - comments and likes
            *************************************************/
            $post_info = '<div id="post-info"><div id="masthead"><h3>'.$post_owner.'</h3></div>';
            
            $post_caption = '<div class="comment-section"><div class="post-caption"><p>'.$caption.'</p></div>';

            $users_comments = '<div class="user_comment"><p class="username">'.$user_name.'</p><p class="comment">'.$user_comment.'</p></div></div>';

            $like_section = '<div class="post-likes"><div class="like-icon"></div><p>'.$post_likes.' likes</p></div>';

            $post_date_section = '';

            $post_info_footer = '<div id="post-footer">'.$like_section.'</div></div>';

            $clearfix = '<div class="clearfix"></div>';
            
            
            // Concat
            $post = $post_info.$post_caption.$users_comments;
            $share_post = $post_container_start.$post_image.$post.$post_info_footer.$clearfix.$post_container_end;


            /************************************************* 
            * Share
            *************************************************/
            $downloadApp_contaner_start = '<div class="vc_row wpb_fadeInUp fadeInUp wpb_start_animation animated">';

                $apple_share = '<a id="app-download-apple" class="app-download" href="http://google.com" target="_blank" title="Available on the iPhone App Store">Available on the iPhone App Store</a>';
            
                $google_share = '<a id="app-download-google" class="app-download" href="http://google.com" target="_blank" title="Available on the iPhone App Store">Android app on Google Play</a>';

            $downloadApp_contaner_end = '</div>';

            // Concat
            $downloadAppLinks = $downloadApp_contaner_start.$apple_share.$google_share.$downloadApp_contaner_end;


            /************************************************* 
            * Display HTML
            *************************************************/
            $share = $share_post.$downloadAppLinks;

            $result = sprintf("%s", $share);
            echo $result;


        ?>




		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php


//get_sidebar();
get_footer();