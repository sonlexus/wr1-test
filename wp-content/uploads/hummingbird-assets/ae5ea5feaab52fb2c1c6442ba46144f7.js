/**handles:ticket-list-display-js**/
/******************************************
* Ticket List Display
* Author: Ryan Mahabir
* Date: 2018-01-28
******************************************/


/******************************************
* Start the magic
******************************************/
(function($) {
	$(document).ready(function($) {
        //console.log('ready');

		var ticketList = [];	  
        //console.log(shop_options);
        
		//$('#event-ticket-display').append(shop_options)
		var userLimit = $('#event-ticket-display').data( "attr" );
		//console.log('UserLimit: ' + userLimit);

		$.each( shop_options, function( key, value ) {
			//console.log( key + ": " + value );

			if (userLimit === -1) {
				//console.log('userlimit is null');
				
				ticketList.push(value);
			} else if (key > userLimit-1) {
				//console.log('hit limit');

				return;
			} else {
				ticketList.push(value);
			}
		});

        $('#event-ticket-display').append(ticketList);
        
        
		/*function init(el, username, feed) {
			var newInstagramFeed = new instagramCarousel( el );
			newInstagramFeed.init(username, feed);
        }*/
        
        //init('body', username, feed);


	});
}(jQuery));