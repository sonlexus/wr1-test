/**handles:custom-js**/
/* ************************** */
/* Init
/* ************************** */
function init() {
	var el = jQuery( 'body' ),
		windowWidth = jQuery(window).width(),
		isMobile = false;

	// Modify event tour dates
	renderEventDates(el);

	// Change mobile icon
	changeMobileIcon(windowWidth);
}


/* ************************** */
/* Change mobile icon
/* Detects homepage, mobile icon, removes icon, replaces it with custom div
/* controlled by css
/* ************************** */
function changeMobileIcon(windowWidth) {
	if (windowWidth <= 736) {
		//console.log('mobile view');

		var	el = jQuery( 'body' ),
			$header = ('#jas-header'),
			$mobileHeader = el.find( $header ),
			$domNode = 'a.hide-md',
			$mobileIcon = el.find( $domNode );

		if (el.hasClass('home')) {
			//console.log('homepage');

			if ($mobileIcon.length) {
				//console.log('found mobile icon' + $mobileIcon);

				//$newIcon = '<img src="../wp-content/themes/claue-child-artist-template/custom_images/hamburger-white.svg" width="30" height="16" alt="Menu">'

				$newIcon = '<div id="custom-mobile-icon"></div>'
	
				$mobileIcon.empty();
				$mobileIcon.append($newIcon);
			}
		}

		
	}
}


/* ************************** */
/* Change tour/event dates
/* 1 => January
/* ************************** */
function renderEventDates(element) {

	var //$element = jQuery( element ),
    	$domNode = '.displayProduct-Container',
    	$eventTicketsTable = element.find( $domNode ),
    	monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	monthNamesAbv = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];


	if ($eventTicketsTable.length) {
		console.log('found event table');

		var $row = $eventTicketsTable.find( '.dp-table-tr' );

		jQuery($row).each(function(key, item){
			//console.log(key, item);

		    jQuery(this).find('.MonthDateClass').each(function(i, item){
		    	var date = Number(item.innerHTML),
		    		newDate = monthNamesAbv[date-1];
		    	
		    	//console.log(this, newDate);
		    	jQuery(this).text(newDate);
		    });

		});

		//removeMeetAndGreetCta($row);
	}

	return;
}


/* ************************** */
/* Start the magic!
/* ************************** */
jQuery( document ).ready(function() {
	console.log( 'ready!' );
	
	// Call Init function
	init();

});